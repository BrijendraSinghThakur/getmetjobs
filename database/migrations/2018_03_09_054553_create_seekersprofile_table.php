<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeekersprofileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seeker_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seeker_id');
            $table->string('name');
            $table->string('phone');
            $table->date('dob');
            $table->string('gender');
            $table->string('address');
            $table->string('city');
            $table->string('pincode');
            $table->string('marital_status');
            $table->string('current_location');
            $table->string('total_experience');
            $table->string('preferred_location');
            $table->string('annual_salary');
            $table->string('industry');
            $table->string('highest_degree');
            $table->string('resume');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('seeker_profiles');
    }
}
