<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeekerWorkExperienceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seeker_work_experience', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seeker_id');
            $table->string('designation');
            $table->string('company');
            $table->integer('salary');
            $table->date('join_date');
            $table->date('leave_date');
            $table->string('notice_period');
            $table->enum('education_type', ['Full Time', 'Part Time','Distance Learning']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seeker_qualification');
    }
}
