<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->biginteger('mobile');
            $table->string('password');
            $table->enum('role', ['super admin', 'admin']);
            $table->string('login_token');
            $table->enum('status', ['Active', 'Blocked']);
            $table->enum('gender', ['', 'Male','Female']);
            $table->rememberToken();
            $table->date('date_of_birth')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
