<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeekerQualificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seeker_qualification', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seeker_id');
            $table->string('highest_education');
            $table->string('course');
            $table->string('specialization');
            $table->string('university');
            $table->string('passout_year');
            $table->enum('education_type', ['Full Time', 'Part Time','Distance Learning']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seeker_qualification');
    }
}
