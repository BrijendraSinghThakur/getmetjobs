<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobInterviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_interview', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_id');
            $table->integer('seeker_id');
            $table->dateTime('schedule_time')->nullable();
            $table->integer('schedule_by');
            $table->dateTime('re_schedule_time')->nullable();
            $table->dateTime('selected_time')->nullable();
            $table->dateTime('rejected_time')->nullable();
            $table->enum('status', ['Schedule', 'Re-schedule','Selected','Rejected']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_interview');
    }
}
