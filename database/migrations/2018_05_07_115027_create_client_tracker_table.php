<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientTrackerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_tracker', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('created_by');
            $table->string('domain_name');
            $table->string('client_name');
            $table->string('website');
            $table->string('location');
            $table->integer('total_vacancy');
            $table->integer('min_experience');
            $table->integer('max_experience');
            $table->bigInteger('min_offered_ctc');
            $table->bigInteger('max_offered_ctc');
            $table->integer('education_sub_category_id')->comment('qualification');
            $table->string('gender_preference')->comment('Male and Female');
            $table->string('other_benifits');
            $table->integer('working_hours');
            $table->string('skill_id');
            $table->string('age_criteria');
            $table->string('travelling_required');
            $table->integer('working_days');
            $table->string('other_skills');
            $table->enum('status',['Pending', 'In Progress','Completed']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_tracker');
    }
}
