<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class PrivacyPolicy extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'privacy_policy';
    public $timestamps = false;
}