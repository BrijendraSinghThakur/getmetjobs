<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EducationSubCategory extends Model
{
    protected $table = 'education_sub_category';
    public $timestamps = false;
}
