<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class WorkExperience extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'work_experience';
    public $timestamps = false;
}