<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Seeker;
use App\OtpVerification;
use App\DateTime;
use App\NotificationSetting;
use App\Jobs;
use App\SaveJob;
use App\ApplyJob;
use App\JobNotification;
use App\Industry;
use App\Designations;
use App\EducationSubCategory;
use Carbon\Carbon;
use File;
use App\WorkExperience;
use App\Salary;
use App\Specialization;
use App\TermsCondition;
use App\PrivacyPolicy;
use App\SeekerWorkExperience;
use App\SeekerQualification;
use App\SchoolMedium;
use App\Skill;
use App\AppDownload;
use App\Department;
use App\Role;
use App\SeekerLoginTrack;
use App\SeekerAdditionalSkill;
use PDF;
use App\JobInterview;

class SeekerController extends Controller
{
    # Function : This function used to send otp for new registration
    # Request  : mobile number and email
    # Response : success true/false json response
    # Author   : Brijendra
    public function registerSeeker(Request $request)
    {

        $data = $request->all();

        if (isset($data['mobile']) && $data['mobile'] ) 
        {
            $mobile = $data['mobile'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Mobile is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        if (isset($data['email']) && $data['email'] ) 
        {
            $email = $data['email'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Email is Required.";
            $array['data'] = (object)array();
            return $array;
        }


        $seekerDatail = Seeker::where('email', $email)->first();
        if($seekerDatail != '')
        {
            $array['success']  = false;
            $array['message'] = "Email already exist!";
            $array['data'] = (object)array();
            return $array;
        }

        $seekerDatail = Seeker::where('mobile', $mobile)->first();
        if($seekerDatail != '')
        {
            $array['success']  = false;
            $array['message'] = "Mobile No already exist!";
            $array['data'] = (object)array();
            return $array;
        }

        $message = "Welcome to GetMeJob your OTP for registration is : ";
        $result  = $this->sendOtp($mobile, 'registration', 'seeker', $message);

        if ($result != 'Failed') 
        {
            $array['success'] = true;
            $array['message'] = "OTP Sent";
            $array['data']    = (object)array();
            return $array;
        }
        else
        {
            $array['success']  = false;
            $array['message'] = "OTP sending failed";
            $array['data'] = (object)array();
            return $array;
        }
        
    }


    # Function : This function used to verify otp and if verified then register new seeker
    # Request  : mobile, email, password, otp, name
    # Response : success true/false json response
    # Author   : Brijendra
    public function verifyOtpAndRegister(Request $request)
    {
        $data       = $request->all();

        $name       = NULL;
        $mobile     = NULL;
        $email      = NULL;
        $password   = NULL;
        $year       = '';
        $month      = '';
        $token      = '';

        if (isset($data['otp']) && $data['otp'] ) 
        {
            $otp = $data['otp'];

            $verifyOtp = OtpVerification::where('otp', $otp)->where('otp_screen', 'registration')->first();

            if ($verifyOtp != '') 
            {
                if (isset($data['token']) && $data['token'] ) 
                {
                    $token = $data['token'];
                }
                // else
                // {
                //     $array['success'] = false;
                //     $array['message'] = "FCM token is required.";
                //     $array['data'] = (object)array();
                //     return $array;
                // }

                if (isset($data['name']) && $data['name'] ) 
                {
                    $name = $data['name'];
                }
                else
                {
                    $array['success'] = false;
                    $array['message'] = "Name is Required.";
                    $array['data'] = (object)array();
                    return $array;
                }

                if (isset($data['mobile']) && $data['mobile'] ) 
                {
                    $mobile = $data['mobile'];
                }
                else
                {
                    $array['success'] = false;
                    $array['message'] = "Mobile is Required.";
                    $array['data'] = (object)array();
                    return $array;
                }

                if (isset($data['email']) && $data['email'] ) 
                {
                    $email = $data['email'];
                }
                else
                {
                    $array['success'] = false;
                    $array['message'] = "Email is Required.";
                    $array['data'] = (object)array();
                    return $array;
                }

                if (isset($data['password']) && $data['password'] ) 
                {
                    $password = md5($data['password']);
                }
                else
                {
                    $array['success'] = false;
                    $array['message'] = "Password is Required.";
                    $array['data'] = (object)array();
                    return $array;
                }


                $seeker             = new Seeker();
                $seeker->name       = $name;
                $seeker->mobile     = $mobile;
                $seeker->email      = $email;
                $seeker->password   = $password;
                $seeker->token      = $token;
                $seeker->save();

                $seeker->status = true;

                $notificationSetting  = new NotificationSetting();
                $notificationSetting->seeker_id         = $seeker->id;
                $notificationSetting->job_update        = 1;
                $notificationSetting->recruiter_update  = 1;
                $notificationSetting->general_update    = 1;
                $notificationSetting->apply_update      = 1;
                $notificationSetting->save();

                OtpVerification::where('otp', $otp)->where('otp_screen', 'registration')->delete();

                $array['success'] = true;
                $array['message'] = "OTP Verified Register Successfully.";
                $array['data']    = $seeker;
                return $array;
                
            }
            else
            {
                $array['success'] = false;
                $array['message'] = "otp not verified.";
                $array['data'] = (object)array();
                return $array;
            }

        }
        else
        {
            $array['success'] = false;
            $array['message'] = "otp is Required.";
            $array['data'] = (object)array();
            return $array;
        }
                
    }
    
    # Function : This function used to login seeker via email or mobile number
    # Request  : mobile/email, password
    # Response : success true/false json response
    # Author   : Brijendra
    public function loginSeeker(Request $request)
    {
    	$data = $request->all();

        $mobile = '';
        $email  = '';
        // file_put_contents('testvalue.txt',$data,FILE_USE_INCLUDE_PATH);

        if ((isset($data['email_or_number']) && $data['email_or_number'])) 
        {
            $emailOrNumber = $data['email_or_number'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Email or Mobile is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        if (preg_match("/^(\+91-|\+91|0)?\d{10}$/",$emailOrNumber)) 
        {
            $mobile = $emailOrNumber;
        }
        else
        {
            $email = $emailOrNumber;
        }
        
        if (isset($data['password']) && $data['password'] ) 
        {
           $password = md5($data['password']);
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Password is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        $fcmToken = '';
        if (isset($data['fcm_token']) && $data['fcm_token'] ) 
        {
           $fcmToken = $data['fcm_token'];
        }
        // else
        // {
        //     $array['success'] = false;
        //     $array['message'] = "FCM token is required.";
        //     $array['data'] = (object)array();
        //     return $array;
        // }

        if ($email != '') 
        {
            $checkUser = Seeker::where('email', $email)->first();

            if ($checkUser != '') 
            {
                $login = Seeker::where('email', $email)->where('password', $password)->first();
                if ($login != '') 
                {
                    $login->token = $fcmToken;
                    $login->save();
                    $status = false;
                    if($login->mobile)
                    {
                        $status = true;
                    }
                    $login->status = $status;
                    $this->loginTrack($login->id);
                    $array['success'] = true;
                    $array['message'] = "Login successfully!!";
                    $array['data'] = $login;
                    return $array;
                }
                else
                {
                    $array['success'] = false;
                    $array['message'] = "Invalid Credentials !!!";
                    $array['data'] = (object)array();
                    return $array;
                }
            }
            else
            {
                $array['success'] = false;
                $array['message'] = "This email not registered with us, please signup first.";
                $array['data']    = (object)array();
                return $array;
            }
        }
        else if ($mobile != '') 
        {

            $checkUser = Seeker::where('mobile', $mobile)->first();

            if ($checkUser != '') 
            {
                $login = Seeker::where('mobile', $mobile)->where('password', $password)->first();
                if ($login != '') 
                {
                    $login->token = $fcmToken;
                    $login->save();
                    $login->status = true;
                    $this->loginTrack($login->id);
                    $array['success'] = true;
                    $array['message'] = "Login successfully!!";
                    $array['data']    = $login;
                    return $array;
                }
                else
                {
                    $array['success'] = false;
                    $array['message'] = "Invalid Credentials !!!";
                    $array['data'] = (object)array();
                    return $array;
                }
            }
            else
            {
                $array['success'] = false;
                $array['message'] = "This mobile number not registered with us, please signup first.";
                $array['data'] = (object)array();
                return $array;
            }
        }

    }


    # Function : This function used to login seeker via facebook, google, linkedin
    # Request  : email id
    # Response : success true/false json response
    # Author   : Brijendra
    public function socialLogin(Request $request)
    {
        $data = $request->all();

        if (isset($data['email']) && $data['email']) 
        {
            $email = $data['email'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Email id is Required.";
            $array['data']    = (object)array();
            return $array;
        }

        $name = '';
        if (isset($data['name']) && $data['name']) 
        {
            $name = $data['name'];
        }

        $mobile = '';

        $fcmToken = '';
        if (isset($data['fcm_token']) && $data['fcm_token']) 
        {
            $fcmToken = $data['fcm_token'];
        }
        // else
        // {
        //     $array['success'] = false;
        //     $array['message'] = "FCM token is Required.";
        //     $array['data']    = (object)array();
        //     return $array;
        // }

        $checkUser = Seeker::where('email', $email)->first();


        if ($checkUser != '') 
        {
            if (isset($data['mobile']) && $data['mobile']) 
            {
                $mobile = $data['mobile'];
                $checkUser->mobile = $mobile;
            }
            $checkUser->token = $fcmToken;
            $checkUser->save();
            $status  = false;
            if($checkUser->mobile)
            {
                $status  = true;
            }
            $checkUser->status = $status;
            $this->loginTrack($checkUser->id);
            $array['success'] = true;
            $array['message'] = "Login Successfully.";
            $array['data']    = $checkUser;
            return $array;
        }
        else
        {
            $status  = false;
            if($mobile)
            {
                $status  = true;
            }

            $seeker             = new Seeker();
            $seeker->name       = $name;
            $seeker->mobile     = $mobile;
            $seeker->email      = $email;
            $seeker->password   = '';
            $seeker->token      = $fcmToken;
            $seeker->save();

            $notificationSetting  = new NotificationSetting();
            $notificationSetting->seeker_id         = $seeker->id;
            $notificationSetting->job_update        = 1;
            $notificationSetting->recruiter_update  = 1;
            $notificationSetting->general_update    = 1;
            $notificationSetting->apply_update      = 1;
            $notificationSetting->save();

            $seeker->status = $status;
            $this->loginTrack($seeker->id);
            $array['success'] = true;
            $array['message'] = "Successfully register and login";
            $array['data']    = $seeker;
            return $array;
        }


    }


    # Function : This function used to get seeker profile details
    # Request  : seeker_id
    # Response : success true/false json response
    # Author   : Brijendra
    public function getSeekerProfile(Request $request)
    {
        $data = $request->all();

        if (isset($data['seeker_id']) && $data['seeker_id'] ) 
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Seeker id is Required.";
            $array['data']    = (object)array();
            return $array;
        }

        $seekerDetails = Seeker::where('id', $seekerId)->first();
        if ($seekerDetails != '') 
        {
            $seekerDetails->join_date = date('d,M Y h:i A', strtotime($seekerDetails->created_at));
            $seekerDetails->modify_date = date('d,M Y h:i A', strtotime($seekerDetails->updated_at));
            if($seekerDetails->date_of_birth)
            {
                $seekerDetails->date_of_birth = date('d-m-Y', strtotime($seekerDetails->date_of_birth)); 
            }
            else
            {
                $seekerDetails->date_of_birth = '';
            }
            $baseUrl = url('/');

            $industry = '';
            if($seekerDetails['preferred_industry_id'])
            {
                $industryIds = explode(',', $seekerDetails['preferred_industry_id']); 
                $industryDetails = Industry::whereIn('id',$industryIds)->get()->toArray();   
                if(count($industryDetails) > 0)
                {
                    $industryArr = array();
                    foreach ($industryDetails as $key => $value) 
                    {
                        $industryArr[] = $value['industry_name']; 
                    }
                    $industry = implode(',', $industryArr);
                }
            }
            $seekerWorkExperienceDetails = SeekerWorkExperience::where('seeker_id', $seekerId)->get();
            foreach ($seekerWorkExperienceDetails as $key => $value) 
            {
                $value->experience = $value->experience." Year";
            }


            $seekerQualificationDetails = SeekerQualification::where('seeker_id', $seekerId)->get();

            $profileCompletePercentage = $this->seekerProfilePercentage($seekerId);
            $seekerDetails['department_name'] = "";
            if ($seekerDetails['department_id']) 
            {
                $department = Department::where('id', $seekerDetails['department_id'])->first();
                if ($department) 
                {
                    $seekerDetails['department_name'] = $department->department_name;
                }
            }

            $seekerDetails['role_name'] = "";
            if ($seekerDetails['role_id']) 
            {
                $role = Role::where('id', $seekerDetails['role_id'])->first();
                if ($role) 
                {
                    $seekerDetails['role_name'] = $role->role;
                }
            }

            $skillsDetails = Skill::get()->keyBy('id')->toArray();
            $skillIds = $seekerDetails['skill_ids'];
            $seekerDetails['skills'] = '';
            if($skillIds)
            {
                $skills = '';
                $skillIds = explode(',', $skillIds);
                foreach ($skillIds as $key => $skillval) 
                {
                    if(isset($skillsDetails[$skillval]))
                    {
                        if($skills)
                        {
                            $skills .= ', '. $skillsDetails[$skillval]['skill'];
                        }
                        else
                        {
                            $skills = $skillsDetails[$skillval]['skill'];
                        }
                    }
                }
                
                $seekerDetails['skills'] = $skills;
            }

            $additionalSkill = SeekerAdditionalSkill::where('seeker_id', $seekerId)->get();
            foreach ($additionalSkill as $key => $value) 
            {
                $seekerDetails['skills'] .= ', '. $value->skill;
            }

            if ($seekerDetails['skills'] == '') 
            {
                $seekerDetails['skills'] = 'NA';
            }

            $seekerDetails['industry'] = $industry;
            if ($seekerDetails['industry'] == '') 
            {
                $seekerDetails['industry'] = 'NA';
            }

            $seekerDetails['summary'] = $seekerDetails['about_me'];
            if ($seekerDetails['summary'] == '') 
            {
                $seekerDetails['summary'] = 'NA';
            }

            $seekerDetails['profile_percent'] = $profileCompletePercentage;
            $seekerDetails['profile_image'] = $baseUrl.$seekerDetails['profile_image'];
            $seekerDetails['experience'] = $seekerWorkExperienceDetails;
            $seekerDetails['qualification'] = $seekerQualificationDetails;
            $experience = 0;
            if(count($seekerWorkExperienceDetails) > 0)
            {
                $len = count($seekerWorkExperienceDetails);
                $experience = $seekerWorkExperienceDetails[$len-1]['experience'];
            }
            $seekerDetails['seeker_experience'] = $experience;
            $array['success'] = true;
            $array['message'] = "Record Found";
            $array['data']    = $seekerDetails;
            return $array;
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "User not exist";
            $array['data']    = (object)array();
            return $array;
        }

    }


    # Function : This function used to sent otp for forgot password
    # Request  : mobile
    # Response : success true/false json response
    # Author   : Brijendra
    public function forgetPassword(Request $request)
    {
        $data = $request->all();

        if (isset($data['mobile']) && $data['mobile']) 
        {
            $mobile = $data['mobile'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Mobile number is Required.";
            $array['data']    = (object)array();
            return $array;
        }

        $seekerDatail = Seeker::where('mobile', $mobile)->first();
        if($seekerDatail == '')
        {
            $array['success']  = false;
            $array['message'] = "This mobile number not registered with us, please signup first.";
            $array['data'] = (object)array();
            return $array;
        }

        $message = "Your GetMeJob forgot password OTP is : ";
        $result  = $this->sendOtp($mobile, 'forgot-password', 'seeker', $message);

        if ($result != 'Failed') 
        {
            $array['success'] = true;
            $array['message'] = "OTP Sent";
            $array['data']    = (object)array();
            return $array;
        }
        else
        {
            $array['success']  = false;
            $array['message'] = "OTP sending failed";
            $array['data'] = (object)array();
            return $array;
        }


    }


    # Function : This function used to verify otp
    # Request  : mobile, otp, password
    # Response : success true/false json response
    # Author   : Brijendra
    public function verifyOtp(Request $request)
    {
        $data = $request->all();

        if (isset($data['otp']) && $data['otp']) 
        {
            $otp = $data['otp'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Otp required";
            $array['data']    = (object)array();
            return $array;
        }

        if (isset($data['screen']) && $data['screen']) 
        {
            $screen = $data['screen'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Screen name required";
            $array['data']    = (object)array();
            return $array;
        }


        $verifyOtp = OtpVerification::where('otp', $otp)->where('otp_screen', $screen)->first();
        if ($verifyOtp != '') 
        {
            OtpVerification::where('otp', $otp)->where('otp_screen', $screen)->delete();

            $array['success'] = true;
            $array['message'] = "Otp Verified";
            $array['data']    = (object)array();
            return $array;
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Otp not verified, please enter correct otp";
            $array['data']    = (object)array();
            return $array;
        }

    }

    # Function : This function used to send message
    # Request  : mobile, otpType, userType, text
    # Response : failed / otp
    # Author   : Brijendra
    public function sendOtp($mobile, $otpType, $userType, $text)
    {
        //Your message to send, Add URL encoding here.
        $otpCode   = rand(1000, 9999);
        $message   = urlencode($text.$otpCode);
        $response_type = 'json';
        
        //Define route 
        $route = "4";
         
        if($otpType == 'forgot-password')
        {
            $message = "Use ".$otpCode." as your Getmejobs account password reset code. It’s valid for the next 30 mins. Thank You - Getmejobs";
        } 
        else
        {
            $message = $otpCode." is your OTP and is valid for the next 30 mins. Thank You - Getmejobs.";
        }
        $message   = urlencode($message);
        //Prepare you post parameters
        $postData = array(
            'authkey'  => '85262ARwrcvP1i7555eec59',
            'mobiles'  => $mobile,
            'message'  => $message,
            'sender'   => 'GETMEJOB',
            'route'    => $route,
            'response' => $response_type
        );
     
        //API URL
        $url = "https://control.msg91.com/sendhttp.php";
     
        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
        ));
     
        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
     
        //get response
        $output = curl_exec($ch);

        if(curl_errno($ch))
        {
            curl_close($ch); 

            return 'Failed';
        }
        else
        {
            curl_close($ch); 

            $seeker   = Seeker::where('mobile', $mobile)->first();
            $seekerId = 0;
            if ($seeker != '') 
            {
                $seekerId = (int)$seeker->id;
            }

            $otp = new OtpVerification();
            $otp->user_id   = $seekerId;
            $otp->user_type = $userType;
            $otp->otp       = $otpCode;
            $otp->otp_screen  = $otpType;
            $otp->save();
            
            return $otpCode;
        }
    }


    # Function : This function used to reset password
    # Request  : mobile, password
    # Response : success true/false json response
    # Author   : Brijendra
    public function resetPassword(Request $request)
    {
        $data = $request->all();
       
        if (isset($data['mobile']) && $data['mobile'] ) 
        {
            $mobile = $data['mobile'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Mobile is Required.";
            $array['data'] = (object)array();
            return $array;
        }
        
        if (isset($data['password']) && $data['password'] ) 
        {
            $password = md5($data['password']);
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Password is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        $checkUser = Seeker::where('mobile', $mobile)->first();

        if ($checkUser != '') 
        {
            $checkUser->password = $password;
            $checkUser->save();

            $array['success'] = true;
            $array['message'] = "Password successfully update";
            $array['data']    = (object)array();
            return $array;

        }
        else
        {
            $array['success'] = false;
            $array['message'] = "This mobile number not registered with us, please signup first.";
            $array['data'] = (object)array();
            return $array;
        }
        
    }


    # Function : This function used to update profile details
    # Request  : seeker id, details type, qualification details, industry details, skills details, experience details, resume 
    # Response : success true/false json response
    # Author   : Brijendra
    public function updateProfileDetails(Request $request)
    {
        $data = $request->all();
        // return $data;
       
        if (isset($data['seeker_id']) && $data['seeker_id'] ) 
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Seeker id is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        if (isset($data['details_type']) && $data['details_type'] ) 
        {
            $detailsType = strtolower($data['details_type']);
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Details type is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        $checkSeeker = Seeker::where('id', $seekerId)->first();

        if ($checkSeeker == '') 
        {
            $array['success'] = false;
            $array['message'] = "User not exist.";
            $array['data']    = (object)array();
            return $array;
        }

        // $profileCompletePercentage = 75;
        // $checkSeeker['profile_percent'] = $profileCompletePercentage;

        if ($detailsType == 'qualification') 
        {
            $highestEducation = '';
            if (isset($data['highest_education']) && $data['highest_education'] ) 
            {
                $highestEducation = $data['highest_education'];
            }
            else
            {
                $array['success'] = false;
                $array['message'] = "Education is required.";
                $array['data']    = (object)array();
                return $array;
            }

            $course = '';
            if (isset($data['course']) && $data['course'] ) 
            {
                $course = $data['course'];
            }

            $specialization = '';
            if (isset($data['specialization']) && $data['specialization'] ) 
            {
                $specialization = $data['specialization'];
            }

            $university = '';
            if (isset($data['university']) && $data['university'] ) 
            {
                $university = $data['university'];
            }

            $passoutYear = '';
            if (isset($data['passout_year']) && $data['passout_year'] ) 
            {
                $passoutYear = $data['passout_year'];
            }

            $educationType = '';
            if (isset($data['education_type']) && $data['education_type'] ) 
            {
                $educationType = $data['education_type'];
            }

            $marks = '';
            if (isset($data['marks']) && $data['marks'] ) 
            {
                $marks = $data['marks'];
            }

            $medium = '';
            if (isset($data['medium']) && $data['medium'] ) 
            {
                $medium = $data['medium'];
            }
            $seekerQualificationDetails = SeekerQualification::where('seeker_id', $seekerId)->where('highest_education', $highestEducation)->first();
            if($seekerQualificationDetails)
            {
                $array['success'] = false;
                $array['message'] = "This qualification already added.";
                $array['data']    = (object)array();
                return $array;
            }
            else
            {
                $seekerQualification = new SeekerQualification;
                $seekerQualification->seeker_id = $seekerId;
                $seekerQualification->highest_education = $highestEducation;
                $seekerQualification->course         = $course;
                $seekerQualification->specialization = $specialization;
                $seekerQualification->university     = $university;
                $seekerQualification->passout_year   = $passoutYear;
                $seekerQualification->education_type = $educationType;
                $seekerQualification->marks          = $marks;
                $seekerQualification->medium         = $medium;
                $seekerQualification->created_at     = date('Y-m-d H:i:s');
                $seekerQualification->updated_at     = date('Y-m-d H:i:s');
                $seekerQualification->save();

                $checkSeeker->updated_at = date('Y-m-d H:i:s');
                $checkSeeker->save();


                $seekerQualificationDetails = SeekerQualification::where('seeker_id', $seekerId)->get();
                $array['success'] = true;
                $array['message'] = "Qualification details.";
                $array['data']    = $seekerQualificationDetails;
                return $array;
            }
        }
        else if ($detailsType == 'industry') 
        {
            $industryIds = '';
            if (isset($data['industry_ids']) && $data['industry_ids'] ) 
            {
                $industryIds = $data['industry_ids'];
            }
            else
            {
                $array['success'] = false;
                $array['message'] = "Industry id is Required.";
                $array['data']    = (object)array();
                return $array;
            }

            if (isset($data['department_id']) && $data['department_id'] ) 
            {
                $departmentId = $data['department_id'];
            }
            else
            {
                $array['success'] = false;
                $array['message'] = "Department id is Required.";
                $array['data']    = (object)array();
                return $array;
            }

            if (isset($data['role_id']) && $data['role_id'] ) 
            {
                $roleId = $data['role_id'];
            }
            else
            {
                $array['success'] = false;
                $array['message'] = "Role id is Required.";
                $array['data']    = (object)array();
                return $array;
            }

            $checkSeeker->role_id  = $roleId;
            $checkSeeker->department_id  = $departmentId;
            $checkSeeker->preferred_industry_id  = $industryIds;
            $checkSeeker->updated_at = date('Y-m-d H:i:s');
            $checkSeeker->save();

            $industryIds = explode(',', $industryIds); 
            $industryDetail = Industry::select('industry_name')->whereIn('id', $industryIds)->get()->toArray();
            $industryArr = array();
            foreach ($industryDetail as $key => $value) 
            {
                $industryArr[] = $value['industry_name']; 
            }
            $checkSeeker->industry = implode(',', $industryArr);
            $array['success'] = true;
            $array['message'] = "Industry detail update";
            $array['data']    = $checkSeeker;
            return $array;
        }
        else if ($detailsType == 'skills') 
        {
            $skills = '';
            if (isset($data['skills']) && $data['skills'] ) 
            {
                $skills = $data['skills'];
            }

            $checkSeeker->skills  = $skills;
            $checkSeeker->updated_at = date('Y-m-d H:i:s');
            $checkSeeker->save();

            $array['success'] = true;
            $array['message'] = "Skills update";
            $array['data']    = $checkSeeker;
            return $array;
        }
        else if ($detailsType == 'experience') 
        {
            if (isset($data['designation']) && $data['designation'] ) 
            {
                $designation = $data['designation'];
            }
            else
            {
                $array['success'] = false;
                $array['message'] = "Designation Required.";
                $array['data']    = (object)array();
                return $array;
            }

            if (isset($data['company']) && $data['company'] ) 
            {
                $company = $data['company'];
            }
            else
            {
                $array['success'] = false;
                $array['message'] = "Company Required.";
                $array['data']    = (object)array();
                return $array;
            }

            if (isset($data['salary']) && $data['salary'] ) 
            {
                $salary = $data['salary'];
            }
            else
            {
                $array['success'] = false;
                $array['message'] = "Salary Required.";
                $array['data']    = (object)array();
                return $array;
            }

            if (isset($data['salary_in']) && $data['salary_in'] ) 
            {
                $salaryIn = $data['salary_in'];
            }
            else
            {
                $array['success'] = false;
                $array['message'] = "Salary Format Required.";
                $array['data']    = (object)array();
                return $array;
            }

            if (isset($data['join_date']) && $data['join_date'] ) 
            {
                $joinDate = $data['join_date'];
            }
            else
            {
                $array['success'] = false;
                $array['message'] = "Join date Required.";
                $array['data']    = (object)array();
                return $array;
            }

            if (isset($data['leave_date']) && $data['leave_date'] ) 
            {
                $leaveDate = $data['leave_date'];
            }
            else
            {
                $array['success'] = false;
                $array['message'] = "Leave date Required.";
                $array['data']    = (object)array();
                return $array;
            }

            $noticePeriod = '';
            if (isset($data['notice_period']) && $data['notice_period'] ) 
            {
                $noticePeriod = $data['notice_period'];
            }

            $workExperienceYear = 0;
            if (isset($data['experience']) && $data['experience'] ) 
            {
                $workExperienceYear = $data['experience'];
            }

            $checkSeeker->profile        = 'experience';
            $checkSeeker->updated_at = date('Y-m-d H:i:s');
            $checkSeeker->save();
            $seekerWorkExperience = SeekerWorkExperience::where('seeker_id', $seekerId)->where('company', $company)->first();
            if($seekerWorkExperience)
            {
                $array['success'] = false;
                $array['message'] = "You have already added this company details.";
                $array['data']    = (object)array();
                return $array;
            }
            else
            {
                $seekerworkExp = new SeekerWorkExperience;
                $seekerworkExp->seeker_id      = $seekerId;
                $seekerworkExp->designation    = $designation;
                $seekerworkExp->company        = $company;
                $seekerworkExp->salary         = $salary;
                $seekerworkExp->salary_in      = $salaryIn;
                $seekerworkExp->join_date      = $joinDate;
                $seekerworkExp->leave_date     = $leaveDate;
                $seekerworkExp->notice_period  = $noticePeriod;
                $seekerworkExp->experience     = $workExperienceYear;
                $seekerworkExp->created_at     = date('Y-m-d H:i:s');
                $seekerworkExp->updated_at     = date('Y-m-d H:i:s');
                $seekerworkExp->save();

                $seekerWorkExperienceDetails = SeekerWorkExperience::where('seeker_id', $seekerId)->get();
                foreach ($seekerWorkExperienceDetails as $key => $value) 
                {
                    $value->experience = $value->experience." Year";
                }

                $array['success'] = true;
                $array['message'] = "Work experience details";
                $array['data']    = $seekerWorkExperienceDetails;
                return $array;
            }
        }
        else if ($detailsType == 'resume') 
        {
            if (isset($data['resume']) && $data['resume']) 
            {
                $file       = $request->file('resume');
                $extension  = $file->getClientOriginalExtension();
                $resumeName = $checkSeeker->mobile.'_resume_'.time().'.'.$extension;
                $file->move(public_path('/resumes'), $resumeName);
                $resumeUrl  = '/resumes/'.$resumeName;

                $checkSeeker->resume    = $resumeUrl;
                $checkSeeker->updated_at = date('Y-m-d H:i:s');
                $checkSeeker->save();

                $array['success'] = true;
                $array['message'] = "Resume Uplaod";
                $array['data']    = $checkSeeker;
                return $array;
            }
            else
            {
                $array['success'] = false;
                $array['message'] = "Resume Required.";
                $array['data']    = (object)array();
                return $array;
            }
        }
        else if ($detailsType == 'summary') 
        {
            if (isset($data['about_me']) && $data['about_me']) 
            {
                $checkSeeker->about_me    = $data['about_me'];
                $checkSeeker->updated_at = date('Y-m-d H:i:s');
                $checkSeeker->save();

                $array['success'] = true;
                $array['message'] = "Summery Update";
                $array['data']    = $checkSeeker;
                return $array;
            }
            else
            {
                $array['success'] = false;
                $array['message'] = "Summery Required.";
                $array['data']    = (object)array();
                return $array;
            }
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Details type not valid";
            $array['data']    = (object)array();
            return $array;
        }   
        
    }


    # Function : This function used to fetch counts of seeker saved jobs, applied jobs ...etc
    # Request  : seeker id
    # Response : success true/false json response
    # Author   : Brijendra
    public function home(Request $request)
    {
        $data = $request->all();
       
        if (isset($data['seeker_id']) && $data['seeker_id'] ) 
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Seeker id is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        $seekerDetails = Seeker::where('id', $seekerId)->first();
        if ($seekerDetails != '') 
        {
            $profileCompletePercentage = $this->seekerProfilePercentage($seekerId);
            $baseUrl = url('/');
            $profile = array();
            
            $profile['id']      = $seekerDetails->id;
            $profile['name']    = $seekerDetails->name;
            $profile['mobile']  = $seekerDetails->mobile;
            $profile['email']   = $seekerDetails->email;
            if($seekerDetails->profile_image)
            {
                $profile['image']   = $baseUrl.$seekerDetails->profile_image;
            }
            else
            {
                $profile['image']   = '';
            }
            $profile['profile_percent'] = $profileCompletePercentage;
            
            $savedJobs      = SaveJob::where('seeker_id', $seekerId)->get();
            $applyJobs      = ApplyJob::where('seeker_id', $seekerId)->get();

            $applyJobIdsArray = array();
            foreach ($applyJobs as $key => $value) 
            {
                array_push($applyJobIdsArray, $value['job_id']);
            }

            $appliedJobs    = Jobs::whereIn('id', $applyJobIdsArray)->get();

            $seekerExperience = SeekerWorkExperience::where('seeker_id', $seekerId)->orderBy('id','DESC')->first();
            $designation = '';
            if($seekerExperience && $seekerExperience->designation)
            {
                $designation = $seekerExperience->designation;
                $recommandJobs  = Jobs::where('designation', $designation)->where('is_job_open','Open')->where('is_job_delete',0)->get();
            }
            else
            {
                $recommandJobs  = Jobs::where('is_job_open','Open')->where('is_job_delete',0)->get();
            }

            $scheduledInterview = JobInterview::where('schedule_time','!=', 'NULL')->where('schedule_time','!=', '0000-00-00 00:00:00')->where('seeker_id', $seekerId)->count();
            $selectedInterview = JobInterview::where('selected_time','!=', 'NULL')->where('selected_time','!=', '0000-00-00 00:00:00')->where('seeker_id', $seekerId)->count();
            $rejectedInterview = JobInterview::where('rejected_time','!=', 'NULL')->where('rejected_time','!=', '0000-00-00 00:00:00')->where('seeker_id', $seekerId)->count();

            $mainArray      = array(
                "profile_details"       => $profile, 
                "saved_job"             => count($savedJobs), 
                "applied_job"           => count($applyJobs), 
                "interviewed"           => 0, 
                "recommended_jobs"      => count($recommandJobs), 
                "job_status"            => $appliedJobs, 
                "scheduled_interview"   => $scheduledInterview, 
                "selected_by_company"   => $selectedInterview, 
                "reject_by_company"     => $rejectedInterview
            );

            $array['success'] = true;
            $array['message'] = "Record found";
            if(count($appliedJobs) < 1)
            {
                $array['message'] = "Looks like you haven’t applied for a Job yet.";
            }
            $array['data']    = $mainArray;
            return $array;
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Seeker not exist";
            $array['data']    = (object)array();
            return $array;
        }
    
        
    }


    # Function : This function used to update notification setting
    # Request  : seeker id, job update, recruiter update, general update, apply update
    # Response : success true/false json response
    # Author   : Brijendra
    public function updateNotificationSetting(Request $request)
    {
        $data = $request->all();
       
        if (isset($data['seeker_id']) && $data['seeker_id'] ) 
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Seeker id is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        $jobUpdate = 1;
        if (isset($data['job_update']) && $data['job_update'] != '') 
        {
            $jobUpdate = (int)$data['job_update'];
        }

        $recruiterUpdate = 1;
        if (isset($data['recruiter_update']) && $data['recruiter_update'] != '' ) 
        {
            $recruiterUpdate = $data['recruiter_update'];
        }

        $generalUpdate = 1;
        if (isset($data['general_update']) && $data['general_update'] != '') 
        {
            $generalUpdate = (int)$data['general_update'];
        }

        $applyUpdate = 1;
        if (isset($data['apply_update']) && $data['apply_update'] != '') 
        {
            $applyUpdate = (int)$data['apply_update'];
        }

        $profileUpdate = 1;
        if (isset($data['profile_update']) && $data['profile_update'] != '') 
        {
            $profileUpdate = (int)$data['profile_update'];
        }

        $checkSetting = NotificationSetting::where('seeker_id', $seekerId)->first();
        if ($checkSetting != '') 
        {
            $checkSetting->job_update        = $jobUpdate;
            $checkSetting->recruiter_update  = $recruiterUpdate;
            $checkSetting->general_update    = $generalUpdate;
            $checkSetting->apply_update      = $applyUpdate;
            $checkSetting->profile_update    = $profileUpdate;
            $checkSetting->save();

            $array['success'] = true;
            $array['message'] = "Setting Update";
            $array['data']    = (object)array();
            return $array;
        }
        else
        {
            $notificationSetting  = new NotificationSetting();
            $notificationSetting->seeker_id         = $seekerId;
            $notificationSetting->job_update        = $jobUpdate;
            $notificationSetting->recruiter_update  = $recruiterUpdate;
            $notificationSetting->general_update    = $generalUpdate;
            $notificationSetting->apply_update      = $applyUpdate;
            $notificationSetting->profile_update    = $profileUpdate;
            $notificationSetting->save();

            $array['success'] = true;
            $array['message'] = "Setting Update";
            $array['data']    = (object)array();
            return $array;
        }
        
    }


    # Function : This function used to fetch recommended jobs list
    # Request  : seeker id ,is filter, filter by
    # Response : success true/false json response
    # Author   : Brijendra
    public function getRecommendedJobs(Request $request)
    {

        $data = $request->all();
       
        if (isset($data['seeker_id']) && $data['seeker_id'] ) 
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Seeker id is Required.";
            $array['data'] = (object)array();
            return $array;
        }
        $seekerExperience = SeekerWorkExperience::where('seeker_id', $seekerId)->orderBy('id','DESC')->first();
        $designation = '';
        if($seekerExperience)
        {
            $designation = $seekerExperience->designation;
        }

        $isFilter = 'false';
        if (isset($data['is_filter']) && $data['is_filter'] ) 
        {
            $isFilter = $data['is_filter'];
        }
        $applyJobs = ApplyJob::where('seeker_id', $seekerId)->get()->keyBy('job_id');
        $skillsDetails = Skill::get()->keyBy('id')->toArray();

        if ($isFilter == 'true') 
        {
            $filterBy = "id";
            if (isset($data['filter_by']) && $data['filter_by'] ) 
            {
                $filterBy = $data['filter_by'];
            }

            if ($filterBy == 'low_salary') 
            {
                if($designation)
                {
                    $jobs = Jobs::where('designation', $designation)->where('is_job_open','Open')->where('is_job_delete',0)->orderBy('min_salary', 'asc')->get();
                }
                else
                {
                    $jobs = Jobs::where('is_job_open','Open')->where('is_job_delete',0)->orderBy('min_salary', 'asc')->get();
                }
            }
            else if ($filterBy == 'high_salary') 
            {
                if($designation)
                {
                     $jobs = Jobs::where('designation', $designation)->where('is_job_open','Open')->where('is_job_delete',0)->orderBy('min_salary', 'desc')->get();
                }
                else
                {
                    $jobs = Jobs::where('is_job_open','Open')->where('is_job_delete',0)->orderBy('min_salary', 'desc')->get();
                }
            }
            else if ($filterBy == 'location') 
            {
                if($designation)
                {
                    $jobs = Jobs::where('designation', $designation)->where('is_job_open','Open')->where('is_job_delete',0)->orderBy('job_location', 'asc')->get();
                }
                else
                {
                    $jobs = Jobs::where('is_job_open','Open')->where('is_job_delete',0)->orderBy('job_location', 'asc')->get();
                }
            }
            else
            {
                if($designation)
                {
                    $jobs = Jobs::where('designation', $designation)->where('is_job_open','Open')->where('is_job_delete',0)->orderBy('id', 'desc')->get();
                }
                else
                {
                    $jobs = Jobs::where('is_job_open','Open')->where('is_job_delete',0)->orderBy('id', 'desc')->get();
                }
            }

            $savedJobs = SaveJob::where('seeker_id', $seekerId)->get()->keyBy('job_id');


            foreach ($jobs as $key => $value) 
            {
                if(isset($savedJobs[$value->id]))
                {
                    $value['is_saved']  = true;
                }
                else
                {
                    $value['is_saved']  = false;
                }

                if(isset($applyJobs[$value->id]))
                {
                    $value['is_applied']  = true;
                }
                else
                {
                    $value['is_applied']  = false;
                }

                $skillIds = $value['skill_ids'];
                $value['all_skills'] = '';
                if($skillIds)
                {
                    $skills = '';
                    $skillIds = explode(',', $skillIds);
                    foreach ($skillIds as $key => $skillval) 
                    {
                        if(isset($skillsDetails[$skillval]))
                        {
                            if($skills)
                            {
                                $skills .= ', '. $skillsDetails[$skillval]['skill'];
                            }
                            else
                            {
                                $skills = $skillsDetails[$skillval]['skill'];
                            }
                        }
                    }
                    
                    $value['all_skills'] = $skills;
                }

                $value['created_date'] = date('d-m-Y', strtotime($value->created_at));
            }

            $array['success'] = true;
            $array['message'] = "Record Found";
            $array['data']    = $jobs;
            return $array;
        }
        else
        {
            if($designation)
            {
                $jobs = Jobs::where('designation', $designation)->where('is_job_open','Open')->where('is_job_delete',0)->orderBy('id', 'desc')->get();
            }
            else
            {
                $jobs = Jobs::where('is_job_open','Open')->where('is_job_delete',0)->orderBy('id', 'desc')->get();
            }
            $savedJobs = SaveJob::where('seeker_id', $seekerId)->get()->keyBy('job_id');

            foreach ($jobs as $key => $value) 
            {
                if(isset($savedJobs[$value->id]))
                {
                    $value['is_saved']  = true;
                }
                else
                {
                    $value['is_saved']  = false;
                }

                if(isset($applyJobs[$value->id]))
                {
                    $value['is_applied']  = true;
                }
                else
                {
                    $value['is_applied']  = false;
                }

                $skillIds = $value['skill_ids'];
                $value['all_skills'] = '';
                if($skillIds)
                {
                    $skills = '';
                    $skillIds = explode(',', $skillIds);
                    foreach ($skillIds as $key => $skillval) 
                    {
                        if(isset($skillsDetails[$skillval]))
                        {
                            if($skills)
                            {
                                $skills .= ', '. $skillsDetails[$skillval]['skill'];
                            }
                            else
                            {
                                $skills = $skillsDetails[$skillval]['skill'];
                            }
                        }
                    }
                    $value['all_skills'] = $skills;
                }

                $value['created_date'] = date('d-m-Y', strtotime($value->created_at));
            }

            $array['success'] = true;
            $array['message'] = "Record Found";
            $array['data']    = $jobs;
            return $array;
            
        }

    }


    # Function : This function used to fetch applied jobs list
    # Request  : seeker id
    # Response : success true/false json response
    # Author   : Brijendra
    public function getAppledJobs(Request $request)
    {
        $data = $request->all();

        if (isset($data['seeker_id']) && $data['seeker_id'] ) 
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Seeker id is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        $applyJobs = ApplyJob::where('seeker_id', $seekerId)->get()->keyBy('job_id');
        $jobIdsArray = array();

        foreach ($applyJobs as $key => $value) 
        {
            array_push($jobIdsArray, $value['job_id']);
        }

        $jobsList = Jobs::whereIn('id', $jobIdsArray)->get();
        $savedJobs = SaveJob::where('seeker_id', $seekerId)->get()->keyBy('job_id');
        foreach ($jobsList as $key => $value) 
        {
            if(isset($savedJobs[$value->id]))
            {
                $value['is_saved']  = true;
            }
            else
            {
                $value['is_saved']  = false;
            }

            if(isset($applyJobs[$value->id]))
            {
                $value['is_applied']  = true;
            }
            else
            {
                $value['is_applied']  = false;
            }

            $value['created_date'] = date('d-m-Y', strtotime($value->created_at));
        }

        $array['success'] = true;
        if(count($jobsList) > 0)
        {
            $array['message'] = "Record Found";
        }
        else
        {
            $array['message'] = "Haven’t applied for a Job yet?";
        }
        $array['data']    = $jobsList;
        return $array;
    }

    # Function : This function used to fetch applied jobs list
    # Request  : seeker id
    # Response : success true/false json response
    # Author   : Brijendra
    public function getSavedJobs(Request $request)
    {
        $data = $request->all();

        if (isset($data['seeker_id']) && $data['seeker_id'] ) 
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Seeker id is Required.";
            $array['data'] = (object)array();
            return $array;
        }
        $applyJobsDetails = ApplyJob::where('seeker_id', $seekerId)->get()->keyBy('job_id');
        $applyJobs   = SaveJob::where('seeker_id', $seekerId)->get();
        $jobIdsArray = array();
        foreach ($applyJobs as $key => $value) 
        {
            array_push($jobIdsArray, $value['job_id']);
        }

        $jobsList = Jobs::whereIn('id', $jobIdsArray)->get();

        foreach ($jobsList as $key => $value) 
        {
            if(isset($applyJobsDetails[$value->id]))
            {
                $value['is_applied']  = true;
            }
            else
            {
                $value['is_applied']  = false;
            }
            $value['created_date'] = date('d-m-Y', strtotime($value->created_at));
        }

        $array['success'] = true;
        if(count($jobsList) > 0)
        {
            $array['message'] = "Record Found";
        }
        else
        {
            $array['message'] = "We’ve got a bunch of Job Opportunities you could explore.";
        }
        $array['data']    = $jobsList;
        return $array;

    }

    # Function : This function used to add seeker profile image
    # Request  : seeker id, image
    # Response : success true/false json response
    # Author   : Brijendra
    public function addProfileImage(Request $request)
    {
        $data = $request->all();

        if (isset($data['seeker_id']) && $data['seeker_id'] ) 
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Seeker Id is Required.";
            $array['data']    = (object)array();
            return $array;
        }

        $checkSeeker = Seeker::where('id', $seekerId)->first();
        if ($checkSeeker == '') 
        {
            $array['success'] = false;
            $array['message'] = "User not exist !!";
            $array['data']    = (object)array();
            return $array;
        }
        else
        {
            if (isset($data['image']) && $data['image'] ) 
            {
                $file       = $request->file('image');
                $extension  = $file->getClientOriginalExtension();
                $imageName  = $checkSeeker->mobile.'_user_'.time().'.'.$extension;
                $file->move(public_path('/seekerProfileImages'), $imageName);
                $imageUrl  = '/seekerProfileImages/'.$imageName;
                
                $checkSeeker->profile_image = $imageUrl;
                $checkSeeker->save();
                
                $array['success'] = true;
                $array['message'] = "Seeker image uploaded !!";
                $array['data']    = (object)array();
                return $array;
            }
            else
            {
                $array['success'] = false;
                $array['message'] = "image is Required.";
                $array['data']    = (object)array();
                return $array;
            }
            
        }

    }

    # Function : This function used to apply job
    # Request  : seeker id, job id
    # Response : success true/false json response
    # Author   : Brijendra
    public function applyForJob(Request $request)
    {
        $data = $request->all();

        if (isset($data['seeker_id']) && $data['seeker_id'] ) 
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Seeker id is Required.";
            $array['data']    = (object)array();
            return $array;
        }

        if (isset($data['job_id']) && $data['job_id'] ) 
        {
            $jobId = $data['job_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Job id is Required.";
            $array['data']    = (object)array();
            return $array;
        }

        $checkApplyJobs = ApplyJob::where('seeker_id', $seekerId)->where('job_id', $jobId)->first();
        if ($checkApplyJobs == '') 
        {
            $applyJobs            = new ApplyJob;
            $applyJobs->seeker_id = $seekerId;
            $applyJobs->job_id    = $jobId;
            $applyJobs->save();
            
            $array['success'] = true;
            $array['message'] = "Successfully applied for this job";
            $array['data']    = (object)array();
            return $array;
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Already applied";
            $array['data']    = (object)array();
            return $array;   
        }

    }

    # Function : This function is used to save job
    # Request  : seeker id, job id
    # Response : success true/false json response
    # Author   : Brijendra
    public function saveJob(Request $request)
    {
        $data = $request->all();

        if (isset($data['seeker_id']) && $data['seeker_id'] ) 
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Seeker id is Required.";
            $array['data']    = (object)array();
            return $array;
        }

        if (isset($data['job_id']) && $data['job_id'] ) 
        {
            $jobId = $data['job_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Job id is Required.";
            $array['data']    = (object)array();
            return $array;
        }

        if (isset($data['is_save']) && $data['is_save'] ) 
        {
            $isSave = $data['is_save'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Flag Required.";
            $array['data']    = (object)array();
            return $array;
        }

        $checkSaveJobs = SaveJob::where('seeker_id', $seekerId)->where('job_id', $jobId)->first();
        if ($isSave == 'true') 
        {
            if ($checkSaveJobs == '') 
            {
                $saveJob            = new SaveJob;
                $saveJob->seeker_id = $seekerId;
                $saveJob->job_id    = $jobId;
                $saveJob->save();
                
                $array['success'] = true;
                $array['message'] = "Successfully save this job";
                $array['data']    = (object)array();
                return $array;
                
            }
            else
            {
                $array['success'] = false;
                $array['message'] = "Already saved";
                $array['data']    = (object)array();
                return $array;   
            }
        }
        else
        {
            if ($checkSaveJobs != '') 
            {
                SaveJob::where('seeker_id', $seekerId)->where('job_id', $jobId)->delete();
                
                $array['success'] = true;
                $array['message'] = "Successfully unsave this job";
                $array['data']    = (object)array();
                return $array;
                
            }
            else
            {
                $array['success'] = false;
                $array['message'] = "You are not able to unsave because job not saved before.";
                $array['data']    = (object)array();
                return $array;   
            }
        }


    }


    # Function : This function is used to search job
    # Request  : seeker id, job id
    # Response : success true/false json response
    # Author   : Brijendra
    public function searchJob(Request $request)
    {
        $data = $request->all();

        if (isset($data['seeker_id']) && $data['seeker_id'] ) 
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Seeker id is Required.";
            $array['data']    = (object)array();
            return $array;
        }

        $isFilter = 'false';
        if (isset($data['is_filter']) && $data['is_filter'] ) 
        {
            $isFilter = $data['is_filter'];
        }

        $skillsOrDesignation = '';
        if (isset($data['skill_or_designation']) && $data['skill_or_designation'] ) 
        {
            $skillsOrDesignation = $data['skill_or_designation'];
        }

        $location = '';
        if (isset($data['location']) && $data['location'] ) 
        {
            $location = $data['location'];
        }

        $experience = '';
        if (isset($data['experience']) && $data['experience'] ) 
        {
            $experience = (int)$data['experience'];
        }

        $salary = '';
        if (isset($data['salary']) && $data['salary'] ) 
        {
            $salary = (int)$data['salary'];
        }

        $query = Jobs::select('*');
        if ($skillsOrDesignation != '') 
        {
            $query->where('designation','LIKE', '%'.$skillsOrDesignation.'%')->orWhere('skills','LIKE', '%'.$skillsOrDesignation.'%');
        }

        if ($location != '') 
        {
            $query->where('job_location','LIKE', $location.'%');
        }

        if ($experience != '') 
        {
            $query->where('max_experience','<=', $experience);
        }

        if ($salary != '') 
        {
            $query->where('min_salary','<=', $salary);
        }

        if ($isFilter == 'true') 
        {
            $relevance = '';
            if (isset($data['relevance']) && $data['relevance'] ) 
            {
                $relevance = $data['relevance'];
            }

            $recent = '';
            if (isset($data['recent']) && $data['recent'] ) 
            {
                $recent = $data['recent'];
            }

            if ($relevance != "") 
            {    
                $query->orderBy("id", "asc");
            }
            
            if($recent != "recent")
            {
                $query->orderBy("id", "desc");
            }

            $jobs = $query->get();
        }
        else
        {
            $jobs = $query->get();
        }

        $jobIdsArray = array();
        foreach ($jobs as $key => $value) 
        {
            array_push($jobIdsArray, $value->id);
        }
        $savedJobs = SaveJob::where('seeker_id', $seekerId)->get()->keyBy('job_id');

        $applyJobs = ApplyJob::whereIn('job_id', $jobIdsArray)->where('seeker_id', $seekerId)->get()->keyBy('job_id');


        foreach ($jobs as $key => $value) 
        {
            if(isset($savedJobs[$value->id]))
            {
                $value['is_saved']  = true;
            }
            else
            {
                $value['is_saved']  = false;
            }

            if(isset($applyJobs[$value->id]))
            {
                $value['is_applied']  = true;
            }
            else
            {
                $value['is_applied']  = false;
            }

            $value['created_date'] = date('d-m-Y', strtotime($value->created_at));
        }

        $array['success'] = true;
        if(count($jobs) > 0)
        {
            $array['message'] = "Record Found";
        }
        else
        {
            $array['message'] = "No Jobs for your skills at the moment. We’re working hard & continuously to ensure we get the perfect organization to place you in.";
        }
        $array['data']    = $jobs;
        $array['count']   = count($jobs);
        return $array;

    }


    # Function : This function is used to fetch the list of notifications
    # Request  : seeker id
    # Response : success true/false json response
    # Author   : Brijendra
    public function notificationList(Request $request)
    {
        $data = $request->all();

        if (isset($data['seeker_id']) && $data['seeker_id'] ) 
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Seeker id is Required.";
            $array['data']    = (object)array();
            return $array;
        }

        $jobNotifications = JobNotification::where('seeker_id', $seekerId)->get();
        $jobIdsArray = array();
        foreach ($jobNotifications as $key => $value) 
        {
            array_push($jobIdsArray, $value['job_id']);
        }

        $jobs = Jobs::whereIn('id', $jobIdsArray)->get();
        $applyJobs = ApplyJob::whereIn('job_id', $jobIdsArray)->where('seeker_id', $seekerId)->get()->keyBy('job_id');

        foreach ($jobs as $key => $value) 
        {
            if(isset($applyJobs[$value->id]))
            {
                $value['is_applied']  = true;
            }
            else
            {
                $value['is_applied']  = false;
            }
        }

        $array['success'] = true;
        if(count($jobs) > 0)
        {
            $array['message'] = "Record Found";
        }
        else
        {
            $array['message'] = "None – at the moment.";
        }
        $array['data']    = $jobs;
        return $array;
    }


    # Function : This function is used to set profile visibility to recuiters
    # Request  : seeker id, visibility
    # Response : success true/false json response
    # Author   : Brijendra
    public function setProfileVisibility(Request $request)
    {
        $data = $request->all();

        if (isset($data['seeker_id']) && $data['seeker_id'] != '') 
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Seeker id is Required.";
            $array['data']    = (object)array();
            return $array;
        }

        if (isset($data['visibility']) && $data['visibility'] != '') 
        {
            $visibility = (int)$data['visibility'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Visibility flag Required.";
            $array['data']    = (object)array();
            return $array;
        }


        $seeker = Seeker::where('id', $seekerId)->first();
        if ($seeker != '') 
        {
            $seeker->profile_visibility  = $visibility;
            $seeker->save();
            
            $array['success'] = true;
            $array['message'] = "Visibilty Set";
            $array['data']    = (object)array();
            return $array;
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "User not exixt";
            $array['data']    = (object)array();
            return $array;
        }


    }


    # Function : This function is used to update personal detail if it is verified user
    # Request  : seeker_id, mobile, name, dob, language, location
    # Response : success true/false json response
    # Author   : Brijendra
    public function updatePersonalDetails(Request $request)
    {
        $data = $request->all();
        if (isset($data['seeker_id']) && $data['seeker_id'] != '') 
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Seeker id Required.";
            $array['data']    = (object)array();
            return $array;
        }

        if (isset($data['mobile']) && $data['mobile'] ) 
        {
            $mobile = $data['mobile'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Mobile number Required.";
            $array['data']    = (object)array();
            return $array;
        }

        $name = '';
        if (isset($data['name']) && $data['name'] ) 
        {
            $name = $data['name'];
        }

        $dob = '';
        if (isset($data['dob']) && $data['dob'] ) 
        {
            $dob = $data['dob'];
        }

        $language = '';
        if (isset($data['language']) && $data['language'] ) 
        {
            $language = $data['language'];
        }

        $location = '';
        if (isset($data['location']) && $data['location'] ) 
        {
            $location = $data['location'];
        }

        $preferredLocation = '';
        if (isset($data['preferred_location']) && $data['preferred_location'] ) 
        {
            $preferredLocation = $data['preferred_location'];
        }

        $address = '';
        if (isset($data['address']) && $data['address'] ) 
        {
            $address = $data['address'];
        }

        $gender = '';
        if (isset($data['gender']) && $data['gender'] ) 
        {
            $gender = $data['gender'];
        }

        $casteCategory = '';
        if (isset($data['caste_category']) && $data['caste_category'] ) 
        {
            $casteCategory = $data['caste_category'];
        }

        $maritalStatus = '';
        if (isset($data['marital_status']) && $data['marital_status'] ) 
        {
            $maritalStatus = $data['marital_status'];
        }

        $physicalyChalanged = 'No';
        if (isset($data['physicaly_challenge']) && $data['physicaly_challenge'] ) 
        {
            $physicalyChalanged = $data['physicaly_challenge'];
        }

        $checkSeeker = Seeker::where('id', $seekerId)->first();
        if ($checkSeeker != '') 
        {
            if ($checkSeeker->mobile != $mobile) 
            {

                $message = "Your GetMeJob mobile number verification OTP is : ";
                $result  = $this->sendOtp($mobile, 'personal-details', 'seeker', $message);

                if ($result != 'Failed') 
                {
                    $result = array("is_verified" => false);

                    $array['success'] = true;
                    $array['message'] = "OTP Sent";
                    $array['data']    = $result;
                    return $array;
                }
                else
                {
                    $array['success']  = false;
                    $array['message']  = "OTP sending failed";
                    $array['data']     = (object)array();
                    return $array;
                }

            }
            else
            {
                
                $checkSeeker->name          = $name;
                $checkSeeker->mobile        = $mobile;
                $checkSeeker->language      = $language;
                $checkSeeker->location      = $location;
                $checkSeeker->preferred_location  = $preferredLocation;
                $checkSeeker->is_verified   = 1;
                $checkSeeker->date_of_birth = date('Y-m-d', strtotime($dob));
                $checkSeeker->address       = $address;
                $checkSeeker->gender        = $gender;
                $checkSeeker->marital_status      = $maritalStatus;
                $checkSeeker->physicaly_challenge = $physicalyChalanged;
                $checkSeeker->caste_category = $casteCategory;
                $checkSeeker->updated_at = date('Y-m-d H:i:s');
                $checkSeeker->save();
                
                $checkSeeker['is_verified'] = true;
                $array['success'] = true;
                $array['message'] = "personal detail update";
                $array['data']    = $checkSeeker;
                return $array;
            }
        }
        else
        {
            $array['success']  = false;
            $array['message']  = "User not exist";
            $array['data']     = (object)array();
            return $array;
        }

    }


    # Function : This function used to verify otp and if verified then save personal details
    # Request  : mobile, email, password, otp, name
    # Response : success true/false json response
    # Author   : Brijendra
    public function verifyOtpToUpdatePersonalDetail(Request $request)
    {

        $data       = $request->all();

        $name       = NULL;
        $mobile     = NULL;
        $email      = NULL;
        $password   = NULL;
        $year       = '';
        $month      = '';
        $token      = '';

        if (isset($data['otp']) && $data['otp'] ) 
        {
            $otp = $data['otp'];

            $verifyOtp = OtpVerification::where('otp', $otp)->where('otp_screen', 'personal-details')->first();

            if ($verifyOtp != '') 
            {

                if (isset($data['seeker_id']) && $data['seeker_id'] ) 
                {
                    $seekerId = $data['seeker_id'];
                }
                else
                {
                    $array['success'] = false;
                    $array['message'] = "Seeker id Required.";
                    $array['data']    = (object)array();
                    return $array;
                }

                if (isset($data['mobile']) && $data['mobile'] ) 
                {
                    $mobile = $data['mobile'];
                }
                else
                {
                    $array['success'] = false;
                    $array['message'] = "Mobile number Required.";
                    $array['data']    = (object)array();
                    return $array;
                }

                $name = '';
                if (isset($data['name']) && $data['name'] ) 
                {
                    $name = $data['name'];
                }

                $dob = '';
                if (isset($data['dob']) && $data['dob'] ) 
                {
                    $dob = $data['dob'];
                }

                $language = '';
                if (isset($data['language']) && $data['language'] ) 
                {
                    $language = $data['language'];
                }

                $location = '';
                if (isset($data['location']) && $data['location'] ) 
                {
                    $location = $data['location'];
                }

                $preferredLocation = '';
                if (isset($data['preferred_location']) && $data['preferred_location'] ) 
                {
                    $preferredLocation = $data['preferred_location'];
                }

                $address = '';
                if (isset($data['address']) && $data['address'] ) 
                {
                    $address = $data['address'];
                }

                $gender = '';
                if (isset($data['gender']) && $data['gender'] ) 
                {
                    $gender = $data['gender'];
                }

                $maritalStatus = '';
                if (isset($data['marital_status']) && $data['marital_status'] ) 
                {
                    $maritalStatus = $data['marital_status'];
                }

                $casteCategory = '';
                if (isset($data['caste_category']) && $data['caste_category'] ) 
                {
                    $casteCategory = $data['caste_category'];
                }

                $physicalyChalanged = 'No';
                if (isset($data['physicaly_challenge']) && $data['physicaly_challenge'] ) 
                {
                    $physicalyChalanged = $data['physicaly_challenge'];
                }

                $checkSeeker = Seeker::where('id', $seekerId)->first();

                $checkSeeker->name          = $name;
                $checkSeeker->mobile        = $mobile;
                $checkSeeker->language      = $language;
                $checkSeeker->location      = $location;
                $checkSeeker->preferred_location  = $preferredLocation;
                $checkSeeker->is_verified   = 1;
                $checkSeeker->date_of_birth = date('Y-m-d', strtotime($dob));
                $checkSeeker->address       = $address;
                $checkSeeker->gender        = $gender;
                $checkSeeker->marital_status      = $maritalStatus;
                $checkSeeker->physicaly_challenge = $physicalyChalanged;
                $checkSeeker->caste_category = $casteCategory;
                $checkSeeker->save();

                OtpVerification::where('otp', $otp)->where('otp_screen', 'personal-details')->delete();
                $checkSeeker['is_verified'] = true;
                $array['success'] = true;
                $array['message'] = "OTP Verified personal detail update successfully.";
                $array['data']    = $checkSeeker;
                return $array;
                
            }
            else
            {
                $array['success'] = false;
                $array['message'] = "otp not verified.";
                $array['data'] = (object)array();
                return $array;
            }

        }
        else
        {
            $array['success'] = false;
            $array['message'] = "otp is Required.";
            $array['data'] = (object)array();
            return $array;
        }
                
    }

    # Function : This function used to reset password
    # Request  : id, password
    # Response : success true/false json response
    # Author   : Brijendra
    public function changePassword(Request $request)
    {
        $data = $request->all();
       
        if (isset($data['seeker_id']) && $data['seeker_id'] ) 
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Seeker id is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        if (isset($data['current_password']) && $data['current_password'] ) 
        {
            $currentPassword = md5($data['current_password']);
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Current Password is Required.";
            $array['data'] = (object)array();
            return $array;
        }
        
        if (isset($data['new_password']) && $data['new_password'] ) 
        {
            $newPassword = md5($data['new_password']);
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "New Password is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        $checkUser = Seeker::where('id', $seekerId)->first();

        if ($checkUser != '') 
        {
            if ($checkUser->password == $currentPassword) 
            {
                $checkUser->password = $newPassword;
                $checkUser->save();

                $array['success'] = true;
                $array['message'] = "Password successfully update";
                $array['data']    = (object)array();
                return $array;
            }
            else
            {
                $array['success'] = false;
                $array['message'] = "Current password is incorrect";
                $array['data']    = (object)array();
                return $array;
            }

        }
        else
        {
            $array['success'] = false;
            $array['message'] = "User no exist";
            $array['data'] = (object)array();
            return $array;
        }
        
    }

    # Function : This function used to save seeker skill
    # Request  : seeker id, skills
    # Response : success true/false json response
    # Author   : Brijendra
    public function addSeekerSkills(Request $request)
    {
        $data = $request->all();
       
        if (isset($data['seeker_id']) && $data['seeker_id'] ) 
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Seeker id is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        if (isset($data['industry_id']) && $data['industry_id'] ) 
        {
            $industryId = $data['industry_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Industry id required.";
            $array['data']    = (object)array();
            return $array;
        }

        $skills = "";
        if (isset($data['skills']) && $data['skills'] ) 
        {
            $skills = $data['skills'];
        }
        // else
        // {
        //     $array['success'] = false;
        //     $array['message'] = "Skills Required.";
        //     $array['data'] = (object)array();
        //     return $array;
        // }

        if (isset($data['other_skills']) && $data['other_skills'] ) 
        {
            $extraSkill = $data['other_skills'];
            $extraSkillArray = explode(",",$extraSkill);
            
            SeekerAdditionalSkill::where('seeker_id', $seekerId)->delete();

            foreach ($extraSkillArray as $key => $value) 
            {
                $addNewSkill = new SeekerAdditionalSkill;
                $addNewSkill->industry_id = $industryId;
                $addNewSkill->seeker_id   = $seekerId;
                $addNewSkill->skill       = $value;
                $addNewSkill->save();
            }
        }
    

        $checkUser = Seeker::where('id', $seekerId)->first();

        if ($checkUser != '') 
        {
            // if ($skills != '') 
            // {
                $checkUser->skill_ids = $skills;
                $checkUser->updated_at = date('Y-m-d H:i:s');
                $checkUser->save();
            // }

            $array['success'] = true;
            $array['message'] = "Skills added";
            $array['data']    = (object)array();
            return $array;
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "User no exist";
            $array['data'] = (object)array();
            return $array;
        }
        
    }


    public function deleteNumber(Request $request)
    {
        $data  = $request->all();

        if (isset($data['mobile']) && $data['mobile'] ) 
        {
            $mobile = $data['mobile'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Mobile is Required.";
            $array['data']    = (object)array();
            return $array;
        }

        Seeker::where('mobile', $mobile)->delete();

        return "Deleted";
    }




    public function year(Request $request)
    {
        $year = [ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '15+' ];
        
        $array['success'] = true;
        $array['message'] = "Get experience year ...";
        $array['data'] = $year;
        return $array;
    }

    public function month(Request $request)
    {
        $month = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11' ];
        $array['success'] = true;
        $array['message'] = "Get experience month ...";
        $array['data'] = $month;
        return $array;
    }

    public function insertData(Request $request)
    {
        $month = array(
            'Assamese / Asomiya',
            'Bengali / Bangla',
            'English',
            'Gujarati',
            'Hindi',
            'Kannada',
            'Kashmiri',
            'Konkani',
            'Malayalam',
            'Manipuri',
            'Marathi',
            'Oriya',
            'Punjabi',
            'Sanskrit',
            'Telugu',
            'Urdu',
            'other'
        );



        // foreach ($month as $key => $value) {
        //     $new = new SchoolMedium;
        //     $new->medium = $value;
        //     $new->save();
        // }
        $array['success'] = true;
        $array['message'] = "Get experience month ...";
        $array['data'] = $month;
        return $array;
    }


    public function autoDelete(Request $request)
    {

        $time   = Carbon::now()->subMinutes(3)->toDateTimeString();
        $delete = OtpVerification::where('created_at', '<', $time)->delete();
        $delete = ForgetPassword::where('created_at', '<', $time)->delete();

        return $delete;
    }

    # Function : This function used for work experience details
    # Response : success true json response
    # Author   : Rahul
    public function getWorkExperience()
    {
        $workExperienceDetails = WorkExperience::select('experience')->get();
        $array['success'] = true;
        $array['message'] = "Work experience details";
        $array['data'] = $workExperienceDetails;
        return $array;
    }

    # Function : This function used for salary details
    # Response : success true json response
    # Author   : Rahul
    public function getSalary()
    {
        $salaryDetails = Salary::select('salary')->get();
        $array['success'] = true;
        $array['message'] = "Salary details";
        $array['data'] = $salaryDetails;
        return $array;
    }

    # Function : This function used for verifi mobile number and update
    # Response : success true json response
    # Author   : Rahul
    public function mobileNumberVerification(Request $request)
    {
        $data = $request->all();
        $mobile = '';
        $userId = '';
        $otpType = '';
        $otp = '';

        if (isset($data['mobile']) && $data['mobile'] ) 
        {
            $mobile = $data['mobile'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Mobile number is Required.";
            $array['data']    = (object)array();
            return $array;
        }

        if (isset($data['user_id']) && $data['user_id'] ) 
        {
            $userId = $data['user_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "User id is Required.";
            $array['data']    = (object)array();
            return $array;
        }

        if (isset($data['otp']) && $data['otp'] ) 
        {
            $otp = $data['otp'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "OTP is Required.";
            $array['data']    = (object)array();
            return $array;
        }

        if (isset($data['otp_type']) && $data['otp_type'] ) 
        {
            $otpType = $data['otp_type'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "OTP type is Required.";
            $array['data']    = (object)array();
            return $array;
        }

        $verifyOtp = OtpVerification::where('otp', $otp)->where('otp_screen', $otpType)->orderBy('id','DESC')->first();

        if($verifyOtp == '')
        {
            $array['success'] = false;
            $array['message'] = "Invalid OTP.Please try again.";
            $array['data']    = (object)array();
            return $array;
        }

        $seekerDetails = Seeker::where('id', $userId)->first();
        if($seekerDetails)
        {
            $seekerDetails->mobile = $mobile;
            $seekerDetails->save();
            $array['success'] = true;
            $array['message'] = "Mobile number update successfully.";
            $array['data']    = (object)array();
            return $array;
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "User not registered.";
            $array['data']    = (object)array();
            return $array;
        }

    }

    # Function : This function used for send otp for verify mobile number
    # Response : success true json response
    # Author   : Rahul
    public function sendVerificationOTP(Request $request)
    {
        $data = $request->all();
        if (isset($data['mobile']) && $data['mobile'] ) 
        {
            $mobile = $data['mobile'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Mobile number is Required.";
            $array['data']    = (object)array();
            return $array;
        }

        if (isset($data['otp_type']) && $data['otp_type'] ) 
        {
            $otpType = $data['otp_type'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "OTP type is Required.";
            $array['data']    = (object)array();
            return $array;
        }

        $text = "your GetMeJob ".$otpType . " otp is ";
        $result = $this->sendOtp($mobile, $otpType, 'seeker', $text);
         if ($result != 'Failed') 
        {
            $array['success'] = true;
            $array['message'] = "OTP Sent";
            $array['data']    = (object)array();
            return $array;
        }
        else
        {
            $array['success']  = false;
            $array['message'] = "OTP sending failed";
            $array['data'] = (object)array();
            return $array;
        }
    }

    # Function : This function used for get specialization according to education sub categor id
    # Response : success true json response
    # Author   : Rahul
    public function getSpecialization(Request $request)
    {
        $data = $request->all();
        $educationSubCategoryId = '';

        if (isset($data['education_sub_category_id']) && $data['education_sub_category_id'] ) 
        {
            $educationSubCategoryId = $data['education_sub_category_id'];
        }
        
        if($educationSubCategoryId)
        {
            $specializeDetails = Specialization::select('id','specialize')->where('education_sub_category_id', $educationSubCategoryId)->get();
        }
        else
        {
            $specializeDetails = Specialization::select('id','specialize')->get();
        }

        $array['success']  = true;
        $array['message'] = "Specialization details";
        $array['data'] = $specializeDetails;
        return $array;

    }

    # Function : This function used for get terms and condition
    # Response : success true json response
    # Author   : Rahul
    public function termsAndCondition()
    {
        $termsAndCondition = TermsCondition::get();
        $array['success']  = true;
        $array['message']  = "Terms and condition details";
        $array['data'] = $termsAndCondition;
        return $array;
    }

    # Function : This function used for get privacy policy
    # Response : success true json response
    # Author   : Rahul
    public function privacyPolicy()
    {
        $privacyPolicy = PrivacyPolicy::get();
        $array['success']  = true;
        $array['message'] = "Privacy policy details";
        $array['data'] = $privacyPolicy;
        return $array;
    }

    # Function : This function used for get similar jobs according to designation
    # Response : success true json response
    # Author   : Rahul
    public function similarJobs(Request $request)
    {
        $data = $request->all();
        if(isset($data['designation']) && $data['designation'])
        {
            $designation = $data['designation'];
        }
        else
        {
            $array['success']  = false;
            $array['message'] = "Designation is required.";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['job_id']) && $data['job_id'])
        {
            $jobId = $data['job_id'];
        }
        else
        {
            $array['success']  = false;
            $array['message'] = "Job id is required.";
            $array['data'] = (object)array();
            return $array;
        }

        $jobDetails = Jobs::where('designation', $designation)->where('id', '!=', $jobId)->where('is_job_open', 'Open')->get();
        $applyJobDetails = array();
        $saveJobDetails = array();
        if(isset($data['seeker_id']) && $data['seeker_id'])
        {
            $seekerId = $data['seeker_id'];
            $jobIds = array();
            foreach ($jobDetails as $key => $value) 
            {
                array_push($jobIds, $value->id);
            }
            $applyJobDetails = ApplyJob::where('seeker_id', $seekerId)->whereIn('job_id', $jobIds)->get()->keyBy('job_id');
            $saveJobDetails = SaveJob::where('seeker_id', $seekerId)->whereIn('job_id', $jobIds)->get()->keyBy('job_id');
        }
        else
        {
            $array['success']  = false;
            $array['message'] = "Seeker id is required.";
            $array['data'] = (object)array();
            return $array;
        }

        foreach ($jobDetails as $key => $value) 
        {
            if(isset($applyJobDetails[$value->id]) && $applyJobDetails[$value->id])
            {
                $value->is_applied = true;
            }
            else
            {
                $value->is_applied = false;
            }

            if(isset($saveJobDetails[$value->id]) && $saveJobDetails[$value->id])
            {
                $value->is_saved = true;
            }
            else
            {
                $value->is_saved = false;
            }
        }

        $array['success']  = true;
        $array['message'] = "Similar job details.";
        $array['data'] = $jobDetails;
        $array['count'] = count($jobDetails);
        return $array;
    }

    public function preferencesJobs(Request $request)
    {
        $data = $request->all();
        $minSalary = '';
        $maxSalary = '';
        $location = '';
        $designation = '';
        $workExperience = '';
        if(isset($data['seeker_id']) && $data['seeker_id'])
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success']  = false;
            $array['message'] = "Seeker id is required.";
            $array['data'] = (object)array();
            return $array;
        }
        
        $query = Jobs::select('*');
        if(isset($data['designation_id']) && $data['designation_id'])
        {
            $designationId = $data['designation_id'];
            $designationData = Designations::find($designationId);
            if($designationData)
            {
                $designation = $designationData->designation;
                $query->where('designation','LIKE',  '%'.$designation.'%');
            }
        }

        if(isset($data['min_salary']) && $data['min_salary'])
        {
            $minSalary = $data['min_salary'];
            $query->where('min_salary','>=', $minSalary);
        }

        if(isset($data['max_salary']) && $data['max_salary'])
        {
            $maxSalary = $data['max_salary'];
            $query->where('max_salary','<=', $maxSalary);
        }
        
        if(isset($data['location']) && $data['location'])
        {
            $location = $data['location'];
            $query->where('job_location',$location);
        }

        if(isset($data['work_experience']) && $data['work_experience'])
        {
            $workExperience = $data['work_experience'];
            $query->where('max_experience','<=', $workExperience);
        }

        $sortBy = '';
        if(isset($data['sort_by']) && $data['sort_by'])
        {
            $sortBy = $data['sort_by'];
        }

        if($sortBy == 'recent')
        {
            $query->orderBy('created_at', 'DESC');
        }

        $preferencesJobsRelevance = array();
        if($sortBy == 'relevance')
        {
            $seekerSkills = Seeker::select('skill_ids')->where('id', $seekerId)->first();
            if($seekerSkills && $seekerSkills->skill_ids)
            {
                $seekerSkillsId = explode(',', $seekerSkills->skill_ids);

                if($seekerSkillsId)
                {
                    $preferencesJobDetails = $query->get();
                    $preferencesSkills = array();
                    $preferencesWithoutSkills = array();
                    foreach ($preferencesJobDetails as $key => $value) 
                    {
                        $jobSkills = array();
                        if($value->skill_ids)
                        {
                            $jobSkills = explode(',', $value->skill_ids);
                        }
                        $result = array_intersect($seekerSkillsId, $jobSkills);
                        if(count($result) > 0)
                        {
                            array_push($preferencesSkills, $value);
                        }
                        else
                        {
                            array_push($preferencesWithoutSkills, $value);
                        }
                    }

                    $preferencesJobsRelevance = array_merge($preferencesSkills, $preferencesWithoutSkills);
                    $preferencesJobs = $preferencesJobsRelevance;
                }
                else
                {
                    $preferencesJobs = $query->get();
                }
            }
            else
            {
                $preferencesJobs = $query->get();
            }
            
        }
        else
        {
            $preferencesJobs = $query->get();
            
        }

        $savedJobs = SaveJob::where('seeker_id', $seekerId)->get()->keyBy('job_id');
        $applyJobs = ApplyJob::where('seeker_id', $seekerId)->get()->keyBy('job_id');

        $jobIdsArray = array();
        foreach ($preferencesJobs as $key => $value) 
        {
            if(isset($applyJobs[$value->id]))
            {
                $value['is_applied']  = true;
            }
            else
            {
                $value['is_applied']  = false;
            }

            if(isset($savedJobs[$value->id]))
            {
                $value['is_saved']  = true;
            }
            else
            {
                $value['is_saved']  = false;
            }
        }

        $array['success']  = true;
        

        if(count($preferencesJobs) > 0)
        {
            $array['message'] = "Preferences job details.";
        }
        else
        {
            $array['message'] = "No Jobs for your skills at the moment. We’re working hard & continuously to ensure we get the perfect organization to place you in.";
        }
        $array['data'] = $preferencesJobs;
        $array['count'] = count($preferencesJobs);
        return $array;
    }

    public function deleteQualification(Request $request)
    {
        $data = $request->all();
        $seekerId = '';
        $qualificationId = '';

        if(isset($data['seeker_id']) && $data['seeker_id'])
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success']  = false;
            $array['message'] = "Seeker id is required.";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['qualification_id']) && $data['qualification_id'])
        {
            $qualificationId = $data['qualification_id'];
        }
        else
        {
            $array['success']  = false;
            $array['message'] = "Qualification id is required.";
            $array['data'] = (object)array();
            return $array;
        }

        $qualificationDetails = SeekerQualification::where('seeker_id', $seekerId)->where('id', $qualificationId)->first();
        if($qualificationDetails)
        {
            $qualificationDetails->delete();

            $array['success']  = true;
            $array['message'] = "Qualification deleted successfully.";
            $array['data'] = (object)array();
            return $array;
        }
        else
        {
            $array['success']  = false;
            $array['message'] = "This qualification already deleted.";
            $array['data'] = (object)array();
            return $array;
        }
    }

    public function deleteWorkExperience(Request $request)
    {
        $data = $request->all();
        $seekerId = '';
        $qualificationId = '';

        if(isset($data['seeker_id']) && $data['seeker_id'])
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success']  = false;
            $array['message'] = "Seeker id is required.";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['experience_id']) && $data['experience_id'])
        {
            $experienceId = $data['experience_id'];
        }
        else
        {
            $array['success']  = false;
            $array['message'] = "Experience id is required.";
            $array['data'] = (object)array();
            return $array;
        }

        $experienceDetails = SeekerWorkExperience::where('seeker_id', $seekerId)->where('id', $experienceId)->first();
        if($experienceDetails)
        {
            $experienceDetails->delete();

            $array['success']  = true;
            $array['message'] = "Work experience deleted successfully.";
            $array['data'] = (object)array();
            return $array;
        }
        else
        {
            $array['success']  = false;
            $array['message'] = "Work experience already deleted.";
            $array['data'] = (object)array();
            return $array;
        }
    }

    public function seekerProfilePercentage($seekerId)
    {
        $data  = Seeker::find($seekerId);
        $percentageCriteria = 0;
        if($data->profile == 'fresher')
        {
            $percentageCriteria = 16.67;
        }
        else
        {
            $percentageCriteria = 14.23;
        }

        $totalPercentage = 0;
        if($data->date_of_birth != '')
        {
            $totalPercentage += $percentageCriteria;
        }
        $qualification = SeekerQualification::where('seeker_id', $seekerId)->count();
        if($qualification > 0)
        {
            $totalPercentage += $percentageCriteria;
        }

        $workExperience = SeekerWorkExperience::where('seeker_id', $seekerId)->count();
        if($workExperience > 0)
        {
            $totalPercentage += $percentageCriteria;
        }

        if($data->resume)
        {
            $totalPercentage += $percentageCriteria;
        }

        if($data->skill_ids)
        {
            $totalPercentage += $percentageCriteria;
        }

        if($data->preferred_industry_id)
        {
            $totalPercentage += $percentageCriteria;
        }

        if($data->about_me)
        {
            $totalPercentage += $percentageCriteria;
        }

        if($totalPercentage > 100)
        {
            $totalPercentage = 100;
        }
        return intval($totalPercentage);
    }

    public function editQualification(Request $request)
    {
        $data = $request->all();
        $experienceId = '';

        if(isset($data['qualification_id']) && $data['qualification_id'])
        {
            $qualificationId = $data['qualification_id'];
        }
        else
        {
            $array['success']  = false;
            $array['message'] = "Qualification id is required.";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['seeker_id']) && $data['seeker_id'])
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success']  = false;
            $array['message'] = "Seeker id is required.";
            $array['data'] = (object)array();
            return $array;
        }

        $seekerQualification = array();

        if(isset($data['highest_education']) && $data['highest_education'])
        {
            $seekerQualification['highest_education'] = $data['highest_education'];
            
            $checkDetails = SeekerQualification::where('id', $qualificationId)->where('seeker_id', $seekerId)->first();
            if ($checkDetails) 
            {
                if ($checkDetails->highest_education != $data['highest_education']) 
                {
                    $checkSeekerQualification = SeekerQualification::where('highest_education', $data['highest_education'])->where('seeker_id', $seekerId)->first();

                    if ($checkSeekerQualification) 
                    {
                        $array['success']  = false;
                        $array['message'] = "This Qualification already added";
                        $array['data'] = (object)array();
                        return $array;
                    }
                }
            }
        }


        if(isset($data['course']) && $data['course'])
        {
            $seekerQualification['course'] = $data['course'];
        }

        if(isset($data['specialization']) && $data['specialization'])
        {
            $seekerQualification['specialization'] = $data['specialization'];
        }

        if(isset($data['university']) && $data['university'])
        {
            $seekerQualification['university'] = $data['university'];
        }

        if(isset($data['passout_year']) && $data['passout_year'])
        {
            $seekerQualification['passout_year'] = $data['passout_year'];
        }

        if(isset($data['education_type']) && $data['education_type'])
        {
            $seekerQualification['education_type'] = $data['education_type'];
        }

        if (isset($data['marks']) && $data['marks'] ) 
        {
            $seekerQualification['marks'] = $data['marks'];
        }

        if (isset($data['medium']) && $data['medium'] ) 
        {
            $seekerQualification['medium'] = $data['medium'];
        }

        $seekerQualification['updated_at'] = date('Y-m-d H:i:s');
        
        SeekerQualification::where('id', $qualificationId)->where('seeker_id', $seekerId)->update($seekerQualification);

        $seekerQualificationDetails = SeekerQualification::where('seeker_id', $seekerId)->get();
        $array['success'] = true;
        $array['message'] = "Qualification updated successfully";
        $array['data']    = $seekerQualificationDetails;
        return $array;
    }

    public function editWorkExperience(Request $request)
    {
        $data = $request->all();
        $experienceId = '';

        if(isset($data['experience_id']) && $data['experience_id'])
        {
            $experienceId = $data['experience_id'];
        }
        else
        {
            $array['success']  = false;
            $array['message'] = "Experience id is required.";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['seeker_id']) && $data['seeker_id'])
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success']  = false;
            $array['message'] = "Seeker id is required.";
            $array['data'] = (object)array();
            return $array;
        }

        $seekerworkExp = array();

        if(isset($data['designation']) && $data['designation'])
        {
            $seekerworkExp['designation'] = $data['designation'];
        }

        if(isset($data['company']) && $data['company'])
        {
            $seekerworkExp['company'] = $data['company'];
        }

        if(isset($data['salary']) && $data['salary'])
        {
            $seekerworkExp['salary'] = $data['salary'];
        }

        if(isset($data['salary_in']) && $data['salary_in'])
        {
            $seekerworkExp['salary_in'] = $data['salary_in'];
        }

        if(isset($data['join_date']) && $data['join_date'])
        {
            $seekerworkExp['join_date'] = $data['join_date'];
        }

        if(isset($data['leave_date']) && $data['leave_date'])
        {
            $seekerworkExp['leave_date'] = $data['leave_date'];
        }

        if(isset($data['notice_period']) && $data['notice_period'])
        {
            $seekerworkExp['notice_period'] = $data['notice_period'];
        }

        $seekerworkExp['experience'] = 0;
        if(isset($data['experience']) && $data['experience'])
        {
            $seekerworkExp['experience'] = $data['experience'];
        }

        $seekerworkExp['updated_at'] = date('Y-m-d H:i:s');
        
        SeekerWorkExperience::where('id', $experienceId)->where('seeker_id', $seekerId)->update($seekerworkExp);

        $seekerWorkExperienceDetails = SeekerWorkExperience::where('id', $experienceId)->where('seeker_id', $seekerId)->get();
        foreach ($seekerWorkExperienceDetails as $key => $value) 
        {
            $value->experience = $value->experience." Year";
        }
        $array['success'] = true;
        $array['message'] = "Work experience updated successfully";
        $array['data']    = $seekerWorkExperienceDetails;
        return $array;
    }

    public function updateSeekerProfile(Request $request)
    {
        $data = $request->all();
        if(isset($data['seeker_id']) && $data['seeker_id'])
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success']  = false;
            $array['message'] = "Seeker id is required.";
            $array['data'] = (object)array();
            return $array;
        }

        $seekerDetails = array();
        if(isset($data['status']) && $data['status'])
        {
            $seekerDetails['status'] = $data['status'];
        }

        $seekerDetails['updated_at'] = date('Y-m-d H:i:s');

        Seeker::where('id', $seekerId)->update($seekerDetails);

        $array['success'] = true;
        $array['message'] = "Seeker details updated successfully";
        $array['data']    = (object)array();
        return $array;
    }

    # Function : This function used to fetch Notification setting data
    # Request  : seeker id
    # Response : success true/false json response
    # Author   : Brijendra
    public function getNotificationSetting(Request $request)
    {

        $data = $request->all();

        if (isset($data['seeker_id']) && $data['seeker_id'] ) 
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Seeker id is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        $notificationSetting = NotificationSetting::where('seeker_id', $seekerId)->first();
        if ($notificationSetting) 
        {
            $array['success'] = true;
            $array['message'] = "Seeker details updated successfully";
            $array['data']    = $notificationSetting;
            return $array;
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "No record found";
            $array['data'] = (object)array();
            return $array;
        }

        
    }

    # Function : This function used to prifile visibility setting data
    # Request  : seeker id
    # Response : success true/false json response
    # Author   : Brijendra
    public function getProfileVisibilitySetting(Request $request)
    {

        $data = $request->all();

        if (isset($data['seeker_id']) && $data['seeker_id'] ) 
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Seeker id is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        $seeker = Seeker::select('profile_visibility')->where('id', $seekerId)->first();
        if ($seeker) 
        {
            $array['success'] = true;
            $array['message'] = "Seeker details updated successfully";
            $array['data']    = $seeker;
            return $array;
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "No record found";
            $array['data'] = (object)array();
            return $array;
        }

        
    }


    public function generateResume(Request $request)
    {
       
        $data = $request->all();
        if (isset($data['seeker_id']) && $data['seeker_id'] ) 
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Seeker id is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        $user = Seeker::where('id', $seekerId)->first();
        if ($user != '') 
        {
            $profileCompletePercentage = $this->seekerProfilePercentage($seekerId);
            if($profileCompletePercentage <= 80)
            {
                $array['success'] = false;
                $array['message'] = "Please complete your profile before creating your resume.";
                $array['data'] = (object)array();
                return $array;
            }
            if($user->date_of_birth)
            {
                $user->date_of_birth = date('d-m-Y', strtotime($user->date_of_birth)); 
            }
            else
            {
                $user->date_of_birth = '';
            }
            $baseUrl = url('/');

            $industry = '';
            if($user['preferred_industry_id'])
            {
                $industryIds = explode(',', $user['preferred_industry_id']); 
                $industryDetails = Industry::whereIn('id',$industryIds)->get()->toArray();   
                if(count($industryDetails) > 0)
                {
                    $industryArr = array();
                    foreach ($industryDetails as $key => $value) 
                    {
                        $industryArr[] = $value['industry_name']; 
                    }
                    $industry = implode(',', $industryArr);
                }
            }
            $seekerWorkExperienceDetails = SeekerWorkExperience::where('seeker_id', $seekerId)->get();
            foreach ($seekerWorkExperienceDetails as $key => $value) 
            {
                $value->experience = $value->experience." Year";
            }
            
            $seekerQualificationDetails = SeekerQualification::where('seeker_id', $seekerId)->get();
            $result = array();
            foreach ($seekerQualificationDetails as $key => $value) 
            {
               if ($value->highest_education == 'Class 10th') 
               {
                   $value->qualification_text = 'Passed 10th from '.$value->course.' board with '.$value->marks.' in year '.$value->passout_year;
               }
               else if ($value->highest_education == 'Class 12th') 
               {
                   $value->qualification_text = 'Passed 12th from '.$value->course.' board with '.$value->marks.' in year '.$value->passout_year;
               }
               else if ($value->highest_education == 'Graduate') 
               {
                   $value->qualification_text = 'Graduation done from '.$value->university.' university in year '.$value->passout_year;
               }
               else if ($value->highest_education == 'Post Graduate') 
               {
                   $value->qualification_text = 'Post Graduation done from '.$value->university.' university in year '.$value->passout_year;
               }
               else if ($value->highest_education == 'Doctorate or PHD') 
               {
                   $value->qualification_text = 'Doctorate or PHD done from '.$value->university.' university in year '.$value->passout_year;
               }
               else
               {
                   $value->qualification_text = "NA";
               }
            }

            $skillsDetails = Skill::get()->keyBy('id')->toArray();

            $skillIds = $user['skill_ids'];
            $user['all_skills'] = '';
            if($skillIds)
            {
                $skills = '';
                $skillIds = explode(',', $skillIds);
                foreach ($skillIds as $key => $skillval) 
                {
                    if(isset($skillsDetails[$skillval]))
                    {
                        if($skills)
                        {
                            $skills .= ', '. $skillsDetails[$skillval]['skill'];
                        }
                        else
                        {
                            $skills = $skillsDetails[$skillval]['skill'];
                        }
                    }
                }
                
                $user['all_skills'] = $skills;
            }
            

            $user['profile_image'] = $baseUrl.$user['profile_image'];
            $user['summary']       = $user['about_me'];
            $user['today_date']    = date('d-m-Y');
            $user['industry']      = $industry;
            $user['experience']    = $seekerWorkExperienceDetails;
            $user['qualification'] = $seekerQualificationDetails;

            $path = 'resumes/'.$user->mobile.'_resume_'.time().'.pdf';
            $pdf = PDF::loadView('generateResume', compact('user'));
            $pdf->save(public_path($path));

            $seeker = Seeker::where('id', $seekerId)->first();
            $seeker->resume = '/'.$path;
            $seeker->updated_at = date('Y-m-d H:i:s');
            $seeker->save();

            $array['success'] = true;
            $array['message'] = "Seeker details updated successfully";
            $array['data']    = $seeker;
            return $array;
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Seeker not Exist";
            $array['data'] = (object)array();
            return $array;
        }

    }


    public function registerDeviceInDownload(Request $request)
    {
        $data = $request->all();

        if (isset($data['device_id']) && $data['device_id']) 
        {
            $deviceId = $data['device_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Device id Required";
            $array['data']    = (object)array();
            return $array;
        }

        $checkDevice = AppDownload::where('device_id', $deviceId)->first();
        if ($checkDevice == '') 
        {
            $registerDevice = new AppDownload;
            $registerDevice->device_id = $deviceId;
            $registerDevice->save();
        }

        $array['success'] = true;
        $array['message'] = "App Register";
        $array['data'] = (object)array();
        return $array;

    }

    public function loginTrack($seekerId)
    {
        $seekerLoginDetails = SeekerLoginTrack::where('seeker_id', $seekerId)->orderBy('id','desc')->first();
        if ($seekerLoginDetails == '') 
        {
            $newLogin = new SeekerLoginTrack;
            $newLogin->seeker_id = $seekerId;
            $newLogin->login_time = date('Y-m-d H:i:s');
            $newLogin->save();

        }
        else
        {
            if ($seekerLoginDetails->logout_time) 
            {
                $newLogin = new SeekerLoginTrack;
                $newLogin->seeker_id = $seekerId;
                $newLogin->login_time = date('Y-m-d H:i:s');
                $newLogin->save();
            }
            else
            {
                $seekerLoginDetails->login_time = date('Y-m-d H:i:s');
                $seekerLoginDetails->save();
            }
        }

        return 'Ok';
    }

    public function logoutSeeker(Request $request)
    {
        $data = $request->all();
        if (isset($data['seeker_id']) && $data['seeker_id'] ) 
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Seeker id is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        $seekerLoginDetails = SeekerLoginTrack::where('seeker_id', $seekerId)->orderBy('id','desc')->first();
        if ($seekerLoginDetails) 
        {
            $seekerLoginDetails->logout_time = date('Y-m-d H:i:s');
            $seekerLoginDetails->save();

        }

        $array['success'] = true;
        $array['message'] = "Succesfully LogOut";
        $array['data'] = (object)array();
        return $array;
    }

    public function removeProfileImage(Request $request)
    {
        $data = $request->all();

        if (isset($data['seeker_id']) && $data['seeker_id'] ) 
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Seeker id is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        $seeker = Seeker::where('id', $seekerId)->first();
        if ($seeker) 
        {
            $defaultImageUrl = '/seekerProfileImages/defaultUser.png';
            if ($seeker->profile_image) 
            {
                if ($seeker->profile_image != $defaultImageUrl) 
                {
                    if(file_exists(public_path($seeker->profile_image)))
                    {                  
                        unlink(public_path($seeker->profile_image));
                    }
                }

                $seeker->profile_image = $defaultImageUrl;
                $seeker->save();


                $array['success'] = true;
                $array['message'] = "Image Succesfully Removed";
                $array['data'] = (object)array();
                return $array;
            }
            else
            {
                $seeker->profile_image = $defaultImageUrl;
                $seeker->save();

                $array['success'] = true;
                $array['message'] = "Image Succesfully Removed";
                $array['data'] = (object)array();
                return $array;
            }
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Seeker Not Exist.";
            $array['data'] = (object)array();
            return $array;
        }
    }


}
