<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\AppDownload;
use App\Seeker;
use App\SeekerLoginTrack;
use App\ApplyJob;
use App\Jobs;
use App\ClientTracker;

class ReportController extends Controller
{
    public function appDownloadReport(Request $request)
    {
    	$data = $request->all();

    	if (isset($data['start_date']) && $data['start_date']) 
    	{
    		$startDate = $data['start_date'];
    	}
    	else
    	{
    		$array['success'] = false;
            $array['message'] = "Start date is required";
            $array['data'] = (object)array();
            return $array;
    	}

    	if (isset($data['end_date']) && $data['end_date']) 
    	{
    		$endDate = $data['end_date'];
    	}
    	else
    	{
    		$array['success'] = false;
            $array['message'] = "End date is required";
            $array['data'] = (object)array();
            return $array;
    	}

    	$appDownloads = AppDownload::where('created_at', '>=', $startDate)->where('created_at', '<=', $endDate)->get();

    	$array['success'] = true;
        $array['message'] = "Record Found";
        $array['data'] = count($appDownloads);
        return $array;
    }

    public function seekerRegistrationReport(Request $request)
    {
    	$data = $request->all();

    	if (isset($data['start_date']) && $data['start_date']) 
    	{
    		$startDate = $data['start_date'];
    	}
    	else
    	{
    		$array['success'] = false;
            $array['message'] = "Start date is required";
            $array['data'] = (object)array();
            return $array;
    	}

    	if (isset($data['end_date']) && $data['end_date']) 
    	{
    		$endDate = $data['end_date'];
    	}
    	else
    	{
    		$array['success'] = false;
            $array['message'] = "End date is required";
            $array['data'] = (object)array();
            return $array;
    	}

    	$seeker = Seeker::where('created_at', '>=', $startDate)->where('created_at', '<=', $endDate)->get();
    	foreach ($seeker as $key => $value) 
    	{
    		$value->registration_date = date('d-m-Y', strtotime($value->created_at));
    	}
    	if ($seeker) 
    	{
    		$array['success'] = true;
	        $array['message'] = "Record Found";
	        $array['data'] = $seeker;
	        return $array;
    	}
    	else
    	{
    		$array['success'] = false;
            $array['message'] = "No record found";
            $array['data'] = (object)array();
            return $array;	
    	}

    }

    public function candidateLoginReport(Request $request)
    {
    	$data = $request->all();

    	if (isset($data['start_date']) && $data['start_date']) 
    	{
    		$startDate = $data['start_date'];
    	}
    	else
    	{
    		$array['success'] = false;
            $array['message'] = "Start date is required";
            $array['data'] = (object)array();
            return $array;
    	}

    	if (isset($data['end_date']) && $data['end_date']) 
    	{
    		$endDate = $data['end_date'];
    	}
    	else
    	{
    		$array['success'] = false;
            $array['message'] = "End date is required";
            $array['data'] = (object)array();
            return $array;
    	}

    	$seekerLogin = SeekerLoginTrack::where('login_time', '>=', $startDate)
								    	->where('login_time', '<=', $endDate)
								    	->where('logout_time', '')
								    	->get();
		$array['success'] = true;
        $array['message'] = "Record Found";
        $array['data'] = count($seekerLogin);
        return $array;

    }

    public function jobAppliedCandidateCountReport(Request $request)
    {
    	$data = $request->all();

    	if (isset($data['start_date']) && $data['start_date']) 
    	{
    		$startDate = $data['start_date'];
    	}
    	else
    	{
    		$array['success'] = false;
            $array['message'] = "Start date is required";
            $array['data'] = (object)array();
            return $array;
    	}

    	if (isset($data['end_date']) && $data['end_date']) 
    	{
    		$endDate = $data['end_date'];
    	}
    	else
    	{
    		$array['success'] = false;
            $array['message'] = "End date is required";
            $array['data'] = (object)array();
            return $array;
    	}

    	$appliedCandidates = ApplyJob::where('created_at', '>=', $startDate)->where('created_at', '<=', $endDate)->get();
    	$appliedCandidatesIdsArray = array();
    	$appliedJobIdsArray = array();
    	foreach ($appliedCandidates as $key => $value) 
    	{
    		array_push($appliedJobIdsArray, $value->job_id);
    	}

    	$jobDetails = Jobs::whereIn('id', $appliedJobIdsArray)->get()->keyBy('id');

    	foreach ($appliedCandidates as $key => $value) 
    	{
    		$jobCount = ApplyJob::where('created_at', '>=', $value->created_at)->where('created_at', '<=', $value->created_at)->where('job_id', $value->job_id)->get();

    		$value->candidate_count = count($jobCount);

    		if (isset($jobDetails[$value->job_id])) 
    		{
    			$value->job_designation = $jobDetails[$value->job_id]['designation'];
    			$clientDetails = ClientTracker::where('id', $jobDetails[$value->job_id]['client_id'])->first();
    			if ($clientDetails) 
    			{
    				$value->client_name = $clientDetails->client_name;
    				$value->client_id = $clientDetails->id;
    			}
    			else
    			{
    				$value->client_id = 0;
    			}
    		}
    		else
    		{
    			$value->job_designation = 'NA';
    			$value->client_id = 0;
    		}

    	}


		$array['success'] = true;
        $array['message'] = "Record Found";
        $array['data'] = $appliedCandidates;
        return $array;

    }
}
