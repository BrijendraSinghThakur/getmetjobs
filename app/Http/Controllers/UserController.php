<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Jobs;
use App\ApplyJob;
use App\Seeker;
use App\SeekerWorkExperience;
use Auth;
use App\JobInterview;
use App\OtpVerification;
use Mail;

class UserController extends Controller
{

    public function user(Request $request)
    {
        $data = $request->all();
        if(isset($data['id']) && $data['id'])
        {
            $data = User::find($data['id']);
        }
        else
        {
            $data = User::all();
        }

        $array['success'] = true;
        $array['message'] = "User details.";
        $array['data'] = $data;
        return $array;
    }

	public function userRegistration(Request $request)
    {
		$data = $request->all();

		$name = NULL;
    	$email = NULL;
    	$password = NULL;

    	if (isset($data['name']) && $data['name'] ) 
        {
    	   $name = $data['name'];
	    }
	    else
        {
            $array['success'] = false;
            $array['message'] = "Name is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        if (isset($data['email']) && $data['email'] ) 
        {
    	   $email = $data['email'];
	    }
	    else
        {
            $array['success'] = false;
            $array['message'] = "Email is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        if (isset($data['password']) && $data['password'] ) 
        {
    	    $password = bcrypt($data['password']);
	    }
	    else
        {
            $array['success'] = false;
            $array['message'] = "Password is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        if (isset($data['mobile']) && $data['mobile']) 
        {
           $mobile = $data['mobile'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Mobile is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        if (isset($data['role']) && $data['role']) 
        {
           $userRole = $data['role'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "User role is Required.";
            $array['data'] = (object)array();
            return $array;
        }
        $userDetails = User::where('email', $email)->first();
        if($userDetails)
        {
            $array['success'] = false;
            $array['message'] = "This email already used.";
            $array['data'] = (object)array();
            return $array;
        }

        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->password = $password;
        $user->mobile = $mobile;
        $user->role = $userRole;
        $user->login_token = $this->generateRandomString();
        $user->save();

        $array['success'] = true;
        $array['message'] = "Register successfully !!!.";
        $array['data'] = $user;
        return $array;
	}

	public function userLogin(Request $request)
    {
    	$data = $request->all();
        
        if(isset($data['email']) && $data['email'])
        {
            $email = $data['email'];
        }
        else
        {
	            $array['success'] = false;
	            $array['message'] = "Email Required";
	            $array['data'] = (object)array();
	            return $array;
        }

        if(isset($data['password']) && $data['password'])
        {
            $password = $data['password'];
	    }
        else
        {
	            $array['success'] = false;
	            $array['message'] = "Password Required";
	            $array['data'] = (object)array();
	            return $array;
        }
    	
        // $login = User::select('*')->where('email', $email)->where('password', $password)->first();
		$login = Auth::attempt(['email' => $email, 'password' => $password]);

        if($login)
        {
            $characters = '0123456789';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < 6; $i++) 
            {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            $userDetails = User::select('*')->where('email', $email)->first();
            $otpVerification = new OtpVerification;
            $otpVerification->user_id = $userDetails->id;
            $otpVerification->user_type = $userDetails->role;
            $otpVerification->otp = $randomString;
            $otpVerification->otp_screen = 'login';
            $otpVerification->created_at = date('Y-m-d H:i:s');
            $otpVerification->updated_at = date('Y-m-d H:i:s');
            $otpVerification->save();

            $message = '';
            $email = 'anmol@tirumalajobs.com';
            $emailContent = array();
            $emailContent['otp'] = $randomString;
            $emailContent['user'] = $userDetails->name;
            $subject = "Get Me Jobs Login OTP";
            Mail::send('emails.login', $emailContent, function($message) use ($email, $subject){
                $message->to($email, 'Get Me Jobs Login OTP')->subject
                    ($subject);
                $message->from('info.tirumalajobs@gmail.com');
            });

            $array['success'] = true;
            $array['message'] = "Logged in successfully!!";
            $array['data'] = $userDetails;
            return $array;
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Email or Password is Incorrect!!";
            $array['data'] = (object)array();
            return $array;
            
        }
    }
    
    public function generateRandomString($length = 16) 
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) 
        {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getActiveUser(Request $request)
    {
        $data = User::where('status','Active')->get();
        $array['success'] = true;
        $array['message'] = "User details.";
        $array['data'] = $data;
        return $array;
    }

    public function getBlockedUser(Request $request)
    {
        $data = User::where('status','Blocked')->get();
        $array['success'] = true;
        $array['message'] = "User details.";
        $array['data'] = $data;
        return $array;
    }

    public function updateUser(Request $request)
    {
        $data = $request->all();
        $userId = '';
        $email = '';
        $mobile = '';
        $userDetails = array();
        if(isset($data['id']) && $data['id'])
        {
            $userId = $data['id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "User id is Required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['name']) && $data['name'])
        {
            $userDetails['name'] = $data['name'];
        }

        if(isset($data['email']) && $data['email'])
        {
            $userDetails['email'] = $data['email'];
            $email = $data['name'];
        }

        if(isset($data['mobile']) && $data['mobile'])
        {
            $userDetails['mobile'] = $data['mobile'];
            $mobile = $data['mobile'];
        }

        if(isset($data['role']) && $data['role'])
        {
            $userDetails['role'] = $data['role'];
        }

        if($email && $mobile)
        {
            $userInfo = User::where(function($query) use ($email, $mobile) {
                                $query->where('email', $email)->orwhere('mobile', $mobile);
                            })->where('id','!=', $userId)->first();

            if($userInfo && $userInfo->email == $data['email'])
            {
                $array['success'] = false;
                $array['message'] = "This Email is already used";
                $array['data'] = (object)array();
                return $array;
            }
            elseif ($userInfo && $userInfo->mobile == $data['mobile'])
            {
                $array['success'] = false;
                $array['message'] = "Mobile number already used.";
                $array['data'] = (object)array();
                return $array;
            }
        }
        elseif($email)
        {
            $userInfo = User::where('email', $email)->where('id','!=', $userId)->first();

            if($userInfo && $userInfo->email == $data['email'])
            {
                $array['success'] = false;
                $array['message'] = "This Email is already used";
                $array['data'] = (object)array();
                return $array;
            }
        }
        elseif($mobile)
        {
            $userInfo = User::where('mobile', $mobile)->where('id','!=', $userId)->first();

            if($userInfo && $userInfo->mobile == $data['mobile'])
            {
                $array['success'] = false;
                $array['message'] = "Mobile number already used.";
                $array['data'] = (object)array();
                return $array;
            }
        }
        if(isset($data['gender']) && $data['gender'])
        {
            $userDetails['gender'] = $data['gender'];
        }

        if(isset($data['dob']) && $data['dob'])
        {
            $userDetails['date_of_birth'] = $data['dob'];
        }

        if(isset($data['status']) && $data['status'])
        {
            $userDetails['status'] = $data['status'];
        }

        if (isset($data['image']) && $data['image']) 
        {
            $file       = $request->file('image');
            $extension  = $file->getClientOriginalExtension();
            $imageName  = strtotime(date('Y-m-d H:i:s')).'_user_'.time().'.'.$extension;
            $file->move(public_path('/userProfileImages'), $imageName);
            $imageUrl  = '/userProfileImages/'.$imageName;
            $userDetails['image'] = $imageUrl;
        }



        $userDetails['updated_at'] = date('Y-m-d H:i:s');
        if(count($userDetails) > 0)
        {
            User::where('id', $userId)->update($userDetails);
        }
        $userDetails = User::select('id','name','email','mobile','image')->where('id', $userId)->first();
        $array['success'] = true;
        $array['message'] = "Data successfully updated";
        $array['data'] = $userDetails;
        return $array;
    }

    



}
