<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\EducationCategory;
use App\Jobs;
use App\Seeker;
use App\ClientTracker;
use App\SeekerWorkExperience;
use App\ApplyJob;
use App\JobInterview;
use App\Industry;
use App\EducationSubCategory;
use App\Specialization;
use App\Cities;
use App\Skill;
use App\SeekerQualification;
use App\UserAllotment;
use App\Designations;
use App\University;
use App\Department;
use App\SaveJob;
use DB;
use Mail;
use App\AppDownload;
use App\Role;
use App\SeekerLoginTrack;
use App\OtpVerification;

class JobsController extends Controller
{

    public function addCategory(Request $request)
    {
        $data = $request->all();
        if(isset($data['category']) && $data['category'])
        {
            $categoryName = trim($data['category']);
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Category name is required";
            $array['data'] = (object)array();
            return $array;
        }
        
        $educationCategory = EducationCategory::where('education', $categoryName)->first();

        if($educationCategory)
        {
            $array = array();
            $array['success'] = false;
            $array['message'] = "Category name already exist.";
            $array['data'] = (object)array();
            return $array;
        }
        else
        {
            $category = new EducationCategory;
            $category->education = $categoryName;
            $category->save();
            
            $array = array();
            $array['success'] = true;
            $array['message'] = "Category Successfully Added.";
            $array['data'] = (object)array();
            return $array;
        }
    }

    public function updateCategory(Request $request)
    {
        $data = $request->all();
        if(isset($data['category_id']) && $data['category_id'])
        {
            $categoryId = $data['category_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Category ID is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['category_name']) && $data['category_name'])
        {
            $categoryName = trim($data['category_name']);
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Category name is required";
            $array['data'] = (object)array();
            return $array;
        }
        $educationDetails = EducationCategory::where('education', $categoryName)->where('id','!=', $categoryId)->first();
        if($educationDetails)
        {
            $array = array();
            $array['success'] = false;
            $array['message'] = "Category name already exist.";
            $array['data'] = (object)array();
            return $array;
        }
        else
        {
            $category = EducationCategory::find($categoryId);
            $category->education = $categoryName;
            $category->save();
            
            $array = array();
            $array['success'] = true;
            $array['message'] = "Category Successfully Update.";
            $array['data'] = (object)array();
            return $array;
        }

    }

    public function addSubCategory(Request $request)
    {
        $data = $request->all();

        if(isset($data['edu_category_id']) && $data['edu_category_id'] != '')
        {
            $eduCatID = $data['edu_category_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Category id is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['sub_category']) && $data['sub_category'])
        {
            $subEducation = $data['sub_category'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Sub Category is required";
            $array['data'] = (object)array();
            return $array;
        }

        $category = new EducationSubCategory;
        $category->education_category = $eduCatID;
        $category->sub_education      = $subEducation;
        $category->save();
        
        $array = array();
        $array['success'] = true;
        $array['message'] = "Sub Category Successfully Added.";
        $array['data'] = (object)array();
        return $array;
    }

    public function updateSubCategory(Request $request)
    {
        $data = $request->all();

        if(isset($data['subCategory_id']) && $data['subCategory_id'] != '')
        {
            $subCatId = $data['subCategory_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Category id is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['subCategory_name']) && $data['subCategory_name'])
        {
            $subEducation = $data['subCategory_name'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Sub Category is required";
            $array['data'] = (object)array();
            return $array;
        }

        $category = EducationSubCategory::find($subCatId);
        $category->sub_education  = $subEducation;
        $category->save();
        
        $array = array();
        $array['success'] = true;
        $array['message'] = "Sub Category Successfully Update.";
        $array['data'] = (object)array();
        return $array;
    }

    public function addIndustry(Request $request)
    {
        $data = $request->all();
        if(isset($data['industry_name']) && $data['industry_name'])
        {
            $industryName = $data['industry_name'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Industry name is required";
            $array['data'] = (object)array();
            return $array;
        }
        $industrayExist = Industry::where('industry_name', $industryName)->first();
        if($industrayExist)
        {
            $array = array();
            $array['success'] = false;
            $array['message'] = "Industry name already exist.";
            $array['data'] = (object)array();
            return $array;
        }
        else
        {
            $indus = new Industry;
            $indus->industry_name = $industryName;
            $indus->created_at = date('Y-m-d H:i:s');
            $indus->updated_at = date('Y-m-d H:i:s');
            $indus->save();

            $array = array();
            $array['success'] = true;
            $array['message'] = "Industry Successfully Added.";
            $array['data'] = (object)array();
            return $array;
        }
    }

    public function updateIndustry(Request $request)
    {
        $data = $request->all();
        if(isset($data['industry_id']) && $data['industry_id'])
        {
            $industryId = $data['industry_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Industry id is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['industry_name']) && $data['industry_name'])
        {
            $industryName = $data['industry_name'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Industry name is required";
            $array['data'] = (object)array();
            return $array;
        }
        
        $indus = Industry::find($industryId);
        $indus->industry_name = $industryName;
        $indus->save();

        $array = array();
        $array['success'] = true;
        $array['message'] = "Industry Successfully Update.";
        $array['data'] = (object)array();
        return $array;
    }

    public function addDesignation(Request $request)
    {
        $data = $request->all();
        if(isset($data['designation_name']) && $data['designation_name'])
        {
            $designationName = trim($data['designation_name']);
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Designation name is required";
            $array['data'] = (object)array();
            return $array;
        }
        $designationDetails = Designations::where('designation', $designationName)->first();
        if($designationDetails)
        {
            $array = array();
            $array['success'] = false;
            $array['message'] = "Designation name already exist.";
            $array['data'] = (object)array();
            return $array;
        }
        else
        {
            $designation = new Designations;
            $designation->designation = $designationName;
            $designation->created_at = date('Y-m-d H:i:s');
            $designation->updated_at = date('Y-m-d H:i:s');
            $designation->save();

            $array = array();
            $array['success'] = true;
            $array['message'] = "Designation Successfully Added.";
            $array['data'] = (object)array();
            return $array;
        }
    }

    public function updateDesignation(Request $request)
    {
        $data = $request->all();
        if(isset($data['designation_id']) && $data['designation_id'])
        {
            $designationId = $data['designation_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Designation id is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['designation_name']) && $data['designation_name'])
        {
            $designationName = $data['designation_name'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Designation name is required";
            $array['data'] = (object)array();
            return $array;
        }
        
        $designation = Designations::find($designationId);
        $designation->designation = $designationName;
        $designation->save();

        $array = array();
        $array['success'] = true;
        $array['message'] = "Designation Successfully Update.";
        $array['data'] = (object)array();
        return $array;
    }

    public function createJob(Request $request)
    {
        $data = $request->all();
        if(isset($data['client_id']) && $data['client_id'])
        {
            $client = $data['client_id'];
            $clintDetails = ClientTracker::where('id', $client)->first();
        }
        else
        {
             // $array['success'] = false;
             //    $array['message'] = "Client name is required";
             //    $array['data'] = (object)array();
             //    return $array;
            if(isset($data['client']) && $data['client'])
            {
                $clintDetails = ClientTracker::where('client_name', $data['client'])->first();
            }
            else
            {
                $array['success'] = false;
                $array['message'] = "Client name is required";
                $array['data'] = (object)array();
                return $array;
            }
        }

        if ($clintDetails == '') 
        {
            $array['success'] = false;
            $array['message'] = "Invalid Client name";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['designation']) && $data['designation'])
        {
            $designation = $data['designation'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Designation is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['job_type']) && $data['job_type'])
        {
            $jobType = $data['job_type'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Job type is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['job_description']) && $data['job_description'])
        {
            $jobDescription = $data['job_description'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Job description is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['job_location']) && $data['job_location'])
        {
            $jobLocation = $data['job_location'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Job location is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['min_experience']))
        {
            $minExperienceYears = (string)$data['min_experience'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Min experience year is required";
            $array['data'] = (object)array();
            return $array;
        }

        $minExperienceMonth = "0";
        if(isset($data['min_experience_month']))
        {
            $minExperienceMonth = (string)$data['min_experience_month'];
        }

        $minExperience = $minExperienceYears.".".$minExperienceMonth;


        if(isset($data['max_experience']) && $data['max_experience'])
        {
            $maxExperienceYears = (string)$data['max_experience'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Max experience year is required";
            $array['data'] = (object)array();
            return $array;
        }

        $maxExperienceMonth = "0";
        if(isset($data['max_experience_month']))
        {
            $maxExperienceMonth = (string)$data['max_experience_month'];
        }

        $maxExperience = $maxExperienceYears.".".$maxExperienceMonth;

        if(isset($data['min_salary']) && $data['min_salary'])
        {
            $minSalary = $data['min_salary'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "min salary is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['max_salary']) && $data['max_salary'])
        {
            $maxSalary = $data['max_salary'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Max salary is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['vacancy']) && $data['vacancy'])
        {
            $vacancy = $data['vacancy'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Vacancy is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['skill_ids']) && $data['skill_ids'])
        {
            $skillIds = implode(',', $data['skill_ids']);
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Skill is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['industry_type']) && $data['industry_type'])
        {  
            $industryType  = $data['industry_type'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "industry type is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['education_category_id']) && $data['education_category_id'])
        {  
            $educationCategoryId  = $data['education_category_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Category is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['sub_education_category_id']) && $data['sub_education_category_id'])
        {  
            $educationSubCategoryId  = $data['sub_education_category_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Sub Category is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['desire_candidate']) && $data['desire_candidate'])
        {
            $desireCandidate = $data['desire_candidate'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Job Responsibilities and Requirements is required";
            $array['data'] = (object)array();
            return $array;
        }

        $salaryType = 'LPA';
        if(isset($data['salary_type']) && $data['salary_type'])
        {
            $salaryType = $data['salary_type'];
        }

        $job = new Jobs;
        $job->client_id         = $clintDetails->id;
        $job->designation       = $designation;
        $job->job_type          = $jobType;
        $job->job_description   = $jobDescription;
        $job->job_location      = $jobLocation;
        $job->min_experience    = (float)$minExperience;
        $job->max_experience    = (float)$maxExperience;
        $job->min_salary        = $minSalary;
        $job->max_salary        = $maxSalary;
        $job->salary_type       = $salaryType;
        $job->vacancy           = $vacancy;
        $job->skill_ids         = $skillIds;
        $job->industry_type     = $industryType;
        $job->education_category_id   = $educationCategoryId;
        $job->sub_education_category_id  = $educationSubCategoryId;
        $job->desire_candidate  = $desireCandidate;
        $job->is_job_open       = 'Open';
        $job->created_at        = date('Y-m-d H:i:s');
        $job->updated_at        = date('Y-m-d H:i:s');
        $job->save();

        $array = array();
        $array['success'] = true;
        $array['message'] = "Job Created Successfully";
        $array['data'] = (object)array();
        return $array;
    }


    public function getBlockedCandidates(Request $request)
    {
        $data = $request->all();
        $blockedSeekerDetails = Seeker::where('status', 'Blocked')->get();

        $array['success'] = true;
        $array['message'] = "Blocked Candidates Details";
        $array['data'] = $blockedSeekerDetails;
        return $array;
    }


    # Function : This function is used to fetch client list
    # Request  : None
    # Response : success true/false json response
    # Author   : Brijendra
    public function getClientList(Request $request)
    {
        $data = $request->all();
        $query   = ClientTracker::where('is_deleted', 0);
        
        if(isset($data['client_name']) && $data['client_name'])
        {
            $query = $query->where('client_name', 'LIKE', '%'.$data['client_name'].'%');
        }

        if(isset($data['domain_name']) && $data['domain_name'])
        {
            $query = $query->where('domain_name', 'LIKE', '%'.$data['domain_name'].'%');
        }
        
        $clientList = $query->get();

        $clientIdsArray = array();
        foreach ($clientList as $key => $value) 
        {
            array_push($clientIdsArray, $value->id);
        }

        $vacancyCount = 0;
        foreach ($clientList as $key => $value) 
        {
            $clientJobs = Jobs::where('client_id', $value->id)->where('is_job_open', 'Open')->get();
            if ($clientJobs) 
            {
                foreach ($clientJobs as $key1 => $value1) 
                {
                    $vacancyCount += $value1->vacancy;
                    $value->total_vacancy = $vacancyCount;
                }

                $vacancyCount = 0;
            }
            else
            {
                $value->total_vacancy = 0;
            }
        }

        $array['success'] = true;
        $array['message'] = "Record found";
        $array['data']    = $clientList;
        return $array;   
  
    }

    # Function : This function is used to soft delete client
    # Request  : None
    # Response : success true/false json response
    # Author   : Brijendra
    public function deleteClient(Request $request)
    {
        $data = $request->all();
        if(isset($data['client_id']) && $data['client_id'])
        {  
            $clientId  = $data['client_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Client is required";
            $array['data'] = (object)array();
            return $array;
        }

        $clientDetails   = ClientTracker::where('id', $clientId)->first();

        if ($clientDetails) 
        {
            $clientDetails->is_deleted = 1;
            $clientDetails->save();

            $array['success'] = true;
            $array['message'] = "Record deleted";
            $array['data']    = (object)array();
            return $array;   
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Client not found";
            $array['data'] = (object)array();
            return $array;
        }

  
    }

    public function getClientJobDetails(Request $request)
    {
        $data = $request->all();

        if(isset($data['client_id']) && $data['client_id'])
        {
            $clientJobs = Jobs::where('client_id', $data['client_id'])->where('is_job_open', 'Open')->get();
            if ($clientJobs) 
            {
                $clientDetails = ClientTracker::where('id', $data['client_id'])->first();
                $vacancyCount  = 0;
                foreach ($clientJobs as $key => $value) 
                {
                    $vacancyCount += $value->vacancy;
                }

                $clientDetails->total_vacancy = $vacancyCount;
                $result = array('client_details' => $clientDetails, 'client_jobs' => $clientJobs);
                $array['success'] = true;
                $array['message'] = "Client tacker details";
                $array['data'] = $result;
                return $array;
            }
            else
            {
                $array['success'] = false;
                $array['message'] = "Client id required";
                $array['data'] = (object)array();
                return $array;
            }
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Client id required";
            $array['data'] = (object)array();
            return $array;
        }

        
    }



    public function addClientTracker(Request $request)
    {
        $data = $request->all();

        if(isset($data['created_by']) && $data['created_by'])
        {
            $createdBy = $data['created_by'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Created user id is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['domain_name']) && $data['domain_name'])
        {
            $domainName = $data['domain_name'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Domain name is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['client_name']) && $data['client_name'])
        {
            $clientName = $data['client_name'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Domain name is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['website']) && $data['website'])
        {
            $website = $data['website'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Website url is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['address']) && $data['address'])
        {
            $address = $data['address'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Address is required";
            $array['data'] = (object)array();
            return $array;
        }


        if(isset($data['location']) && $data['location'])
        {
            $location = $data['location'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Location is required";
            $array['data'] = (object)array();
            return $array;
        }

        $tracker = new ClientTracker;
        $tracker->created_by = $createdBy;
        $tracker->domain_name = $domainName;
        $tracker->client_name = $clientName;
        $tracker->website    = $website;
        $tracker->location    = $location;
        $tracker->address    = $address;
        $tracker->created_at = date('Y-m-d H:i:s');
        $tracker->updated_at = date('Y-m-d H:i:s');
        $tracker->save();

        $array['success'] = true;
        $array['message'] = "Tracker details saved successfully";
        $array['data'] = (object)array();
        return $array;
    }

    public function updateClientTracker(Request $request)
    {
        $data = $request->all();

        if(isset($data['tracker_id']) && $data['tracker_id'])
        {
            $rackerId = $data['tracker_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Tracker id is required";
            $array['data'] = (object)array();
            return $array;
        }

        $trackerDetails = array();
        if(isset($data['total_vacancy']) && $data['total_vacancy'])
        {
            $trackerDetails['total_vacancy'] = $data['total_vacancy'];
        }

        if(isset($data['min_experience']) && $data['min_experience'])
        {
            $trackerDetails['min_experience'] = $data['min_experience'];
        }

        if(isset($data['max_experience']) && $data['max_experience'])
        {
            $trackerDetails['max_experience'] = $data['max_experience'];
        }

        if(isset($data['education_sub_category_id']) && $data['education_sub_category_id'])
        {
            $trackerDetails['education_sub_category_id'] = $data['education_sub_category_id'];
        }

        if(isset($data['gender_preference']) && $data['gender_preference'])
        {
            $trackerDetails['gender_preference'] = $data['gender_preference'];
        }

        if(isset($data['other_benifits']) && $data['other_benifits'])
        {
            $trackerDetails['other_benifits'] = $data['other_benifits'];
        }

        if(isset($data['working_hours']) && $data['working_hours'])
        {
            $trackerDetails['working_hours'] = $data['working_hours'];
        }

        if(isset($data['skill_id']) && $data['skill_id'])
        {
            $trackerDetails['skill_id'] = $data['skill_id'];
        }

        if(isset($data['age_criteria']) && $data['age_criteria'])
        {
            $trackerDetails['age_criteria'] = $data['age_criteria'];
        }

        if(isset($data['travelling_required']) && $data['travelling_required'])
        {
            $trackerDetails['travelling_required'] = $data['travelling_required'];
        }

        if(isset($data['working_days']) && $data['working_days'])
        {
            $trackerDetails['working_days'] = $data['working_days'];
        }

        if(isset($data['other_skills']) && $data['other_skills'])
        {
            $trackerDetails['other_skills'] = $data['other_skills'];
        }

        if(isset($data['max_offered_ctc']) && $data['max_offered_ctc'])
        {
            $trackerDetails['max_offered_ctc'] = $data['max_offered_ctc'];
        }

        if(isset($data['min_offered_ctc']) && $data['min_offered_ctc'])
        {
            $trackerDetails['min_offered_ctc'] = $data['min_offered_ctc'];
        }

        if(isset($data['location']) && $data['location'])
        {
            $trackerDetails['location'] = $data['location'];
        }
        $trackerDetails['updated_at'] = date('Y-m-d H:i:s');

        ClientTracker::where('id', $rackerId)->update($trackerDetails);

        $array['success'] = true;
        $array['message'] = "Tracker details updated successfully";
        $array['data'] = (object)array();
        return $array;
    }


    # Function : This function is used to fetch admin and super admin dashboard job and candidate count details
    # Request  : None
    # Response : success true/false json response
    # Author   : Brijendra
    public function getDashboardDetails(Request $request)
    {
        $data = $request->all();
        $result = array();
        $todayDate = date('Y-m-d 00:00:00');
        $sevenDaysAgoDate = date('Y-m-d 00:00:00', strtotime('-7 days'));
        
        //Jobs (dashboard card 1)       
        $jobs        = Jobs::where('is_job_delete', 0)->get();
        $activeJobs  = DB::table('jobs')->select(DB::raw('SUM(jobs.vacancy) as active_job'))->join('client_tracker', 'client_tracker.id', '=', 'jobs.client_id')->where('client_tracker.is_deleted', 0)->where('is_job_open', 'Open')->get();
        $expireJobs  = Jobs::where('is_job_open', 'Close')->where('is_job_delete', '!=', 1)->get();
        $newJobs     = Jobs::where('created_at','>=', $sevenDaysAgoDate)
                            ->where('created_at','<=', $todayDate)
                            ->where('is_job_open', 'Open')
                            ->get();
        $result['total_jobs']   = count($jobs);
        $result['active_jobs']  = isset($activeJobs[0]->active_job) ? $activeJobs[0]->active_job : 0;
        $result['expired_jobs'] = count($expireJobs);
        $result['new_jobs']     = count($newJobs);

        //Candidate (dashboard card 2)
        $candidates        = Seeker::all();
        $activeCandidates  = Seeker::where('status', 'Active')->get();
        $blockedCandidates = Seeker::where('status', 'Blocked')->get();
        $newCandidates     = Seeker::where('created_at','>=', $sevenDaysAgoDate)
                            ->where('created_at','<=', $todayDate)
                            ->where('status', 'Active')
                            ->get();
        
        $result['total_candidates']   = count($candidates);
        $result['active_candidates']  = count($activeCandidates);
        $result['blocked_candidates'] = count($blockedCandidates);
        $result['new_candidates']     = count($newJobs);


        //(dashboard card 3)
        $appliedJobs = ApplyJob::all();
        $todayLastDate = date('Y-m-d 23:59:59');
        $scheduleTime = JobInterview::where('schedule_time','>=', $todayDate)->where('schedule_time','<=', $todayLastDate)->count();
        $reScheduleTime = JobInterview::where('re_schedule_time','>=', $todayDate)->where('re_schedule_time','<=', $todayLastDate)->count();
        $resumeAllotedToday = JobInterview::where('created_at','>=', $todayDate)->where('created_at','<=', $todayLastDate)->count();
        $selectedCandidate = JobInterview::where('selected_time','>=', $todayDate)->where('selected_time','<=', $todayLastDate)->count();
        $rejectedCandidate = JobInterview::where('rejected_time','>=', $todayDate)->where('rejected_time','<=', $todayLastDate)->count();

        $result['total_applied_jobs']            = count($appliedJobs);
        $result['candidates_rejected_today']     = $rejectedCandidate;

        //(dashboard card 4)
        $totalResumes = Seeker::where('resume', '!=', '')->get();
        $result['total_resume'] = count($totalResumes);
        $days   = 7;
        $m  = date("m"); 
        $d  = date("d"); 
        $y  = date("Y");

        $dateArray = array();
        for($i=0; $i <= $days-1; $i++)
        {
            $dateArray[] = date('Y-m-d', mktime(0,0,0,$m,($d-$i),$y)) ; 
        }

        $dayWiseResumeArray = array();
        $resume = array();
        foreach ($dateArray as $key => $date) 
        {
            $day       = date("l", strtotime($date));
            $startDate = date("Y-m-d 00:00:00", strtotime($date));
            $endDate   = date("Y-m-d 24:59:59", strtotime($date));
            $resumeDettails  = Seeker::where('resume', '!=', '')
                                    ->where('created_at','>=', $startDate)
                                    ->where('created_at','<=', $endDate)
                                    ->get();

            $resume['label'] = $day;
            $resume['value'] = count($resumeDettails);
            array_push($dayWiseResumeArray, $resume);
        }

        $result['resume_graph_data'] = array_reverse($dayWiseResumeArray);

        //(dashboard card 5)
        $result['resume_alloted_today']          = $resumeAllotedToday;
        $result['candidates_interviewed_today']  = $scheduleTime + $reScheduleTime;
        $result['candidates_selected_today']     = $selectedCandidate;

        //(dashboard card 6)
        $userDetails  = User::all();
        // $superAdmin  = User::where('role', 'super admin')->get();
        $adminIds = array();
        $superAdminIds = array();
        $usersCountArray = array();
        foreach ($userDetails as $key => $value) 
        {
            if($value->role == 'admin')
            {
                array_push($adminIds, $value->id);
            }
            if($value->role == 'super admin')
            {
                array_push($superAdminIds, $value->id);
            }

        }

        $adminArray['label'] = 'Admin';
        $adminArray['value'] = count($adminIds);
        array_push($usersCountArray, $adminArray);

        $superAdminArray['label'] = 'Super Admin';
        $superAdminArray['value'] = count($superAdminIds);
        array_push($usersCountArray, $superAdminArray);

        $result['users_count']          = $usersCountArray;
        // $result['total_admin']          = count($adminIds);
        // $result['total_superadmin']     = count($superAdminIds);

        //Daily progress report (dashboard card 7)
        // $result['resume_alloted_today'] = 0;
        $result['interview_scheduled']  = $scheduleTime + $reScheduleTime;
        $result['target_achieve']       = 0;
        $result['candidate_placed']     = $selectedCandidate;
        $result['target_done']          = 10;
        $result['target_from']          = 100;

        $userAllotment = UserAllotment::where('status','Active')->get();
        $userIds = array();

        foreach ($userAllotment as $key => $value) 
        {
            array_push($userIds, $value->user_id);
        }

        $startTime = date('Y-m-d 00:00:00');
        $endTime = date('Y-m-d 23:59:59');
        $adminSchedule = JobInterview::where('created_at','>=', $startTime)->where('created_at','<=', $endTime)->whereIn('schedule_by', $adminIds)->count();
        $superAdminSchedule = JobInterview::where('created_at','>=', $startTime)->where('created_at','<=', $endTime)->whereIn('schedule_by', $superAdminIds)->count();
        $userScheduleResult = array();
        $userScheduleResult['admin'] = $adminSchedule;
        $userScheduleResult['super_admin'] = $superAdminSchedule;

        $jobInterviewDetails = JobInterview::select(DB::raw("COUNT(id) as user_count"),'schedule_by')->where('created_at','>=', $startTime)->where('created_at','<=', $endTime)->groupBy('schedule_by')->get();

        $userDetails = User::whereIn('id', $userIds)->get()->keyBy('id');
        $allotmentUserInfo = array();
        foreach ($userAllotment as $key => $value) 
        {
            $allotmentResult = array();
            if(isset($userDetails[$value->user_id]))
            {
                $allotmentResult['user_name'] = $userDetails[$value->user_id]['name'];
                $scheduleResume = 0;
                if(isset($jobInterviewDetails[$value->user_id]))
                {
                    $scheduleResume = $jobInterviewDetails[$value->user_id]['user_count'];
                }

                $allotmentResult['scheduleResume'] = $scheduleResume;
                $allotmentResult['allotmentCount'] = $value->alloted;
                if($value->alloted > 0)
                {
                    $allotmentResult['allotment_percentage'] = ($scheduleResume/$value->alloted)*100;
                }
                else
                {
                    $allotmentResult['allotment_percentage'] = 0;
                }

                array_push($allotmentUserInfo, $allotmentResult);
            }
        }
        $result['allotmentUserInfo'] = $allotmentUserInfo;
        $result['userScheduleResult'] = $userScheduleResult;

        $result['today_registered'] = Seeker::where('created_at','>=', date('Y-m-d 00:00:00'))->where('created_at','<=', date('Y-m-d 23:59:59'))->count();
        $result['app_download'] = AppDownload::where('created_at','>=', date('Y-m-d 00:00:00'))->where('created_at','<=', date('Y-m-d 23:59:59'))->count();
        $result['vacancy_closed'] = Jobs::where('job_close_date','>=', date('Y-m-d 00:00:00'))->where('job_close_date','<=', date('Y-m-d 23:59:59'))->count();
        return $result;
        
    }

    public function getSpecificJobDetails(Request $request)
    {
        $data = $request->all();
        if (isset($data['job_id']) && $data['job_id']) 
        {
            $jobId = $data['job_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Job id required";
            $array['data'] = (object)array();
            return $array;
        }

        $jobDetails = Jobs::where('id', $jobId)->where('is_job_open', 'Open')->first();
        if ($jobDetails != '') 
        {
            $commonFunction = new CommonController;
            $jobDetails->min_salary = $commonFunction->moneyNumberFormat($jobDetails->min_salary);
            $jobDetails->max_salary = $commonFunction->moneyNumberFormat($jobDetails->max_salary);

            $array['success'] = true;
            $array['message'] = "Job Details Found";
            $array['data']    = $jobDetails;
            return $array;
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "This job has been closed or not found";
            $array['data'] = (object)array();
            return $array;
        }

    }

    public function closeJob(Request $request)
    {
        $data = $request->all();
        if (isset($data['job_id']) && $data['job_id']) 
        {
            $jobId = $data['job_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Job id required";
            $array['data'] = (object)array();
            return $array;
        }

        $jobDetails = Jobs::where('id', $jobId)->where('is_job_open', 'Open')->first();
        if ($jobDetails != '') 
        {
            $jobDetails->is_job_open = "Close";
            $jobDetails->job_close_date = date('Y-m-d H:i:s');
            $jobDetails->save();

            $array['success'] = true;
            $array['message'] = "Job Successfully Close";
            $array['data'] = (object)array();
            return $array;
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "This job has been closed or not found";
            $array['data'] = (object)array();
            return $array;
        }

    }

    public function deleteJob(Request $request)
    {
        $data = $request->all();
        if (isset($data['job_id']) && $data['job_id']) 
        {
            $jobId = $data['job_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Job id required";
            $array['data'] = (object)array();
            return $array;
        }

        $jobDetails = Jobs::where('id', $jobId)->where('is_job_open', 'Open')->first();
        if ($jobDetails != '') 
        {
            $jobDetails->is_job_delete = 1;
            $jobDetails->is_job_open   = "Close";
            $jobDetails->save();

            $array['success'] = true;
            $array['message'] = "Job Successfully Delete";
            $array['data'] = (object)array();
            return $array;
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "This job has been closed or not found";
            $array['data'] = (object)array();
            return $array;
        }

    }


    public function editJobDetails(Request $request)
    {
        $data = $request->all();
        if (isset($data['job_id']) && $data['job_id']) 
        {
            $jobId = $data['job_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Job id required";
            $array['data'] = (object)array();
            return $array;
        }

        $jobDetails = Jobs::where('id', $jobId)->where('is_job_open', 'Open')->first();
            
        if ($jobDetails != '') 
        {
            if ($jobDetails->skill_ids != "") 
            {
                $skilIds = "[".$jobDetails->skill_ids."]";
                $decodeIdsData = json_decode($skilIds, true);
                $jobDetails->skill_ids = array_map('strval',$decodeIdsData);

                $minExp = explode('.', $jobDetails->min_experience);
                $jobDetails->min_experience = $minExp[0];
                if (isset($minExp[1])) 
                {
                    $jobDetails->min_experience_month = (int)$minExp[1];
                }
                else
                {

                    $jobDetails->min_experience_month = 0;
                }

                $maxExp = explode('.', $jobDetails->max_experience);
                $jobDetails->max_experience = $maxExp[0];
                if (isset($maxExp[1])) 
                {
                    $jobDetails->max_experience_month = (int)$maxExp[1];
                }
                else
                {

                    $jobDetails->max_experience_month = 0;
                }

                $clientDetails = ClientTracker::where('id', $jobDetails->client_id)->first();
                $jobDetails->client = $clientDetails->client_name;

            }
            else
            {
                $jobDetails->skill_ids = array();
            }
            $array['success'] = true;
            $array['message'] = "Job Created Successfully";
            $array['data'] = $jobDetails;
            return $array;
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Job Details no found";
            $array['data'] = (object)array();
            return $array;
        }

    }

    public function updateJob(Request $request)
    {
        $data = $request->all();
        
        if(isset($data['id']) && $data['id'])
        {
            $jobId = $data['id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Job id is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['designation']) && $data['designation'])
        {
            $designation = $data['designation'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Designation is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['job_type']) && $data['job_type'])
        {
            $jobType = $data['job_type'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Job type is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['job_description']) && $data['job_description'])
        {
            $jobDescription = $data['job_description'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Job description is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['job_location']) && $data['job_location'])
        {
            $jobLocation = $data['job_location'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Job location is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['min_experience']))
        {
            $minExperienceYears = (string)$data['min_experience'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Min experience year is required";
            $array['data'] = (object)array();
            return $array;
        }

        $minExperienceMonth = "0";
        if(isset($data['min_experience_month']))
        {
            $minExperienceMonth = (string)$data['min_experience_month'];
        }

        $minExperience = $minExperienceYears.".".$minExperienceMonth;


        if(isset($data['max_experience']) && $data['max_experience'])
        {
            $maxExperienceYears = (string)$data['max_experience'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Max experience year is required";
            $array['data'] = (object)array();
            return $array;
        }

        $maxExperienceMonth = "0";
        if(isset($data['max_experience_month']))
        {
            $maxExperienceMonth = (string)$data['max_experience_month'];
        }

        $maxExperience = $maxExperienceYears.".".$maxExperienceMonth;


        if(isset($data['education_category_id']) && $data['education_category_id'])
        {  
            $educationCategoryId  = $data['education_category_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Category is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['sub_education_category_id']) && $data['sub_education_category_id'])
        {  
            $educationSubCategoryId  = $data['sub_education_category_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Sub Category is required";
            $array['data'] = (object)array();
            return $array;
        }

        $salaryType = 'LPA';
        if(isset($data['salary_type']) && $data['salary_type'])
        {
            $salaryType = $data['salary_type'];
        }

        if(isset($data['min_salary']) && $data['min_salary'])
        {
            $minSalary = $data['min_salary'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "min salary is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['max_salary']) && $data['max_salary'])
        {
            $maxSalary = $data['max_salary'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Max salary is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['vacancy']) && $data['vacancy'])
        {
            $vacancy = $data['vacancy'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Vacancy is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['skill_ids']) && $data['skill_ids'])
        {
            $skillIds = implode(',', $data['skill_ids']);
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Skill is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['industry_type']) && $data['industry_type'])
        {  
            $industryType  = $data['industry_type'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "industry type is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['desire_candidate']) && $data['desire_candidate'])
        {
            $desireCandidate = $data['desire_candidate'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Job Responsibilities and Requirements is required";
            $array['data'] = (object)array();
            return $array;
        }

        $array = array();
        $job   = Jobs::where('id', $jobId)->first();
        if ($job != '') 
        {
            $job->designation     = $designation;
            $job->job_type        = $jobType;
            $job->job_description = $jobDescription;
            $job->job_location    = $jobLocation;
            $job->min_salary      = $minSalary;
            $job->max_salary      = $maxSalary;
            $job->vacancy         = $vacancy;
            $job->skill_ids       = $skillIds;
            $job->industry_type    = $industryType;
            $job->desire_candidate  = $desireCandidate;
            $job->min_experience    = (float)$minExperience;
            $job->max_experience    = (float)$maxExperience;
            $job->salary_type       = $salaryType;
            $job->education_category_id   = $educationCategoryId;
            $job->sub_education_category_id  = $educationSubCategoryId;
            $job->save();

            $array['success'] = true;
            $array['message'] = "Details Successfully Update";
            $array['data'] = (object)array();
            return $array;
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Details Not found";
            $array['data'] = (object)array();
            return $array;
        }
    }


    public function applyJobInterview(Request $request)
    {
        $data = $request->all();

        if (isset($data['job_id']) && $data['job_id'] ) 
        {
            $jobId = $data['job_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Job id is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        if (isset($data['seeker_id']) && $data['seeker_id'] ) 
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Seeker id is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        if (isset($data['schedule_time']) && $data['schedule_time'] ) 
        {
            $scheduleTime = $data['schedule_time'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Schedule time is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        if (isset($data['user_id']) && $data['user_id'] ) 
        {
            $scheduleBy = $data['user_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "User id is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        $jobDetails = JobInterview::where('job_id', $jobId)->where('seeker_id', $seekerId)->first();
        if($jobDetails)
        {
            $array['success'] = false;
            $array['message'] = "Already applied for this job.";
            $array['data'] = (object)array();
            return $array;
        }
        else
        {
            $jobInterview = new JobInterview;
            $jobInterview->job_id    = $jobId;
            $jobInterview->seeker_id = $seekerId;
            $jobInterview->schedule_time = $scheduleTime;
            $jobInterview->schedule_by = $scheduleBy;
            $jobInterview->status = 'Schedule';
            $jobInterview->created_at = date('Y-m-d H:i:s');
            $jobInterview->updated_at = date('Y-m-d H:i:s');
            $jobInterview->save();

            $emailContent = array();
            $seekerDetails = Seeker::find($seekerId);
            $emailContent['employeName'] = '';
            $emailContent['companyName'] = '';
            $emailContent['location'] = '';
            $emailContent['address'] = '';
            $emailContent['date'] = date('d M Y' , strtotime($jobInterview->created_at));
            $emailContent['day'] = date('D' , strtotime($jobInterview->created_at));
            $emailContent['time'] = date('H:i A' , strtotime($jobInterview->created_at));;
            $email = '';
            if($seekerDetails)
            {
                $emailContent['employeName'] = $seekerDetails->name;
                $email = $seekerDetails->email;
            }

            $jobDetails = Jobs::find($jobId);
            if($jobDetails)
            {
                $clientId = $jobDetails->client_id;
                $trackerDetails = ClientTracker::find($clientId);
                if($trackerDetails)
                {
                    $emailContent['companyName'] = $trackerDetails->domain_name;
                    $emailContent['address'] = $trackerDetails->address;
                }
            }
            $message = '';
            if($email)
            {
                $subject = 'Interview Schedule  '. $emailContent['companyName'] .','.$emailContent['location'];
                Mail::send('emails.interview_schedule', $emailContent, function($message) use ($email, $subject){
                    $message->to($email, 'Get Me Jobs Interview')->subject
                        ($subject);
                    $message->from('info.tirumalajobs@gmail.com');
                });
            }

            $array['success'] = true;
            $array['message'] = "Interview Successfully Scheduled.";
            $array['data'] = (object)array();
            return $array;
        }
    }

    public function reScheduleJobInterview(Request $request)
    {
        $data = $request->all();

        if (isset($data['interview_id']) && $data['interview_id'] ) 
        {
            $interviewId = $data['interview_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Job interview id is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        if (isset($data['re_schedule_time']) && $data['re_schedule_time'] ) 
        {
            $scheduleTime = $data['re_schedule_time'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Re-schedule time is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        $jobDetails = JobInterview::find($interviewId);
        if($jobDetails)
        {
            $seekerId = $jobDetails->seeker_id;
            $jobId = $jobDetails->job_id;
            if($jobDetails->status == 'Re-schedule' || $jobDetails->status == 'Schedule')
            {
                $jobDetails = array();
                $jobDetails['re_schedule_time'] = $scheduleTime;
                $jobDetails['status'] = 'Re-schedule';
                $jobDetails['updated_at'] = date('Y-m-d H:i:s');
                JobInterview::where('id', $interviewId)->update($jobDetails);
                

                $emailContent = array();
                $seekerDetails = Seeker::find($seekerId);
                $emailContent['employeName'] = '';
                $emailContent['companyName'] = '';
                $emailContent['location'] = '';
                $emailContent['address'] = '';
                $emailContent['date'] = date('d M Y' , strtotime($jobDetails['updated_at']));
                $emailContent['day'] = date('D' , strtotime($jobDetails['updated_at']));
                $emailContent['time'] = date('H:i A' , strtotime($jobDetails['updated_at']));;
                $email = '';
                if($seekerDetails)
                {
                    $emailContent['employeName'] = $seekerDetails->name;
                    $email = $seekerDetails->email;
                }

                $jobDetails = Jobs::find($jobId);
                if($jobDetails)
                {
                    $clientId = $jobDetails->client_id;
                    $trackerDetails = ClientTracker::find($clientId);
                    if($trackerDetails)
                    {
                        $emailContent['companyName'] = $trackerDetails->domain_name;
                        $emailContent['address'] = $trackerDetails->address;
                    }
                }
                $message = '';
                if($email)
                {
                    $subject = 'Interview Re-Schedule  '. $emailContent['companyName'] .','.$emailContent['location'];
                    Mail::send('emails.interview_schedule', $emailContent, function($message) use ($email, $subject){
                        $message->to($email, 'Get Me Jobs Interview')->subject
                            ($subject);
                        $message->from('info.tirumalajobs@gmail.com');
                    });
                }


                $array['success'] = true;
                $array['message'] = "Interview Successfully Re-schedule.";
                $array['data'] = (object)array();
                return $array;
            }
            else
            {
                $array['success'] = false;
                $array['message'] = "You are not able to re-schedule job.";
                $array['data'] = (object)array();
                return $array;
            }
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Job not found.";
            $array['data'] = (object)array();
            return $array;
        }
    }

    public function rejectedJobInterview(Request $request)
    {
        $data = $request->all();

        if (isset($data['interview_id']) && $data['interview_id'] ) 
        {
            $interviewId = $data['interview_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Job interview id is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        $jobDetails = JobInterview::find($interviewId);
        if($jobDetails)
        {
            if($jobDetails->status == 'Re-schedule' || $jobDetails->status == 'Schedule')
            {
                $jobDetails = array();
                $jobDetails['rejected_time'] = date('Y-m-d H:i:s');
                $jobDetails['status'] = 'Rejected';
                $jobDetails['updated_at'] = date('Y-m-d H:i:s');
                JobInterview::where('id', $interviewId)->update($jobDetails);
                
                $array['success'] = true;
                $array['message'] = "Interview Successfully Rejected.";
                $array['data'] = (object)array();
                return $array;
            }
            else
            {
                $array['success'] = false;
                $array['message'] = "You are not able to rejected job.";
                $array['data'] = (object)array();
                return $array;
            }
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Job not found.";
            $array['data'] = (object)array();
            return $array;
        }
    }

    public function completedJobInterview(Request $request)
    {
        $data = $request->all();

        if (isset($data['interview_id']) && $data['interview_id'] ) 
        {
            $interviewId = $data['interview_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Job interview id is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        $jobDetails = JobInterview::find($interviewId);
        if($jobDetails)
        {
            if($jobDetails->status != 'Rejected')
            {
                $jobDetails = array();
                $jobDetails['selected_time'] = date('Y-m-d H:i:s');
                $jobDetails['status'] = 'Selected';
                $jobDetails['updated_at'] = date('Y-m-d H:i:s');
                JobInterview::where('id', $interviewId)->update($jobDetails);
                
                $array['success'] = true;
                $array['message'] = "Candidate Successfully Selected.";
                $array['data'] = (object)array();
                return $array;
            }
            else
            {
                $array['success'] = false;
                $array['message'] = "You have already rejected this candidate.";
                $array['data'] = (object)array();
                return $array;
            }
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Job not found.";
            $array['data'] = (object)array();
            return $array;
        }
    }

    public function candidateApplyJobs(Request $request)
    {
        $data = $request->all();
        if (isset($data['seeker_id']) && $data['seeker_id'] ) 
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Seeker id is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        $applyJobs = ApplyJob::where('seeker_id', $seekerId)->get();
        $jobIds = array();

        foreach ($applyJobs as $key => $value)
        {
            array_push($jobIds, $value->job_id);
        }

        $jobDetails = Jobs::whereIn('id', $jobIds)->get();

        $array['success'] = true;
        $array['message'] = "Job Created Successfully";
        $array['data'] = $jobDetails;
        return $array;
    }

    public function getPostGraduateCategory()
    {
        $educationSubCategory = EducationSubCategory::where('education_category', 4)->get();
        $array['success'] = true;
        $array['message'] = "Post Graduate Details";
        $array['data'] = $educationSubCategory;
        return $array;
    }

    public function getUnderGraduateCategory()
    {
        $educationSubCategory = EducationSubCategory::where('education_category', 3)->get();
        $array['success'] = true;
        $array['message'] = "Under Graduate Details";
        $array['data'] = $educationSubCategory;
        return $array;
    }

    public function getDoctrateOrPHD()
    {
        $educationSubCategory = EducationSubCategory::where('education_category', 5)->get();
        $array['success'] = true;
        $array['message'] = "Doctrate Details";
        $array['data'] = $educationSubCategory;
        return $array;
    }

    # Function : This function used for get specialization according to multiple category
    # Response : success true json response
    # Author   : Rahul
    public function getMultipleSpecialization(Request $request)
    {
        $data = $request->all();
        $educationSubCategoryId = '';

        if (isset($data['education_sub_category_id']) && $data['education_sub_category_id'] ) 
        {
            $educationSubCategoryId = explode(',', $data['education_sub_category_id']);
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Education category is required.";
            $array['data'] = (object)array();
            return $array;
        }
        
        $specializeDetails = Specialization::select('id','specialize')->whereIn('education_sub_category_id', $educationSubCategoryId)->get();
        
        
        $array['success']  = true;
        $array['message'] = "Specialization details";
        $array['data'] = $specializeDetails;
        return $array;

    }

    public function searchResumeDetails(Request $request)
    {
        $data = $request->all();

        $seekerExpIds = array();
        
        if(isset($data['minimum_experience']) || isset($data['maximum_experience']) || isset($data['min_salary']) || isset($data['max_salary']))
        {
            $seekerExpIds = array();
            $seekerExperienceQuery = SeekerWorkExperience::select('seeker_id');
            $minExp = '';
            if(isset($data['minimum_experience']) && $data['minimum_experience'])
            {
                $minExp = $data['minimum_experience'];
                $seekerExperienceQuery = $seekerExperienceQuery->where('experience','>=', $minExp);
            }

            $maxExp = '';
            if(isset($data['maximum_experience']) && $data['maximum_experience'])
            {
                $maxExp = $data['maximum_experience'];
                $seekerExperienceQuery = $seekerExperienceQuery->where('experience','<=', $maxExp);
            }

            $minSalary = '';
            if(isset($data['min_salary']) && $data['min_salary'])
            {
                $minSalary = $data['min_salary'];
                $seekerExperienceQuery = $seekerExperienceQuery->where('salary','>=', $minSalary);
            }

            $maxSalary = '';
            if(isset($data['max_salary']) && $data['max_salary'])
            {
                $maxSalary = $data['max_salary'];
                $seekerExperienceQuery = $seekerExperienceQuery->where('salary','<=', $maxSalary);
            }

            $designation = '';
            if(isset($data['designation']) && $data['designation'])
            {
                $designation = $data['designation'];
                $seekerExperienceQuery = $seekerExperienceQuery->where('designation', $designation);
            }

            $seekerExperience = $seekerExperienceQuery->groupBy('seeker_id')->get();
            if ($minExp != '' || $maxExp != '' || $minSalary != '' || $maxSalary != '' || $designation != '') 
            {
                foreach ($seekerExperience as $key => $value) 
                {
                    array_push($seekerExpIds, $value->seeker_id);
                }
            }
        }

        $anyKeyword = '';
        $seekerKeywordIds = array();
        if(isset($data['keyword']) && $data['keyword'])
        {
            $seekerKeywordIds[] = 0;
            $anyKeyword = $data['keyword'];
            $roleDetails = Role::where('role','LIKE', '%'.$anyKeyword.'%')->get();
            $roleIds = array();
            foreach ($roleDetails as $key => $value) 
            {
                array_push($roleIds, $value->id);
            }

            $seekerRoleDetails = Seeker::whereIn('role_id', $roleIds)->get();
            foreach ($seekerRoleDetails as $key => $value) 
            {
                array_push($seekerKeywordIds, $value->id);
            }

            $skillDetails = Skill::where('skill', 'LIKE','%'.$anyKeyword.'%')->get();
            $skillIds = array();
            foreach ($skillDetails as $key => $value) 
            {
                array_push($skillIds, $value->id);
            }

            $seekerSkillDetails = Seeker::where('skill_ids', '!=','')->get();
            foreach ($seekerSkillDetails as $key => $value) 
            {
                $skillId = explode(',', $value->skill_ids);
                for($i = 0; $i< count($skillId); $i++)
                {
                    $skill = $skillId[$i];
                    if(in_array($skill, $skillIds))
                    {
                        array_push($seekerKeywordIds, $value->id);
                    }
                }
            }
            // $keywordSeekerIds = array();
            $keywordSeekerDetails = SeekerWorkExperience::where('designation', $anyKeyword)->get();
            foreach ($keywordSeekerDetails as $key => $value)
            {
                array_push($seekerKeywordIds, $value->seeker_id);
            }
        }
        $seekerUgQualificationIds = array();
       
        if((isset($data['ug_qualification']) && $data['ug_qualification']) || (isset($data['ug_year']) && $data['ug_year']) || (isset($data['ug_type']) && $data['ug_type']))
        {
            $seekerUgQualificationIds[] = 0;
            $query = SeekerQualification::select('seeker_id');

            if(isset($data['ug_qualification']) && $data['ug_qualification'])
            {
                $query = $query->where('course','>=', $data['ug_qualification']);
            }

            if(isset($data['ug_year']) && $data['ug_year'])
            {
                $query = $query->where('passout_year','>=', $data['ug_year']);
            }

            if(isset($data['ug_type']) && $data['ug_type'])
            {
                $query = $query->where('education_type','>=', $data['ug_type']);
            }

            $seekerQualification = $query->groupBy('seeker_id')->get();
            if ($data['ug_qualification'] != '' || $data['ug_year'] != '' || $data['ug_type'] != '') 
            {
                foreach ($seekerQualification as $key => $value) 
                {
                    array_push($seekerUgQualificationIds, $value->seeker_id);
                }
            }
        }

        $seekerPgQualificationIds = array();
        if((isset($data['pg_qualification']) && $data['pg_qualification']) || (isset($data['pg_year']) && $data['pg_year']) || (isset($data['pg_type']) && $data['pg_type']))
        {
            $seekerPgQualificationIds[] = 0;
            $query = SeekerQualification::select('seeker_id');

            if(isset($data['pg_qualification']) && $data['pg_qualification'])
            {
                $query = $query->where('course','>=', $data['pg_qualification']);
            }

            if(isset($data['pg_year']) && $data['pg_year'])
            {
                $query = $query->where('passout_year','>=', $data['pg_year']);
            }

            if(isset($data['pg_type']) && $data['pg_type'])
            {
                $query = $query->where('education_type','>=', $data['pg_type']);
            }

            $seekerQualification = $query->groupBy('seeker_id')->get();
            if ($data['pg_qualification'] != '' || $data['pg_year'] != '' || $data['pg_type'] != '') 
            {
                foreach ($seekerQualification as $key => $value) 
                {
                    array_push($seekerPgQualificationIds, $value->seeker_id);
                }
            }
        }
        $query = Seeker::select('*');
            
        $query = $query->where('status', 'Active');

        if(isset($data['employer']) && $data['employer'])
        {
            $query = $query->where('name', 'LIKE', '%'.$data['employer'].'%');
        }

        $isFresher = '';
        if(isset($data['seeker_fresher']))
        {
            $isFresher = $data['seeker_fresher'];
        }

        // $isExperience = false;
        // if(isset($data['seeker_experience']))
        // {
        //     $isExperience = $data['seeker_experience'];
        // }

        if ($isFresher == 'true' && $isFresher != '') 
        {
            $query = $query->where('profile', 'fresher');
        }

        if ($isFresher == 'false' && $isFresher != '') 
        {
            $query = $query->where('profile', 'experience');
        }

        if(isset($data['location']) && $data['location'])
        {
            $locations =  $data['location'];//implode(',', $data['location']);
            $query = $query->where('location', 'LIKE', '%'.$locations.'%');
        }

        if(isset($data['gender']) && $data['gender'])
        {
            $query = $query->where('gender', $data['gender']);
        }

        if(isset($data['age']) && $data['age'])
        {
            $year = date('Y') - $data['age'];
            $dateOfBirth = $year.'-'.date('m-d');
            $query = $query->where('date_of_birth', '<=', $dateOfBirth);
        }

        if(count($seekerExpIds) > 0)
        {
            $query = $query->whereIn('id', $seekerExpIds);
        }

        if(count($seekerPgQualificationIds) > 0)
        {
            $query = $query->whereIn('id', $seekerPgQualificationIds);
        }
        
        if(count($seekerUgQualificationIds) > 0)
        {
            $query = $query->whereIn('id', $seekerUgQualificationIds);
        }

        if(count($seekerKeywordIds) > 0)
        {
            $query = $query->whereIn('id', $seekerKeywordIds);
        }

        $seekerDetails = $query->get();

        if(isset($data['preferred_location']) && $data['preferred_location'])
        {
            $preferredSearching = $data['preferred_location'];
            $preferedLoicationSearchData = array();
            foreach ($seekerDetails as $key => $value)
            {
                $preferredLocation = $value->preferred_location;
                if($preferredLocation)
                {
                    $preferredLocation = explode(',', $preferredLocation);
                    foreach($preferredLocation as $keyPre => $valuePre) 
                    {
                        if(in_array($valuePre, $preferredSearching))
                        {
                            array_push($preferedLoicationSearchData, $value);
                        }
                    }
                }
            }

            $seekerDetails = $preferedLoicationSearchData;
        }

        $array['success']  = true;
        $array['message'] = "Seeker Details";
        $array['data'] = $seekerDetails;
        return $array;
    }

    public function getJobsDetails(Request $request)
    {
        $data = $request->all();
        $location = '';
        $qualification = '';
        $experience = '';
        $skills = '';
        $category = '';

        $isFilter = 'false';
        if (isset($data['is_filter']) && $data['is_filter']) 
        {
            $isFilter = $data['is_filter'];
        }

        if ($isFilter == 'true') 
        {

            $query = Jobs::select('jobs.*')->join('client_tracker', 'client_tracker.id', '=', 'jobs.client_id')->where('client_tracker.is_deleted', 0)->where('is_job_delete', 0);
            if (isset($data['designation']) && $data['designation'] != '') 
            {
                $query = $query->where('designation', $data['designation']);
            }

            if (isset($data['qualification']) && $data['qualification'] != '') 
            {
                $query = $query->where('education_category_id', $data['qualification']);
            }

            if (isset($data['industry']) && $data['industry'] != '') 
            {
                $query = $query->where('industry_type', $data['industry']);
            }

            if (isset($data['experience'])) 
            {
                $query = $query->where('min_experience', '<=', $data['experience']);
            }

            if (isset($data['location']) && $data['location'] != '') 
            {
                $query = $query->where('job_location','LIKE', $data['location']);
            }

            $jobDetails = $query->where('is_job_open','Open')->get();

            if (isset($data['skill']) && $data['skill'] != '') 
            {
                $newJobIdsArray = array();
                foreach ($jobDetails as $key => $value) {
                    
                    $idsArray = explode(',', $value['skill_ids']);
                    if(in_array($data['skill'], $idsArray))
                    {
                        array_push($newJobIdsArray, $value->id);
                    }
                }

                $jobDetails = Jobs::whereIn('id',$newJobIdsArray)->get();
            }

            $array['success'] = true;
            $array['message'] = "Job details";
            $array['data'] = $jobDetails;
            return $array;
        }
        else
        {
            $jobDetails = Jobs::select('jobs.*')->join('client_tracker', 'client_tracker.id', '=', 'jobs.client_id')->where('is_job_open','Open')->where('is_job_delete', 0)->where('client_tracker.is_deleted', 0)->orderBy('id','DESC')->get();

            $array['success'] = true;
            $array['message'] = "Job details";
            $array['data'] = $jobDetails;
            return $array;
        }


    }

    # Function : This function used to fetch recommended jobs list
    # Request  : seeker id ,is filter, filter by
    # Response : success true/false json response
    # Author   : Brijendra
    public function getRecommendedJobsList(Request $request)
    {

        $data = $request->all();
       
        if (isset($data['seeker_id']) && $data['seeker_id'] ) 
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Seeker id is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        $seeker = Seeker::where('id', $seekerId)->first();
        if ($seeker == '') 
        {
            $array['success'] = false;
            $array['message'] = "Seeker Not exist";
            $array['data'] = (object)array();
            return $array;
        }

        $applyJobs = ApplyJob::where('seeker_id', $seekerId)->get()->keyBy('job_id');
        $skillsDetails = Skill::get()->keyBy('id')->toArray();

        $jobs = Jobs::where('job_location', $seeker->location)->where('is_job_open', 'Open')->orderBy('id', 'desc')->take(10)->get();
        $savedJobs = SaveJob::where('seeker_id', $seekerId)->get()->keyBy('job_id');

        foreach ($jobs as $key => $value) 
        {

            $skillIds = $value['skill_ids'];
            $value['all_skills'] = '';
            if($skillIds)
            {
                $skills = '';
                $skillIds = explode(',', $skillIds);
                foreach ($skillIds as $key => $skillval) 
                {
                    if(isset($skillsDetails[$skillval]))
                    {
                        if($skills)
                        {
                            $skills .= ', '. $skillsDetails[$skillval]['skill'];
                        }
                        else
                        {
                            $skills = $skillsDetails[$skillval]['skill'];
                        }
                    }
                }
                $value['all_skills'] = $skills;
            }

            $value['created_date'] = date('d-m-Y', strtotime($value->created_at));
        }

        $array['success'] = true;
        $array['message'] = "Record Found";
        $array['data']    = $jobs;
        return $array;
            
        

    }

    public function getJobRecommendedCandidates(Request $request)
    {

        $data = $request->all();
       
        if (isset($data['job_id']) && $data['job_id'] ) 
        {
            $jobId = $data['job_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Job id is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        
        $jobDetails = Jobs::where('id', $jobId)->first();
        if ($jobDetails != "") 
        {
            $seekers = Seeker::where('location', $jobDetails->job_location)->get();
            $seeketIdsArray = array();
            foreach ($seekers as $key => $value) 
            {
                array_push($seeketIdsArray, $value->id);
            }
            $interviewDetails = JobInterview::where('job_id', $jobId)->whereIn('seeker_id', $seeketIdsArray)->get()->keyBy('seeker_id');

            $seekerWorkExp = SeekerWorkExperience::whereIn('seeker_id',$seeketIdsArray)->where('experience', '>=',$jobDetails->min_experience)->get()->keyBy('seeker_id');
            foreach ($seekers as $key => $value) 
            {
                if (isset($seekerWorkExp[$value->id])) 
                {
                    $value->experience = $seekerWorkExp[$value->id]['experience'];
                    $value->salary     = $seekerWorkExp[$value->id]['salary'];
                }
                else
                {
                    $value->experience = 0;
                    $value->salary     = 0;
                }

                $value->interview_id = '';
                $value->interview_status = '';
                $value->profile_image = 'http://getmejobs.triwea.com'.$value->profile_image;
                if(isset($interviewDetails[$value->id]))
                {
                    $value->interview_id = $interviewDetails[$value->id]['id'];
                    $value->interview_status = $interviewDetails[$value->id]['status'];
                }

            }

            $skillsDetails = Skill::get()->keyBy('id')->toArray();
            foreach ($seekers as $key => $value)
            {
                $skillIds = $value['skill_ids'];
                $seekerId = $value['id'];
                $value['all_skills'] = '';
                if($skillIds)
                {
                    $skills = '';
                    $skillIds = explode(',', $skillIds);
                    foreach ($skillIds as $key => $skillval) 
                    {
                        if(isset($skillsDetails[$skillval]))
                        {
                            if($skills)
                            {
                                $skills .= ','. $skillsDetails[$skillval]['skill'];
                            }
                            else
                            {
                                $skills = $skillsDetails[$skillval]['skill'];
                            }
                        }
                    }
                    
                    $value['all_skills'] = $skills;
                }
            }
            $array['success'] = true;
            $array['message'] = "Record Found";
            $array['data']    = $seekers;
            return $array;
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Details Not found";
            $array['data'] = (object)array();
            return $array;
        }
  
    }


    public function jobAppliedCandidates(Request $request)
    {
        $data = $request->all();
       
        if (isset($data['job_id']) && $data['job_id'] ) 
        {
            $jobId = $data['job_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Job id is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        $appliedJobs = ApplyJob::where('job_id', $jobId)->get();
        $appledSeekerIdsArray = array();
        foreach ($appliedJobs as $key => $value) 
        {
            array_push($appledSeekerIdsArray, $value->seeker_id);
        }

        $seekers = Seeker::whereIn('id',$appledSeekerIdsArray)->get();
        $seekerWorkExp = SeekerWorkExperience::whereIn('seeker_id',$appledSeekerIdsArray)->get()->keyBy('seeker_id');
        foreach ($seekers as $key => $value) 
        {
            if (isset($seekerWorkExp[$value->id])) 
            {
                $value->experience = $seekerWorkExp[$value->id]['experience'];
                $value->salary     = $seekerWorkExp[$value->id]['salary'];
            }
            else
            {
                $value->experience = 0;
                $value->salary     = 0;
            }

        }

        $array['success'] = true;
        $array['message'] = "Record found";
        $array['data']    = $seekers;
        return $array;
    }

    public function getAllCandidates(Request $request)
    {
        $data = $request->all();

        $isFilter = 'false';
        if (isset($data['is_filter']) && $data['is_filter'] != '') 
        {
            $isFilter = $data['is_filter'];
        }

        $query = Seeker::select('*')->where('status','Active');

        if (isset($data['location']) && $data['location'] != '') 
        {
            $query = $query->where('location','LIKE', $data['location']);
        }

        $candidateDetails = $query->get();

        if (isset($data['skill_id']) && $data['skill_id'] != '') 
        {
            $newSeekerIdsArray = array();
            foreach ($candidateDetails as $key => $value) {
                
                $idsArray = explode(',', $value['skill_ids']);
                if(in_array($data['skill_id'], $idsArray))
                {
                    array_push($newSeekerIdsArray, $value->id);
                }
            }

            $candidateDetails = Seeker::whereIn('id',$newSeekerIdsArray)->get();
        }

        $seekerIds = array();
        foreach ($candidateDetails as $key => $value) 
        {
            array_push($seekerIds, $value->id);
        }

        $experienceSeekerIdsArray = array();
        $qualificationSeekerIdsArray = array();

        if (isset($data['sub_category']) && $data['sub_category'] != '' && isset($data['category']) && $data['category'] != '' && $isFilter == 'true') 
        {
            $seekerQualification = SeekerQualification::whereIn('seeker_id', $seekerIds)
                                                    ->where('course', $data['sub_category'])
                                                    ->where('highest_education', $data['category'])
                                                    ->orderBy('id','DESC')
                                                    ->groupBy('seeker_id')
                                                    ->get()->keyBy('seeker_id');

            foreach ($seekerQualification as $key => $value) 
            {
                array_push($qualificationSeekerIdsArray, $value->seeker_id);
            }

        }
        else if (isset($data['category']) && $data['category'] != '' && $isFilter == 'true') 
        {
            $seekerQualification = SeekerQualification::whereIn('seeker_id', $seekerIds)
                                                    ->where('highest_education', $data['category'])
                                                    ->orderBy('id','DESC')
                                                    ->groupBy('seeker_id')
                                                    ->get()->keyBy('seeker_id');

            foreach ($seekerQualification as $key => $value) 
            {
                array_push($qualificationSeekerIdsArray, $value->seeker_id);
            }
        }
        else if (isset($data['sub_category']) && $data['sub_category'] != '' && $isFilter == 'true') 
        {
            $seekerQualification = SeekerQualification::whereIn('seeker_id', $seekerIds)
                                                    ->where('course', $data['sub_category'])
                                                    ->orderBy('id','DESC')
                                                    ->groupBy('seeker_id')
                                                    ->get()->keyBy('seeker_id');

            foreach ($seekerQualification as $key => $value) 
            {
                array_push($qualificationSeekerIdsArray, $value->seeker_id);
            }
        }
        else
        {

            $seekerQualification = SeekerQualification::whereIn('seeker_id', $seekerIds)->orderBy('id','DESC')->groupBy('seeker_id')->get()->keyBy('seeker_id');
        }

        if (isset($data['experience']) && $isFilter == 'true') 
        {
            $seekerExperience = SeekerWorkExperience::whereIn('seeker_id', $seekerIds)
                                                    ->where('experience', '<=', $data['experience'])
                                                    ->orderBy('id','DESC')
                                                    ->groupBy('seeker_id')
                                                    ->get()->keyBy('seeker_id');

            foreach ($seekerExperience as $key => $value) 
            {
                array_push($experienceSeekerIdsArray, $value->seeker_id);
            }
        }
        else  
        {
            $seekerExperience = SeekerWorkExperience::whereIn('seeker_id', $seekerIds)
                                                    ->orderBy('id','DESC')
                                                    ->groupBy('seeker_id')
                                                    ->get()->keyBy('seeker_id');
        }

        if (count($experienceSeekerIdsArray) > 0 && count($qualificationSeekerIdsArray) > 0)
        {
            $finalSeekerIdsArray = array_intersect($qualificationSeekerIdsArray, $experienceSeekerIdsArray);
            $candidateDetails    = Seeker::whereIn('id',$finalSeekerIdsArray)->get();
        }
        else if (count($experienceSeekerIdsArray) > 0 && count($qualificationSeekerIdsArray) == 0) 
        {
            $candidateDetails    = Seeker::whereIn('id',$experienceSeekerIdsArray)->get();
        }
        else if (count($experienceSeekerIdsArray) == 0 && count($qualificationSeekerIdsArray) > 0) 
        {
            $candidateDetails    = Seeker::whereIn('id',$qualificationSeekerIdsArray)->get();
        }
        else if (isset($data['category']) && isset($data['sub_category']) && isset($data['experience']) && count($experienceSeekerIdsArray) == 0 && count($qualificationSeekerIdsArray) == 0 && $isFilter == 'true') 
        {
            $candidateDetails    = Seeker::whereIn('id',$qualificationSeekerIdsArray)->get();
        }


        $seekerLastLogin = SeekerLoginTrack::orderBy('login_time','DESC')
                                                ->get();
        $seekerLastLoginData = array();
        foreach ($seekerLastLogin as $key => $value) 
        {
            if(!isset($seekerLastLoginData[$value->seeker_id]))
            {
                $seekerLastLoginData[$value->seeker_id]['login_time'] = $value->login_time;
            }
        }
        // $profileViewerData = array();
        // if(isset($data['view_profile_ids']) && $data['view_profile_ids'])
        // {
        //     $profileViewerData = explode(',', $data['view_profile_ids']);
        // }
        $skillsDetails = Skill::get()->keyBy('id')->toArray();
        foreach ($candidateDetails as $key => $value)
        {
            $skillIds = $value['skill_ids'];
            $seekerId = $value['id'];
            // $value['all_skills'] = '';
            if($value->view_profile == 1)
            {
                $value['view_profile'] = true;
            }
            else
            {
                 $value['view_profile'] = false;
            }
            if($skillIds)
            {
                $skills = '';
                $skillIds = explode(',', $skillIds);
                foreach ($skillIds as $key => $skillval) 
                {
                    if(isset($skillsDetails[$skillval]))
                    {
                        if($skills)
                        {
                            $skills .= ','. $skillsDetails[$skillval]['skill'];
                        }
                        else
                        {
                            $skills = $skillsDetails[$skillval]['skill'];
                        }
                    }
                }
                // $skills = '';
                $value['all_skills'] = $skills;
            }
            
            if(isset($seekerExperience[$seekerId]))
            {
                $value['designation'] = $seekerExperience[$seekerId]['designation'];
                $value['experience']  = $seekerExperience[$seekerId]['experience'].' Year';
                $value['salary']      = $seekerExperience[$seekerId]['salary'];
            }
            else
            {
                $value['designation'] = '';
                $value['experience'] = '0 Year';
                $value['salary'] = '';
            }

            if(isset($seekerQualification[$seekerId]))
            {
                $value['highest_education'] = $seekerQualification[$seekerId]['highest_education'];
                $value['course'] = $seekerQualification[$seekerId]['course']; 
                $value['specialization'] = $seekerQualification[$seekerId]['specialization'];
            }
            else
            {
                $value['highest_education'] = '';
                $value['course'] = '';
                $value['specialization'] = '';
            }

            if(isset($seekerLastLoginData[$seekerId]))
            {
                $value['login_time'] = $seekerLastLoginData[$seekerId]['login_time'];
            }
            else
            {
                $value['login_time'] = date('Y-m-d H:i:s', strtotime($value['updated_at']));
            }
        }

        $array['success'] = true;
        $array['message'] = "All Candidates Details";
        $array['data'] = $candidateDetails;
        return $array;
    }

    public function getAllotmentUser(Request $request)
    {
        $userDetails = User::select('id','name','email','mobile','role')->where('status','Active')->get();

        $userAllotment = UserAllotment::get()->keyBy('user_id');
        foreach ($userDetails as $key => $value) 
        {
            if(isset($userAllotment[$value->id]))
            {
                $value->status = $userAllotment[$value->id]['status'];
                $value->alloted = $userAllotment[$value->id]['alloted'];
            }
            else
            {
                $value->status = 'Inactive';
                $value->alloted = 0;
            }
        }

        $array['success'] = true;
        $array['message'] = "Allotement Users";
        $array['data'] = $userDetails;
        return $array;
    }

    public function updateAllotmentUser(Request $request)
    {
        $data = $request->all();

        if(isset($data['alloted_by']) && $data['alloted_by'])
        {
            $allotedBy = $data['alloted_by'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Alloted by is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['user_id']) && $data['user_id'])
        {
            $userId = $data['user_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "User id is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['status']) && $data['status'])
        {
            $status = $data['status'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Status is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['alloted']) && $data['alloted'])
        {
            $alloted = $data['alloted'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Allotment number is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        $userAllotment = UserAllotment::where('user_id', $userId)->first();
        if($userAllotment)
        {   
            $userAllotment->user_id = $userId;
            $userAllotment->alloted = $alloted;
            $userAllotment->status = $status;
            $userAllotment->alloted_by = $allotedBy;
            $userAllotment->updated_at = date('Y-m-d H:i:s');
            $userAllotment->save();

            $array['success'] = true;
            $array['message'] = "User Allotment Successfully Updated.";
            $array['data'] = (object)array();
            return $array;
        }
        else
        {
            $userAllotment = new UserAllotment;
            $userAllotment->user_id = $userId;
            $userAllotment->alloted = $alloted;
            $userAllotment->status = $status;
            $userAllotment->alloted_by = $allotedBy;
            $userAllotment->created_at = date('Y-m-d H:i:s');
            $userAllotment->updated_at = date('Y-m-d H:i:s');
            $userAllotment->save();

            $array['success'] = true;
            $array['message'] = "User Allotment Successfully Updated.";
            $array['data'] = (object)array();
            return $array;
        }
    }


    public function addUniversity(Request $request)
    {
        $data = $request->all();
        if(isset($data['university_name']) && $data['university_name'])
        {
            $universityName = $data['university_name'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "University name is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['location']) && $data['location'])
        {
            $location = $data['location'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "University name is required";
            $array['data'] = (object)array();
            return $array;
        }
        
        $university = new University;
        $university->university_name = $universityName;
        $university->location        = $location;
        $university->save();
        
        $array = array();
        $array['success'] = true;
        $array['message'] = "University Successfully Added.";
        $array['data'] = (object)array();
        return $array;
    }

    public function updateUniversity(Request $request)
    {
        $data = $request->all();
        if(isset($data['university_id']) && $data['university_id'])
        {
            $universityId = $data['university_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "University id is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['university_name']) && $data['university_name'])
        {
            $universityName = $data['university_name'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "University name is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['university_location']) && $data['university_location'])
        {
            $location = $data['university_location'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "University location is required";
            $array['data'] = (object)array();
            return $array;
        }
        
        $university = University::find($universityId);
        $university->university_name = $universityName;
        $university->location        = $location;
        $university->save();
        
        $array = array();
        $array['success'] = true;
        $array['message'] = "University Successfully Update.";
        $array['data'] = (object)array();
        return $array;
    }

    public function addDepartment(Request $request)
    {
        $data = $request->all();
        if(isset($data['department_name']) && $data['department_name'])
        {
            $departmentName = $data['department_name'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Department name is required";
            $array['data'] = (object)array();
            return $array;
        }

        
        $department = new Department;
        $department->department_name = $departmentName;
        $department->save();
        
        $array = array();
        $array['success'] = true;
        $array['message'] = "Department Successfully Added.";
        $array['data'] = (object)array();
        return $array;
    }

    public function updateDepartment(Request $request)
    {
        $data = $request->all();
        if(isset($data['department_id']) && $data['department_id'])
        {
            $departmentId = $data['department_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Department id is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['department_name']) && $data['department_name'])
        {
            $departmentName = $data['department_name'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Department name is required";
            $array['data'] = (object)array();
            return $array;
        }

        
        $department = Department::find($departmentId);
        $department->department_name = $departmentName;
        $department->save();
        
        $array = array();
        $array['success'] = true;
        $array['message'] = "Department Successfully Update.";
        $array['data'] = (object)array();
        return $array;
    }

    public function deleteCategory(Request $request)
    {
        $data = $request->all();
        if(isset($data['category_id']) && $data['category_id'])
        {
            $categoryId = $data['category_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Category Id is required";
            $array['data'] = (object)array();
            return $array;
        }
        
        $educationCategory = EducationCategory::find($categoryId);

        if($educationCategory)
        {
            $educationCategory->delete();
            $array = array();
            $array['success'] = true;
            $array['message'] = "Category Successfully Deleted.";
            $array['data'] = (object)array();
            return $array;
        }
        else
        {
            $array = array();
            $array['success'] = false;
            $array['message'] = "Category not registered.";
            $array['data'] = (object)array();
            return $array;
        }
    }

    public function deleteEducationSubCategories(Request $request)
    {
        $data = $request->all();
        if(isset($data['sub_category_id']) && $data['sub_category_id'])
        {
            $subCategoryId = $data['sub_category_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Sub Category Id is required";
            $array['data'] = (object)array();
            return $array;
        }
        
        $educationSubCategory = EducationSubCategory::find($subCategoryId);

        if($educationSubCategory)
        {
            $educationSubCategory->delete();
            $array = array();
            $array['success'] = true;
            $array['message'] = "Successfully Deleted.";
            $array['data'] = (object)array();
            return $array;
        }
        else
        {
            $array = array();
            $array['success'] = false;
            $array['message'] = "Category not registered.";
            $array['data'] = (object)array();
            return $array;
        }
    }

    public function deleteIndustry(Request $request)
    {
        $data = $request->all();
        if(isset($data['industry_id']) && $data['industry_id'])
        {
            $industryId = $data['industry_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Industry Id is required";
            $array['data'] = (object)array();
            return $array;
        }
        
        $industryDetails = Industry::find($industryId);

        if($industryDetails)
        {
            $industryDetails->delete();
            $array = array();
            $array['success'] = true;
            $array['message'] = "Industry Successfully Deleted.";
            $array['data'] = (object)array();
            return $array;
        }
        else
        {
            $array = array();
            $array['success'] = false;
            $array['message'] = "Industry not registered.";
            $array['data'] = (object)array();
            return $array;
        }
    }

    public function deleteSkills(Request $request)
    {
        $data = $request->all();
        if(isset($data['skill_id']) && $data['skill_id'])
        {
            $skillId = $data['skill_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Skill Id is required";
            $array['data'] = (object)array();
            return $array;
        }
        
        $skillDetails = Skill::find($skillId);

        if($skillDetails)
        {
            $skillDetails->delete();
            $array = array();
            $array['success'] = true;
            $array['message'] = "Skill Successfully Deleted.";
            $array['data'] = (object)array();
            return $array;
        }
        else
        {
            $array = array();
            $array['success'] = false;
            $array['message'] = "Skill not registered.";
            $array['data'] = (object)array();
            return $array;
        }
    }

    public function deleteDesignation(Request $request)
    {
        $data = $request->all();
        if(isset($data['designation_id']) && $data['designation_id'])
        {
            $designationId = $data['designation_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Designation Id is required";
            $array['data'] = (object)array();
            return $array;
        }
        
         $designationDetails = Designations::find($designationId);

        if($designationDetails)
        {
            $designationDetails->delete();
            $array = array();
            $array['success'] = true;
            $array['message'] = "Designation Successfully Deleted.";
            $array['data'] = (object)array();
            return $array;
        }
        else
        {
            $array = array();
            $array['success'] = false;
            $array['message'] = "Designation not registered.";
            $array['data'] = (object)array();
            return $array;
        }
    }
    
    public function deleteDepartment(Request $request)
    {
        $data = $request->all();
        if(isset($data['department_id']) && $data['department_id'])
        {
            $departmentId = $data['department_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Department Id is required";
            $array['data'] = (object)array();
            return $array;
        }
        
         $departmentDetails = Department::find($departmentId);

        if($departmentDetails)
        {
            $departmentDetails->delete();
            $array = array();
            $array['success'] = true;
            $array['message'] = "Department Successfully Deleted.";
            $array['data'] = (object)array();
            return $array;
        }
        else
        {
            $array = array();
            $array['success'] = false;
            $array['message'] = "Department not registered.";
            $array['data'] = (object)array();
            return $array;
        }
    }

    public function deleteUniversity(Request $request)
    {
        $data = $request->all();
        if(isset($data['university_id']) && $data['university_id'])
        {
            $universityId = $data['university_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "University Id is required";
            $array['data'] = (object)array();
            return $array;
        }
        
         $universityDetails = University::find($universityId);

        if($universityDetails)
        {
            $universityDetails->delete();
            $array = array();
            $array['success'] = true;
            $array['message'] = "University Successfully Deleted.";
            $array['data'] = (object)array();
            return $array;
        }
        else
        {
            $array = array();
            $array['success'] = false;
            $array['message'] = "University not registered.";
            $array['data'] = (object)array();
            return $array;
        }
    }

    public function userRegistrationReport(Request $request)
    {
        $data = $request->all();
        if(isset($data['start_date']) && $data['start_date'])
        {
            $startDate = $data['start_date'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Start Date is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['end_date']) && $data['end_date'])
        {
            $endDate = $data['end_date'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "End Date is required";
            $array['data'] = (object)array();
            return $array;
        }

        $userDetails = User::select('name','mobile','email','status','created_at')->where('created_at','>=', $startDate)->where('created_at','<=', $endDate)->get();
        
        $array['success'] = true;
        $array['message'] = "User Details";
        $array['data'] = $userDetails;
        return $array;
    }

    public function getScheduleInterview(Request $request)
    {
        $data = $request->all();
        if(isset($data['start_date']) && $data['start_date'])
        {
            $startDate = $data['start_date'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Start Date is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['end_date']) && $data['end_date'])
        {
            $endDate = $data['end_date'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "End Date is required";
            $array['data'] = (object)array();
            return $array;
        }

        $jobSchedule = JobInterview::where('schedule_time','>=', $startDate)->where('schedule_time','<=', $endDate)->get()->keyBy('seeker_id');
        $seekerIds = array();

        foreach ($jobSchedule as $key => $value) 
        {
            array_push($seekerIds, $value->seeker_id);
        }

        $seekerDetails = Seeker::whereIn('id', $seekerIds)->get();
        foreach ($seekerDetails as $key => $value) 
        {
            if(isset($jobSchedule[$value->id]))
            {
                $value->schedule_time = $jobSchedule[$value->id]['schedule_time'];
            }
            else
            {
                $value->schedule_time = '';
            }
        }

        $array['success'] = true;
        $array['message'] = "Interview Schedule";
        $array['data'] = $seekerDetails;
        return $array;
    }

    public function getApplyJobs(Request $request)
    {
        $data = $request->all();
        if(isset($data['start_date']) && $data['start_date'])
        {
            $startDate = $data['start_date'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Start Date is required";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['end_date']) && $data['end_date'])
        {
            $endDate = $data['end_date'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "End Date is required";
            $array['data'] = (object)array();
            return $array;
        }

        $applyJobDetails = ApplyJob::where('created_at','>=', $startDate)->where('created_at','<=', $endDate)->get();

        $seekerIds = array();
        $jobIds = array();
        foreach ($applyJobDetails as $key => $value) 
        {
            array_push($seekerIds, $value->seeker_id);
            array_push($jobIds, $value->job_id);
        }

        $seekerDetails = Seeker::whereIn('id', $seekerIds)->get()->keyBy('id');
        $jobsDetails = Jobs::whereIn('id', $jobIds)->get()->keyBy('id');

        $applyJobsInfo = array();
        foreach ($applyJobDetails as $key => $value) 
        {
            $result = array();
            if(isset($seekerDetails[$value->seeker_id]) && $seekerDetails[$value->seeker_id] && isset($jobsDetails[$value->job_id]) && $jobsDetails[$value->job_id])
            {
                $result['name'] = $seekerDetails[$value->seeker_id]['name'];
                $result['email'] = $seekerDetails[$value->seeker_id]['email'];
                $result['mobile'] = $seekerDetails[$value->seeker_id]['mobile'];
                $result['job_type'] = $jobsDetails[$value->job_id]['job_type'];
                $result['apply_date'] = date('Y-m-d H:i:s', strtotime($value->created_at));
                $result['job_id'] = $value->job_id;
                array_push($applyJobsInfo, $result);
            }
        }

        $array['success'] = true;
        $array['message'] = "Apply Jobs Details";
        $array['data'] = $applyJobsInfo;
        return $array;
    }

    public function getSearchResumeCandidates(Request $request)
    {
        $data = $request->all();

        $seekerIds = array();
        if(isset($data['seeker_ids']) && $data['seeker_ids'])
        {
            $seekerIds = explode(',', $data['seeker_ids']);
            $candidateDetails = Seeker::select('*')->whereIn('id', $seekerIds)->get();
        }
        else
        {
            $array['success'] = true;
            $array['message'] = "All Candidates Details";
            $array['data'] = (object)array();
            return $array;
        }

        $seekerExperience = SeekerWorkExperience::whereIn('seeker_id', $seekerIds)
                                                    ->orderBy('id','DESC')
                                                    ->groupBy('seeker_id')
                                                    ->get()->keyBy('seeker_id');

        $seekerQualification = SeekerQualification::whereIn('seeker_id', $seekerIds)->orderBy('id','DESC')->groupBy('seeker_id')->get()->keyBy('seeker_id');

        $skillsDetails = Skill::get()->keyBy('id')->toArray();
        foreach ($candidateDetails as $key => $value)
        {
            $skillIds = $value['skill_ids'];
            $seekerId = $value['id'];
            // $value['all_skills'] = '';
            if($skillIds)
            {
                $skills = '';
                $skillIds = explode(',', $skillIds);
                foreach ($skillIds as $key => $skillval) 
                {
                    if(isset($skillsDetails[$skillval]))
                    {
                        if($skills)
                        {
                            $skills .= ','. $skillsDetails[$skillval]['skill'];
                        }
                        else
                        {
                            $skills = $skillsDetails[$skillval]['skill'];
                        }
                    }
                }
                // $skills = '';
                $value['all_skills'] = $skills;
            }
            
            if(isset($seekerExperience[$seekerId]))
            {
                $value['designation'] = $seekerExperience[$seekerId]['designation'];
                $value['experience']  = $seekerExperience[$seekerId]['experience'].' Year';
                $value['salary']      = $seekerExperience[$seekerId]['salary'];
            }
            else
            {
                $value['designation'] = '';
                $value['experience'] = '0 Year';
                $value['salary'] = '';
            }

            if(isset($seekerQualification[$seekerId]))
            {
                $value['highest_education'] = $seekerQualification[$seekerId]['highest_education'];
                $value['course'] = $seekerQualification[$seekerId]['course']; 
                $value['specialization'] = $seekerQualification[$seekerId]['specialization'];
            }
            else
            {
                $value['highest_education'] = '';
                $value['course'] = '';
                $value['specialization'] = '';
            }
        }

        $array['success'] = true;
        $array['message'] = "All Candidates Details";
        $array['data'] = $candidateDetails;
        return $array;
    }

    public function getSimilarResume(Request $request)
    {
        $data = $request->all();

        $seekerIds = array();
        if((isset($data['department_id']) && $data['department_id']) || (isset($data['role_id']) && $data['role_id']))
        {
            $query = Seeker::select('*');
            
            if(isset($data['department_id']) && $data['department_id'])
            {
                $query = $query->where('department_id', $data['department_id']);
            }

            if(isset($data['role_id']) && $data['role_id'])
            {
                $query = $query->where('role_id', $data['role_id']);
            }
            $candidateDetails = $query->get();
            
        }
        else
        {
            $array['success'] = true;
            $array['message'] = "All Candidates Details";
            $array['data'] = (object)array();
            return $array;
        }

        $seekerExperience = SeekerWorkExperience::whereIn('seeker_id', $seekerIds)
                                                    ->orderBy('id','DESC')
                                                    ->groupBy('seeker_id')
                                                    ->get()->keyBy('seeker_id');

        $seekerQualification = SeekerQualification::whereIn('seeker_id', $seekerIds)->orderBy('id','DESC')->groupBy('seeker_id')->get()->keyBy('seeker_id');

        $skillsDetails = Skill::get()->keyBy('id')->toArray();
        foreach ($candidateDetails as $key => $value)
        {
            $skillIds = $value['skill_ids'];
            $seekerId = $value['id'];
            // $value['all_skills'] = '';
            if($skillIds)
            {
                $skills = '';
                $skillIds = explode(',', $skillIds);
                foreach ($skillIds as $key => $skillval) 
                {
                    if(isset($skillsDetails[$skillval]))
                    {
                        if($skills)
                        {
                            $skills .= ','. $skillsDetails[$skillval]['skill'];
                        }
                        else
                        {
                            $skills = $skillsDetails[$skillval]['skill'];
                        }
                    }
                }
                // $skills = '';
                $value['all_skills'] = $skills;
            }
            
            if(isset($seekerExperience[$seekerId]))
            {
                $value['designation'] = $seekerExperience[$seekerId]['designation'];
                $value['experience']  = $seekerExperience[$seekerId]['experience'].' Year';
                $value['salary']      = $seekerExperience[$seekerId]['salary'];
            }
            else
            {
                $value['designation'] = '';
                $value['experience'] = '0 Year';
                $value['salary'] = '';
            }

            if(isset($seekerQualification[$seekerId]))
            {
                $value['highest_education'] = $seekerQualification[$seekerId]['highest_education'];
                $value['course'] = $seekerQualification[$seekerId]['course']; 
                $value['specialization'] = $seekerQualification[$seekerId]['specialization'];
            }
            else
            {
                $value['highest_education'] = '';
                $value['course'] = '';
                $value['specialization'] = '';
            }
        }

        $array['success'] = true;
        $array['message'] = "All Candidates Details";
        $array['data'] = $candidateDetails;
        return $array;
    }

    public function deleteUser(Request $request)
    {
        $data = $request->all();
        if(isset($data['user_id']) && $data['user_id'])
        {
            $userId = $data['user_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "User Id is required";
            $array['data'] = (object)array();
            return $array;
        }
        
         $userDetails = User::find($userId);

        if($userDetails)
        {
            $userDetails->delete();
            $array = array();
            $array['success'] = true;
            $array['message'] = "User Successfully Deleted.";
            $array['data'] = (object)array();
            return $array;
        }
        else
        {
            $array = array();
            $array['success'] = false;
            $array['message'] = "User not registered.";
            $array['data'] = (object)array();
            return $array;
        }
    }

    public function callLeterSend(Request $request)
    {
        $data = $request->all();
        if(isset($data['email']) && $data['email'])
        {
            $email = $data['email'];
        }
        else
        {
            $array = array();
            $array['success'] = false;
            $array['message'] = "Email id is required.";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['company_name']) && $data['company_name'])
        {
            $companyName = $data['company_name'];
        }
        else
        {
            $array = array();
            $array['success'] = false;
            $array['message'] = "Company Name is required.";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['address']) && $data['address'])
        {
            $address = $data['address'];
        }
        else
        {
            $array = array();
            $array['success'] = false;
            $array['message'] = "Address is required.";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['date']) && $data['date'])
        {
            $date = $data['date'];
        }
        else
        {
            $array = array();
            $array['success'] = false;
            $array['message'] = "Date is required.";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['time']) && $data['time'])
        {
            $time = date('H:i A', strtotime($data['time']));
        }
        else
        {
            $array = array();
            $array['success'] = false;
            $array['message'] = "Time is required.";
            $array['data'] = (object)array();
            return $array;
        }
        $seekerDetails = Seeker::where('email', $email)->first();
        $emailContent = array();
        $emailContent['date'] = $date;
        $emailContent['company_name'] = $companyName;
        $emailContent['address'] = $address;
        $emailContent['time'] = $time;
        $emailContent['day'] = date('D', strtotime($date));
        $emailContent['employeName'] = isset($seekerDetails) ? $seekerDetails->name : '';

        $message = '';
        $subject = 'Call Letter';
        Mail::send('emails.callleter_schedule', $emailContent, function($message) use ($email, $subject){
            $message->to($email, 'Get Me Jobs Interview')->subject
                ($subject);
            $message->from('info.tirumalajobs@gmail.com');
        });

        $array['success'] = true;
        $array['message'] = "Call letter successfully send.";
        $array['data'] = (object)array();
        return $array;
    }

    public function loginOtpVerification(Request $request)
    {
        $data = $request->all();
        if(isset($data['id']) && $data['id'])
        {
            $userId = $data['id'];
        }
        else
        {
            $array = array();
            $array['success'] = false;
            $array['message'] = "User id is required.";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['otp']) && $data['otp'])
        {
            $otp = $data['otp'];
        }
        else
        {
            $array = array();
            $array['success'] = false;
            $array['message'] = "OTP is required.";
            $array['data'] = (object)array();
            return $array;
        }

        if(isset($data['user_type']) && $data['user_type'])
        {
            $userType = $data['user_type'];
        }
        else
        {
            $array = array();
            $array['success'] = false;
            $array['message'] = "User Type is required.";
            $array['data'] = (object)array();
            return $array;
        }
        $newTime = strtotime('-30 minutes');
        $time = date('Y-m-d H:i:s', $newTime);
        $otpVerify = OtpVerification::where('user_id', $userId)->where('user_type', $userType)->where('otp_screen','login')->where('created_at','>=', $time)->orderBy('id','DESC')->first();
        if($otpVerify && $otpVerify->otp == $otp)
        {
            $array = array();
            $array['success'] = true;
            $array['message'] = "Login Successfully.";
            $array['data'] = (object)array();
            return $array;
        }
        else
        {
            $array = array();
            $array['success'] = false;
            $array['message'] = "Envalid OTP";
            $array['data'] = (object)array();
            return $array;
        }
    }

    public function updateCandidateDetails(Request $request)
    {
        $data = $request->all();
        if(isset($data['seeker_id']) && $data['seeker_id'])
        {
            $seekerId = $data['seeker_id'];
        }
        else
        {
            $array = array();
            $array['success'] = false;
            $array['message'] = "Seeker id is required.";
            $array['data'] = (object)array();
            return $array;
        }

        $seekerDetails = Seeker::find($seekerId);
        if($seekerDetails)
        {
            if(isset($data['view_profile']) && $data['view_profile'])
            {
                $seekerDetails->view_profile = 1;
            }

            $seekerDetails->save();
            $array = array();
            $array['success'] = true;
            $array['message'] = "Profile Updated";
            $array['data'] = (object)array();
            return $array;
        }
        else
        {
            $array = array();
            $array['success'] = false;
            $array['message'] = "Seeker is not registered.";
            $array['data'] = (object)array();
            return $array;
        }
    }

    public function editClientTrackerInfo(Request $request)
    {
        $data = $request->all();
        if(isset($data['client_id']) && $data['client_id'])
        {
            $clientId = $data['client_id'];
        }
        else
        {
            $array = array();
            $array['success'] = false;
            $array['message'] = "Client id is required.";
            $array['data'] = (object)array();
            return $array;
        }

        $clientDetails = ClientTracker::find($clientId);
        if($clientDetails)
        {
            if(isset($data['domain_name']) && $data['domain_name'])
            {
                $clientDetails->domain_name = $data['domain_name'];
            }

            if(isset($data['client_name']) && $data['client_name'])
            {
                $clientDetails->client_name = $data['client_name'];
            }

            if(isset($data['website']) && $data['website'])
            {
                $clientDetails->website = $data['website'];
            }

            $clientDetails->updated_at = date('Y-m-d H:i:s');

            $clientDetails->save();
            $array = array();
            $array['success'] = true;
            $array['message'] = "Client Details Updated";
            $array['data'] = (object)array();
            return $array;
        }
        else
        {
            $array = array();
            $array['success'] = false;
            $array['message'] = "Client is not registered.";
            $array['data'] = (object)array();
            return $array;
        }
    }
}
