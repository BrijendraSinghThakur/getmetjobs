<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Skill;

class SkillController extends Controller
{
    public function addSkill(Request $request)
    {
        
        $data = $request->all();
        $skill = NULL;
        $experience = NULL;
        
        if (isset($data['skill']) && $data['skill'] ) 
        {
           $skill = $data['skill'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Skill is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        if (isset($data['industry_id']) && $data['industry_id'] ) 
        {
           $industryId = $data['industry_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Industry is Required.";
            $array['data'] = (object)array();
            return $array;
        }
       
        $skills = new Skill();
        $skills->skill = $skill;
        $skills->industry_id = $industryId;
        $skills->save();

        $array['success'] = true;
        $array['message'] = "Successfully save!!!";
        $array['data'] = $skills;
        return $array;
    }

    public function updateSkill(Request $request)
    {
    	
    	$data = $request->all();
    	$skill = NULL;
    	$experience = NULL;
    	
		if (isset($data['skill_name']) && $data['skill_name'] ) 
        {
    	   $skill = $data['skill_name'];
	    }
	    else
        {
            $array['success'] = false;
            $array['message'] = "Skill is Required.";
            $array['data'] = (object)array();
            return $array;
        }

        if (isset($data['skill_id']) && $data['skill_id'] ) 
        {
           $skillId = $data['skill_id'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Skill id is Required.";
            $array['data'] = (object)array();
            return $array;
        }
       
        $skills = Skill::find($skillId);
        $skills->skill = $skill;
        $skills->save();

        $array['success'] = true;
        $array['message'] = "Successfully update!!!";
        $array['data'] = $skills;
        return $array;
    }

    public function getSkill(Request $request){
    	$data = $request->all();
    	if (isset($data['seekerId']) && $data['seekerId'] ) {
    	   $seekerId = $data['seekerId'];
	    }else{
	            $array['success'] = false;
	            $array['message'] = "Seeker Id is Required.";
	            $array['data'] = (object)array();
	            return $array;
        	}

        $skill = Skill::select('skills', 'experience','seeker_id')->where('seeker_id', $data['seekerId'])->get();
        if (!$skill) {
        	$array['success'] = false;
	        $array['message'] = "Get Skills !!!";
	        $array['data']    = (object)array();
	        return $array;
	        }else{
		        $array['success'] = true;
		        $array['message'] = "Get Skills !!!";
		        $array['data']    = $skill;
		        return $array;
	    }
    }

    // public function updateSkill(Request $request){
    // 	$data = $request->all();
    // 	$seeker = Skill::select('*')->where('id', $data['id'])->update($data);
    // 	if (empty($seeker)) {
    // 		$array['success'] = true;
	   //      $array['message'] = "Resourse not found!!!";
	   //      $array['data']    = (object)array();
	   //      return $array;
    // 	}else{
	   //  	$array['success'] = true;
	   //      $array['message'] = "Update data Successfully!!!";
	   //      $array['data']    = (object)array();
	   //      return $array;
    // 	}
    // }

    public function deleteSkill(Request $request){
    	$data = $request->all();
    	$seeker = Skill::where('id', $data['id'])->delete();
    	
    	if (empty($seeker)) {
    		$array['success'] = true;
	        $array['message'] = "Resourse not found!!!";
	        $array['data']    = (object)array();
	        return $array;
    	}else{
			$array['success'] = true;
	        $array['message'] = "Delete data Successfully!!!";
	        $array['data']    = (object)array();
	        return $array;
	    }
    }
}
