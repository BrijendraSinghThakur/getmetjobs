<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Cities;
use App\Designations;
use App\MaritalCategory;
use App\CasteCategory;
use App\Skill;
use App\EducationCategory;
use App\EducationSubCategory;
use App\JobType;
use App\Seeker;
use App\SeekerWorkExperience;
use App\Jobs;
use App\ClientTracker;
use App\SchoolMedium;
use App\University;
use App\Department;
use App\Role;
use App\Industry;
use DateTime;
use DatePeriod;
use DateInterval;

use FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

class CommonController extends Controller
{
    # Function : This function is used to fetch the list of cities
    # Request  : None
    # Response : success true/false json response
    # Author   : Brijendra
    public function getCitiesList(Request $request)
    {
        $data = $request->all();
  
        $cities = Cities::select('city_id', 'city_name')->orderBy('city_name','ASC')->get();
        $responseArray = array("popular_cities" => "", "all_cities" => $cities);

        $array['success'] = true;
        $array['message'] = "Record Found";
        $array['data']    = $responseArray;
        return $array;
    }

    # Function : This function is used to search city
    # Request  : city_name
    # Response : success true/false json response
    # Author   : Brijendra
    public function searchCity(Request $request)
    {
        $data = $request->all();

        if (isset($data['city_name']) && $data['city_name'] ) 
        {
            $cityName = $data['city_name'];
        	$cities   = Cities::select('city_id', 'city_name')->where('city_name','LIKE', $cityName.'%')->get();

        	$array['success'] = true;
            $array['message'] = "Record found";
            $array['data']    = $cities;
            return $array;
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "City name is Required.";
            $array['data'] = (object)array();
            return $array;
        }
  
    }

    # Function : This function is used to search city
    # Request  : city_name
    # Response : success true/false json response
    # Author   : Brijendra
    public function getDesignationList(Request $request)
    {
        $data = $request->all();

        $designations = Designations::orderBy('designation','ASC')->get();

        $array['success'] = true;
        $array['message'] = "Record Found";
        $array['data']    = $designations;
        return $array;
  
    }


    # Function : This function used to fetch marital categories
    # Request  : None
    # Response : success true/false json response
    # Author   : Brijendra
    public function maritalCategories(Request $request)
    {
        $data = $request->all();
       
        $maritalStatus = MaritalCategory::all();

        $array['success'] = true;
        $array['message'] = "Record Found";
        $array['data']    = $maritalStatus;
        return $array;

        
    }

    # Function : This function used to fetch Caste categories
    # Request  : None
    # Response : success true/false json response
    # Author   : Brijendra
    public function casteCategories(Request $request)
    {
        $data = $request->all();
       
        $casteCategories = CasteCategory::all();

        $array['success'] = true;
        $array['message'] = "Record Found";
        $array['data']    = $casteCategories;
        return $array;
        
    }


    # Function : This function is used to fetch the list of preferred industries
    # Request  : None
    # Response : success true/false json response
    # Author   : Brijendra
    public function preferredIndustryList(Request $request)
    {

        $industries = Industry::all();
            
        $array['success'] = true;
        $array['message'] = "Record found";
        $array['data']    =  $industries;
        return $array;

    }


    # Function : This function used to fetch skills
    # Request  : None
    # Response : success true/false json response
    # Author   : Brijendra
    public function getSkills(Request $request)
    {
        $data = $request->all();
        if(isset($data['industry_ids']) && $data['industry_ids'])
        {
            $industryIds = explode(',', $data['industry_ids']);
            $skills = Skill::whereIn('industry_id', $industryIds)->get();
        }
        else
        {
            $skills = Skill::all();
            $industryIdsArray = array();
            foreach ($skills as $key => $value) 
            {
                array_push($industryIdsArray, $value->industry_id);
            }

            $industry = Industry::whereIn('id', $industryIdsArray)->get()->keyBy('id');

            foreach ($skills as $key => $value) 
            {
                if (isset($industry[$value->industry_id])) 
                {
                    $value->industry_name = $industry[$value->industry_id]['industry_name'];
                }
                else
                {
                    $value->industry_name = 'NA';
                }
            }
        }

        $array['success'] = true;
        $array['message'] = "Record Found";
        $array['data']    = $skills;
        return $array;
        
    }

    # Function : This function is used to search skill
    # Request  : skill name
    # Response : success true/false json response
    # Author   : Brijendra
    public function searchSkill(Request $request)
    {
        $data = $request->all();

        if (isset($data['skill_name']) && $data['skill_name'] ) 
        {
            $skillName = $data['skill_name'];
            $skill   = Skill::where('skill','LIKE', $skillName.'%')->get();

            $array['success'] = true;
            $array['message'] = "Record found";
            $array['data']    = $skill;
            return $array;
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Skill name is Required.";
            $array['data'] = (object)array();
            return $array;
        }
  
    }

    # Function : This function is used to fetch list of years
    # Request  : Nome
    # Response : success true/false json response
    # Author   : Brijendra
    public function yearsList(Request $request)
    {
        $data = $request->all();
        $year = date('Y');
        $currentYear = (int)$year + 4;

        $yearsArray = array();
        for ($i = $currentYear; $i > 1947; $i--) 
        { 
            array_push($yearsArray, $i);
        }

        $array['success'] = true;
        $array['message'] = "Record found";
        $array['data']    = $yearsArray;
        return $array;
  
    }


    # Function : This function is used to fetch list of education categories
    # Request  : None
    # Response : success true/false json response
    # Author   : Brijendra
    public function educationCategories(Request $request)
    {
        $educationCategories   = EducationCategory::all();
        if ($educationCategories != '') 
        {
            $array['success'] = true;
            $array['message'] = "Record found";
            $array['data']    = $educationCategories;
            return $array;
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "No Record Found";
            $array['data']    = (object)array();
            return $array;
        }
  
    }

    # Function : This function is used to fetch list of education sub categories based on main category
    # Request  : None
    # Response : success true/false json response
    # Author   : Brijendra
    public function educationSubCategories(Request $request)
    {
        $data = $request->all();

        if (isset($data['education_category_id']) && $data['education_category_id'] ) 
        {
            $educationCategoryId = $data['education_category_id'];
            $educationSubCategory   = EducationSubCategory::where('education_category', $educationCategoryId)->orderBy('sub_education','ASC')->get();

            $array['success'] = true;
            $array['message'] = "Record found";
            $array['data']    = $educationSubCategory;
            return $array;
        }
        else
        {
            $educationSubCategory   = EducationSubCategory::orderBy('sub_education','ASC')->get();
            $eduCategoryIdsArray = array();
            foreach ($educationSubCategory as $key => $value) 
            {
                array_push($eduCategoryIdsArray, $value->education_category);
            }

            $eduCategory = EducationCategory::whereIn('id', $eduCategoryIdsArray)->get()->keyBy('id');

            foreach ($educationSubCategory as $key => $value) 
            {
                if (isset($eduCategory[$value->education_category])) 
                {
                    $value->education = $eduCategory[$value->education_category]['education'];
                }
                else
                {
                    $value->education = 'NA';
                }
            }

            $array['success'] = true;
            $array['message'] = "Record found";
            $array['data']    = $educationSubCategory;
            return $array;
        }
  
    }

    # Function : This function is used to fetch list of education sub categories based on main category
    # Request  : None
    # Response : success true/false json response
    # Author   : Brijendra
    public function jobeTypeList(Request $request)
    {
        $data = $request->all();

        $jobTypes   = JobType::all();
        if ($jobTypes != '') 
        {
            $array['success'] = true;
            $array['message'] = "Record found";
            $array['data']    = $jobTypes;
            return $array;
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "No Record Found";
            $array['data']    = (object)array();
            return $array;
        }
  
    }


    # Function : This function is used to make salary comma formate
    # Request  : amount
    # Response : success true/false json response
    # Author   : Brijendra
    public function moneyNumberFormat($amount)
    {
        $len = strlen($amount);
        if($len < 6)
        {
            return number_format($amount);
        }   
        else
        {
            $lastNum = substr($amount,-3);
            $countComm = $len - 3;
            $remainintChar = substr($amount, 0, $countComm);
            
            $len = intval(strlen($remainintChar)/2);
            if(strlen($remainintChar)%2 != 0)
            {
                $len += 1;
            }
            $newNumber = '';
            $j = 2;
            for ($i=0; $i < $len ; $i++) 
            { 
                // $newStr = $remainintChar % 100;
                $newStr = substr($remainintChar, -2);
                $remainintChar = intval($remainintChar / 100);
                $newNumber = $newStr.','.$newNumber;
            }
            
            return $newNumber.''.$lastNum;
        }
    }

    # Function : This function is used to fetch client list
    # Request  : None
    # Response : success true/false json response
    # Author   : Brijendra
    public function clientList(Request $request)
    {

        $clientlist   = ClientTracker::select('id','domain_name', 'client_name', 'website')->where('is_deleted',0)->get();

        $array['success'] = true;
        $array['message'] = "Record found";
        $array['data']    = $clientlist;
        return $array;   
  
    }

    # Function : This function is used to fetch university list
    # Request  : None
    # Response : success true/false json response
    # Author   : Brijendra
    public function universityList(Request $request)
    {

        $universityList   = University::all();
        $array['success'] = true;
        $array['message'] = "Record found";
        $array['data']    = $universityList;
        return $array;   
    }

    # Function : This function is used to marks list
    # Request  : None
    # Response : success true/false json response
    # Author   : Brijendra
    public function marksList(Request $request)
    {

        $marksArray = array(
                            "less then 40%", 
                            "40-44.9%", 
                            "45-49.9%", 
                            "50-54.9%",
                            "55-59.9%", 
                            "60-64.9%",
                            "65-69.9%", 
                            "70-74.9%",
                            "75-79.9%", 
                            "80-84.9%",
                            "85-89.9%", 
                            "90-94.9%",
                            "95-99.9%", 
                            "100%", 
                        );


        $array['success'] = true;
        $array['message'] = "Marks list";
        $array['data']    = $marksArray;
        return $array;
    }

    # Function : This function is used to school medium list
    # Request  : None
    # Response : success true/false json response
    # Author   : Brijendra
    public function mediumList(Request $request)
    {

        $mediumList   = SchoolMedium::select('id', 'medium')->get();

        $array['success'] = true;
        $array['message'] = "Record found";
        $array['data']    = $mediumList;
        return $array;   
  
    }

    # Function : This function is used to fetch demartment list
    # Request  : None
    # Response : success true/false json response
    # Author   : Brijendra
    public function departmentList(Request $request)
    {
        $departmentList   = Department::all();

        $array['success'] = true;
        $array['message'] = "Record found";
        $array['data']    = $departmentList;
        return $array;   
  
    }

    # Function : This function is used to fetch role list
    # Request  : None
    # Response : success true/false json response
    # Author   : Brijendra
    public function roleList(Request $request)
    {

        $data = $request->all();
        if(isset($data['department_id']) && $data['department_id'])
        {
            $role = Role::where('department_id', $data['department_id'])->get();

            if ($role == '') 
            {
                $array['success'] = false;
                $array['message'] = "No Record Found";
                $array['data']    = (object)array();
                return $array;
            }
            else
            {
                $array['success'] = true;
                $array['message'] = "Record Found";
                $array['data']    = $role;
                return $array;
            }
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Department Id Required";
            $array['data']    = (object)array();
            return $array;
        } 
  
    }

    public function keywordSelection()
    {

        $skills = Skill::all();
        $designations = Designations::all();
        $role = Role::all();

        $result       = array();
        $keywordArray = array();

        foreach ($skills as $key => $value) 
        {
            $result['keyword'] = $value->skill;
            array_push($keywordArray, $result);
        }

        foreach ($designations as $key => $value) 
        {
            $result['keyword'] = $value->designation;
            array_push($keywordArray, $result);
        }


        foreach ($role as $key => $value) 
        {
            $result['keyword'] = $value->role;
            array_push($keywordArray, $result);
        }


        $array['success'] = true;
        $array['message'] = "Record Found";
        $array['data']    = $keywordArray;
        return $array;
    }


    # Function : This function is used to send notification
    # Response : success true/false json response
    # Author   : Brijendra
    public function sendNotification(Request $request)
    {
        $data = $request->all();
        $candidateType = '';

        if(isset($data['candidate_type']) && $data['candidate_type'])
        {
            $candidateType = $data['candidate_type'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Candidate type is Required.";
            $array['data']    = (object)array();
            return $array;
        }

        if($candidateType == 'Experience')
        {
            if (isset($data['designation']) && $data['designation'] ) 
            {
                $designation = $data['designation'];
            }
            else
            {
                $array['success'] = false;
                $array['message'] = "Designation is Required.";
                $array['data']    = (object)array();
                return $array;
            }
        }

        if (isset($data['title']) && $data['title'] ) 
        {
            $title = $data['title'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Title of notification is Required.";
            $array['data']    = (object)array();
            return $array;
        }

        if (isset($data['job_description']) && $data['job_description'] ) 
        {
            $jobDescription = $data['job_description'];
        }
        else
        {
            $array['success'] = false;
            $array['message'] = "Description is Required.";
            $array['data']    = (object)array();
            return $array;
        }

        if($candidateType == 'Experience')
        {
            $seekerDetails = SeekerWorkExperience::where('designation', $designation)->get();
            if (count($seekerDetails) == 0) 
            {
                $array['success'] = false;
                $array['message'] = "No seeker available for this designation";
                $array['data']    = (object)array();
                return $array;
            }

            $seekerIdsArray = array();
            foreach ($seekerDetails as $key => $value) {
                array_push($seekerIdsArray, $value->seeker_id);
            }

            $seeker = Seeker::whereIn('id', $seekerIdsArray)->get();
        }
        else
        {
            $seeker = Seeker::where('profile', 'fresher')->where('token','!=','')->get();
        }
        
        $seekerTokensArray = array();
        foreach ($seeker as $key => $value) {
            if ($value->token) 
            {
                array_push($seekerTokensArray, $value->token);
            }
        }

        $bodyText = 'Trip Notification';
        $message = array(
                        'title' => $title,
                        'message' => $jobDescription,
                        'type' => 'notification'

                        );

        $optionBuiler = new OptionsBuilder();
        $optionBuiler->setTimeToLive(60*20);
        
        $notificationBuilder = new PayloadNotificationBuilder();
        $notificationBuilder->setBody($bodyText. "=" .$message['type'])->setSound('default');

        
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($message);
           
        $option       = $optionBuiler->build();
        $data         = $dataBuilder->build();
        $notification = $notificationBuilder->build();

        $downstreamResponse = FCM::sendTo($seekerTokensArray, $option, NULL, $data);
        $done = $downstreamResponse->numberSuccess();
        $fail = $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();
        $downstreamResponse->tokensToDelete(); 
        $downstreamResponse->tokensToModify(); 
        $downstreamResponse->tokensToRetry();
        if (!empty($done)) 
        {
            $array['success'] = true;
            $array['message'] = "Notification send successfully";
            $array['data']    = (object)array();
            return $array;
        }
        else if (!empty($fail))
        {

            $array['success'] = false;
            $array['message'] = "Notification sending failed !!";
            $array['data']    = (object)array();
            return $array;
        }

    }

    public function testtt()
    {
        $period = new DatePeriod(
             new DateTime('2010-10-01'),
             new DateInterval('P1D'),
             new DateTime('2010-10-05')
        );
        $startDate = '2010-10-01';
        $endDate = '2010-11-10';
        $format = "Y-m-d";
        $begin = new DateTime($startDate);
        $end = new DateTime($endDate);

        $interval = new DateInterval('P1D'); // 1 Day
        $dateRange = new DatePeriod($begin, $interval, $end);

        $range = [];
        foreach ($dateRange as $date) {
            $range[] = $date->format($format);
        }
        print_r($range);
        // $num = 3.7; // or $num = 3.75;
        // $value = explode('.', $num);
        // if (isset($value[1])) 
        // {
        //     return  $value[1];
        // }
        // else
        // {

        //     return $value[0];
        // }
//         echo date('Y-m-d', strtotime('first day of january month'));
//         echo "<br/>";

// echo date('Y-m-d', strtotime('last day of last month'));
    }


    
}
