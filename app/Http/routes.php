<?php
header('Access-Control-Allow-Origin:  *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
	if (Auth::check()) 
	{
		return view('index');
	}
	else
	{
		return view('login');
	}
});
// Route::get('/', 'HomeController@login');
// Route::get('/login', 'HomeController@login');
// Route::get('/signup', 'HomeController@signup');
// Route::get('/dashboard', 'HomeController@dashboard');

Route::group(['prefix' => 'api/v1'], function() { 

	Route::any('userRegistration', 'UserController@userRegistration');
	Route::any('userLogin', 'UserController@userLogin');
	Route::any('user', 'UserController@user');
	Route::get('getActiveUser', 'UserController@getActiveUser');
	Route::get('getBlockedUser', 'UserController@getBlockedUser');
	Route::any('updateUser', 'UserController@updateUser');
	
	

	Route::any('registerSeeker', 'SeekerController@registerSeeker');
	Route::any('verifyOtpAndRegister', 'SeekerController@verifyOtpAndRegister');
	Route::any('loginSeeker', 'SeekerController@loginSeeker');
	Route::any('socialLogin', 'SeekerController@socialLogin');
	Route::any('logoutSeeker', 'SeekerController@logoutSeeker');
	Route::any('getSeekerProfile', 'SeekerController@getSeekerProfile');
	Route::any('verifyOtp', 'SeekerController@verifyOtp');
	Route::post('updateProfileDetails', 'SeekerController@updateProfileDetails');
	Route::any('home', 'SeekerController@home');
	Route::any('getRecommendedJobs', 'SeekerController@getRecommendedJobs');
	Route::any('getAppledJobs', 'SeekerController@getAppledJobs');
	Route::any('getSavedJobs', 'SeekerController@getSavedJobs');
	Route::any('addProfileImage', 'SeekerController@addProfileImage');
	Route::any('applyForJob', 'SeekerController@applyForJob');
	Route::any('saveJob', 'SeekerController@saveJob');
	Route::any('updateNotificationSetting', 'SeekerController@updateNotificationSetting');
	Route::any('searchJob', 'SeekerController@searchJob');
	Route::any('notificationList', 'SeekerController@notificationList');
	Route::any('setProfileVisibility', 'SeekerController@setProfileVisibility');
	Route::any('updatePersonalDetails', 'SeekerController@updatePersonalDetails');
	Route::any('verifyOtpToUpdatePersonalDetail', 'SeekerController@verifyOtpToUpdatePersonalDetail');
	Route::any('deleteNumber', 'SeekerController@deleteNumber');
	Route::any('changePassword', 'SeekerController@changePassword');
	Route::any('addSeekerSkills', 'SeekerController@addSeekerSkills');
	Route::any('insertData', 'SeekerController@insertData');
	Route::any('generateResume', 'SeekerController@generateResume');
	Route::any('registerDeviceInDownload', 'SeekerController@registerDeviceInDownload');
	Route::any('removeProfileImage', 'SeekerController@removeProfileImage');


	Route::any('preferredIndustryList', 'CommonController@preferredIndustryList');
	Route::any('getCitiesList', 'CommonController@getCitiesList');
	Route::any('searchCity', 'CommonController@searchCity');
	Route::any('getDesignationList', 'CommonController@getDesignationList');
	Route::any('sendNotification', 'CommonController@sendNotification');
	Route::any('maritalCategories', 'CommonController@maritalCategories');
	Route::any('casteCategories', 'CommonController@casteCategories');
	Route::any('getSkills', 'CommonController@getSkills');
	Route::any('searchSkill', 'CommonController@searchSkill');
	Route::any('yearsList', 'CommonController@yearsList');
	Route::any('educationCategories', 'CommonController@educationCategories');
	Route::any('educationSubCategories', 'CommonController@educationSubCategories');
	Route::any('jobeTypeList', 'CommonController@jobeTypeList');
	Route::any('clientList', 'CommonController@clientList');
	Route::any('universityList', 'CommonController@universityList');
	Route::any('marksList', 'CommonController@marksList');
	Route::any('mediumList', 'CommonController@mediumList');
	Route::any('departmentList', 'CommonController@departmentList');
	Route::any('roleList', 'CommonController@roleList');
	Route::any('testtt', 'CommonController@testtt');
	Route::any('keywordSelection', 'CommonController@keywordSelection');

	Route::any('addSkill', 'SkillController@addSkill');
	Route::any('updateSkill', 'SkillController@updateSkill');
	Route::any('deleteSkill', 'SkillController@deleteSkill');

	Route::any('location', 'SeekerController@location');
	Route::any('year', 'SeekerController@year');
	Route::any('month', 'SeekerController@month');

	Route::any('seekerProfile', 'SeekerController@seekerProfile');
	Route::put('updateSeekerProfile', 'SeekerController@updateSeekerProfile');
	Route::any('getSeekerProfile', 'SeekerController@getSeekerProfile');
	
	// Route::any('sendMessage', 'SeekerController@sendMessage');
	Route::any('otpVerify', 'SeekerController@otpVerify');
	Route::any('forgetPassword', 'SeekerController@forgetPassword');
	Route::any('resetPassword', 'SeekerController@resetPassword');
	Route::any('autoDelete', 'SeekerController@autoDelete');

	Route::get('getWorkExperience', 'SeekerController@getWorkExperience');
	Route::get('getSalary', 'SeekerController@getSalary');
	Route::post('mobileNumberVerification', 'SeekerController@mobileNumberVerification');
	Route::any('sendVerificationOTP','SeekerController@sendVerificationOTP');
	Route::any('getSpecialization','SeekerController@getSpecialization');

	Route::get('termsAndCondition','SeekerController@termsAndCondition');
	Route::get('privacyPolicy','SeekerController@privacyPolicy');

	Route::post('similarJobs','SeekerController@similarJobs');
	Route::post('preferencesJobs','SeekerController@preferencesJobs');
	Route::delete('deleteWorkExperience','SeekerController@deleteWorkExperience');
	Route::delete('deleteQualification','SeekerController@deleteQualification');
	Route::put('editWorkExperience','SeekerController@editWorkExperience');
	Route::any('editQualification','SeekerController@editQualification');
	Route::any('getNotificationSetting','SeekerController@getNotificationSetting');
	Route::any('getProfileVisibilitySetting','SeekerController@getProfileVisibilitySetting');

	// Add Category

	Route::get('getDashboardDetails', 'JobsController@getDashboardDetails');
	Route::post('createJob','JobsController@createJob');
	Route::any('getJobsDetails','JobsController@getJobsDetails');
	Route::get('getSpecificJobDetails','JobsController@getSpecificJobDetails');
	Route::get('getBlockedCandidates','JobsController@getBlockedCandidates');
	Route::get('getAllCandidates','JobsController@getAllCandidates');
	Route::any('closeJob','JobsController@closeJob');
	Route::any('deleteJob','JobsController@deleteJob');
	Route::any('editJobDetails', 'JobsController@editJobDetails');
	Route::any('updateJob', 'JobsController@updateJob');
	Route::any('getJobRecommendedCandidates', 'JobsController@getJobRecommendedCandidates');
	Route::any('jobAppliedCandidates', 'JobsController@jobAppliedCandidates');
	Route::any('getDoctrateOrPHD', 'JobsController@getDoctrateOrPHD');
	Route::any('deleteClient', 'JobsController@deleteClient');
	Route::any('getRecommendedJobsList', 'JobsController@getRecommendedJobsList');
	
	Route::post('addCategory','JobsController@addCategory');
	Route::any('addSubCategory','JobsController@addSubCategory');
	Route::any('addDesignation', 'JobsController@addDesignation');
	Route::any('addUniversity', 'JobsController@addUniversity');
	Route::any('addDepartment', 'JobsController@addDepartment');

	Route::any('updateCategory', 'JobsController@updateCategory');
	Route::any('updateDepartment', 'JobsController@updateDepartment');
	Route::any('updateDesignation', 'JobsController@updateDesignation');
	Route::any('updateIndustry', 'JobsController@updateIndustry');
	Route::any('updateSubCategory', 'JobsController@updateSubCategory');
	Route::any('updateUniversity', 'JobsController@updateUniversity');


	// client tracker
	Route::get('getClientJobDetails','JobsController@getClientJobDetails');
	Route::get('getClientList','JobsController@getClientList');
	Route::post('addClientTracker','JobsController@addClientTracker');
	Route::put('updateClientTracker','JobsController@updateClientTracker');

	Route::post('applyJobInterview','JobsController@applyJobInterview');
	Route::put('reScheduleJobInterview','JobsController@reScheduleJobInterview');
	Route::put('rejectedJobInterview','JobsController@rejectedJobInterview');
	Route::put('completedJobInterview','JobsController@completedJobInterview');
	Route::get('candidateApplyJobs','JobsController@candidateApplyJobs');
	Route::post('addIndustry', 'JobsController@addIndustry');
	Route::get('getUnderGraduateCategory','JobsController@getUnderGraduateCategory');
	Route::get('getPostGraduateCategory','JobsController@getPostGraduateCategory');
	Route::get('getMultipleSpecialization','JobsController@getMultipleSpecialization');
	Route::post('searchResumeDetails','JobsController@searchResumeDetails');
	Route::get('getAllotmentUser','JobsController@getAllotmentUser');
	Route::put('updateAllotmentUser','JobsController@updateAllotmentUser');
	

	Route::any('appDownloadReport','ReportController@appDownloadReport');
	Route::any('seekerRegistrationReport','ReportController@seekerRegistrationReport');
	Route::any('candidateLoginReport','ReportController@candidateLoginReport');
	Route::any('jobAppliedCandidateCountReport','ReportController@jobAppliedCandidateCountReport');

	Route::delete('deleteCategory','JobsController@deleteCategory');
	Route::delete('deleteEducationSubCategories','JobsController@deleteEducationSubCategories');
	Route::delete('deleteIndustry','JobsController@deleteIndustry');
	Route::delete('deleteSkills','JobsController@deleteSkills');
	Route::delete('deleteDesignation','JobsController@deleteDesignation');
	Route::delete('deleteDepartment','JobsController@deleteDepartment');
	Route::delete('deleteUniversity','JobsController@deleteUniversity');
	Route::delete('deleteUser','JobsController@deleteUser');

	Route::get('userRegistrationReport','JobsController@userRegistrationReport');
	Route::get('getScheduleInterview','JobsController@getScheduleInterview');
	Route::get('getApplyJobs','JobsController@getApplyJobs');
	
	Route::get('getSearchResumeCandidates','JobsController@getSearchResumeCandidates');
	Route::get('getSimilarResume','JobsController@getSimilarResume');
	Route::post('sendCallLeter','JobsController@callLeterSend');

	Route::get('loginOtpVerification','JobsController@loginOtpVerification');
	Route::put('updateCandidateDetails','JobsController@updateCandidateDetails');
	Route::put('editClientTrackerInfo','JobsController@editClientTrackerInfo');
});
Route::any('{catchall}', function() {
  	if(Auth::check())
	{	
		return view('index');	
	}
	else
	{
		return view('login');	
	}
})->where('catchall', '.*');
// Route::auth();

// Route::get('/home', 'HomeController@index');
