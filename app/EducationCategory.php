<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EducationCategory extends Model
{
    protected $table = 'education_category';
    public $timestamps = false;
}
