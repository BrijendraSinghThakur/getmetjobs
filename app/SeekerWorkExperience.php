<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class SeekerWorkExperience extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'seeker_work_experience';
    public $timestamps = false;
}