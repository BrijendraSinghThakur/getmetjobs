<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeekerImage extends Model
{
    protected $fillable = [
        'seeker_id', 'image'
    ];
}
