<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class TermsCondition extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'terms_condition';
    public $timestamps = false;
}