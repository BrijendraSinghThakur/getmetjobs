<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class UserAllotment extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_allotment';
    public $timestamps = false;
}