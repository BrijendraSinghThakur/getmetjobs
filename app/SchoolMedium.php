<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolMedium extends Model
{
    protected $table = 'school_medium';
}
