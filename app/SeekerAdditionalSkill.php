<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeekerAdditionalSkill extends Model
{

    protected $table = 'seeker_additional_skill';
    public $timestamps = false;
}
