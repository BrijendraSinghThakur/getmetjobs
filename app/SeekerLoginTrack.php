<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeekerLoginTrack extends Model
{
    protected $table = 'seeker_login_track';
    public $timestamps = false;
}
