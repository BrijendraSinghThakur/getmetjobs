<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class JobInterview extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'job_interview';
    public $timestamps = false;
}