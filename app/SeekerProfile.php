<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeekerProfile extends Model
{
    protected $fillable = [
        'seeker_id','name', 'phone', 'dob', 'gender', 'address', 'city', 'pincode', 'marital_status', 'current_location', 'total_experience', 'preferred_location', 'annual_salary', 'industry', 'highest_degree', 'resume'
    ];
}
