<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class SeekerQualification extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'seeker_qualification';
    public $timestamps = false;
}