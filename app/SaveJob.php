<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaveJob extends Model
{
    protected $table = 'save_jobs';
}
