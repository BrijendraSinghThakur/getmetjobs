<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Salary extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'salary';
    public $timestamps = false;
}