<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ClientTracker extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'client_tracker';
    public $timestamps = false;
}