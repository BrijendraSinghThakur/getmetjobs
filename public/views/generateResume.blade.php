<!DOCTYPE html>
<html lang="en">

<head>
    <style>
@import url('https://fonts.googleapis.com/css?family=sans-serif');
    body {
        font-size: 17px;
        font-family: 'sans-serif', sans-serif;
        text-align: justify;
        line-height: 28px;
        color: black;
        background-color: #f3f3f3;
    }

}
    </style>
</head>

<body>
    <div>
        <h3 style="margin: 0px;"><b>{{$user->name}}</b></h3>
        <h4 style="margin: 0px;"><b style="font-size: 15px;">{{$user->mobile}}</b></h4>
        <h4 style="margin: 0px;"><b style="font-size: 15px;">{{$user->email}}</b></h4>
    </div>
    <hr style="border:1px solid lightgray">
        <p>
            <b>Objective</b>
        </p>
        <p style="font-size: 15px;">
            I Confirms that I contacted GetMeJobs from the source of digital information available on the Digital Network.
        </p>

        </br>

        <p>
            <b>Qualifications</b>
        </p>

        <ul>
            @foreach($user['qualification'] as $qualification)
            <li>
                <p style="margin: 0px;font-size: 15px;">{{$qualification->qualification_text}}</p>
            </li>
            @endforeach
        </ul> 

        <p>
            <b>Skills</b>
        </p>
        <p style="margin-left: 39px;font-size: 15px;">{{$user->all_skills}}</p>

        <!-- <ul>
            <li>
                <p style="margin: 0px;font-size: 15px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </li>
            <li>
                <p style="margin: 0px;font-size: 15px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </li>
            <li>
                <p style="margin: 0px;font-size: 15px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </li>
            <li>
                <p style="margin: 0px;font-size: 15px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </li>
        </ul> --> 

        <p>
            <b>Work Experience</b>
        </p>

        @if($user->profile == 'experience')
        @foreach($user['experience'] as $experienceDetails)
        <ul style="list-style: none;">
            <li>
                <p style="margin: 0px;font-size: 15px;"><b style="font-size: 15px;">Designation :</b> {{$experienceDetails->designation}}</p>
            </li>
            <li>
                <p style="margin: 0px;font-size: 15px;"><b style="font-size: 15px;">Company &nbsp;&nbsp;&nbsp;&nbsp;:</b> {{$experienceDetails->company}}
                </p>
            </li>
            <li>
                <p style="margin: 0px;font-size: 15px;"><b style="font-size: 15px;">Salary &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</b> {{$experienceDetails->salary}} {{$experienceDetails->salary_in}}
                </p>
            </li>
            <li>
                <p style="margin: 0px;font-size: 15px;"><b style="font-size: 15px;">Experience &nbsp;&nbsp;&nbsp;:</b> {{$experienceDetails->experience}}
                </p>
            </li>
        </ul>
        @endforeach
        @else
        <p style="margin-left: 39px;font-size: 15px;">I am a fresher</p>
        @endif
        <p>
            <b>Personal Details</b>
        </p>
        <ul style="list-style: none;">

            <li>
                <p style="margin: 0px;font-size: 15px;"><b style="font-size: 15px;">Date of Birth :</b> {{$user->date_of_birth}}</p>
            </li>
            <!-- <li>
                <p style="margin: 0px;font-size: 15px;"><b style="font-size: 15px;">Nationality &nbsp;&nbsp;&nbsp;:</b></p>
            </li> -->
            <li>
                <p style="margin: 0px;font-size: 15px;"><b style="font-size: 15px;">Gender &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</b> {{$user->gender}}</p>
            </li>
            <li>
                <p style="margin: 0px;font-size: 15px;"><b style="font-size: 15px;">Language &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</b> {{$user->language}}</p>
            </li>
        </ul>       
        
        <b style="float: left;">Date : {{$user->today_date}}</b>
        <b style="float: right;">{{$user->name}}</b>
</body>

</html>