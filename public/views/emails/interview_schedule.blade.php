
<h3>Hi <?php echo $employeName; ?>,</h3>

<p>As per discussion your interview has been scheduled with “<?php echo $companyName; ?>”. You are requested to
reach there on time.</p>
<p>Kindly note below details for same</p>
<p>Address – <?php echo $address; ?></p>
<p>Date –<?php echo $date; ?></p>
<p>Day – <?php echo $day; ?></p>
<p>Time – <?php echo $time; ?></p>

<p>Kindly visit on mentioned Interview Location on time, carry your updated CV along with you. Kindly
visit in a formal attire.</p>