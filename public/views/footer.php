    <script type="text/javascript" src="/angular/lib/js/jquery.min.js"></script>
	<script type="text/javascript" src="/angular/lib/js/angular.min.js"></script>
  	<script type="text/javascript" src="/angular/lib/js/angular-animate.min.js"></script>
  	<script type="text/javascript" src="/angular/lib/js/angular-aria.min.js"></script>
  	<script type="text/javascript" src="/angular/lib/js/angular-messages.min.js"></script>
    <script type="text/javascript" src="/angular/lib/js/angular-ui-router.min.js"></script>
    <script type="text/javascript" src="/angular/lib/js/sweet-alert.min.js"></script>
    <script type="text/javascript" src="/angular/lib/js/SweetAlert.js"></script>
    <script type="text/javascript" src="/angular/lib/js/modernizr.min.js"></script> 
    <script type="text/javascript" src="/angular/lib/js/tether.min.js"></script>
    <script type="text/javascript" src="/angular/lib/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/angular/lib/js/dirPagination.js"></script>


    <script type="text/javascript" src="/angular/app.js"></script>

    <!--Controllers-->
    <!-- <script type="text/javascript" src="/angular/common.controller.js"></script> -->

    <!--Services-->

</body>
</html>
