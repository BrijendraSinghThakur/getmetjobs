package dk.com.mainlogin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {
    TextView tv_signup;

    EditText edtPhone, edtPassword;
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv_signup =  findViewById(R.id.tv_signup);
        edtPhone = findViewById(R.id.edt_phone);
        edtPassword = findViewById(R.id.edt_Password);
        btnLogin = findViewById(R.id.btn_Login);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginCall(edtPhone.getText().toString(), edtPassword.getText().toString());
            }
        });

        tv_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegistartionScreen.class);
                startActivity(intent);
            }
        });
    }
    //end of oncretae*****************************************************

    private void LoginCall(String phone, String password) {
        final ProgressDialog progressDialog;
        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage("Login In");
        progressDialog.show();
        Retrofit retrofit = new Retrofit.Builder().baseUrl("http://mobileappdevelop.co/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Loadinterface loadInterface = retrofit.create(Loadinterface.class);

        Call<ResponseBody> resultCall = loadInterface.Login(phone, password);
        resultCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    try {
                        String responedata = response.body().string();
                        JSONObject object = new JSONObject(responedata);
                        String staus = object.getString("status");
                        System.out.println("loginresponse" + object);

                        if (staus.equals("1")) {
                            Toast.makeText(LoginActivity.this, "Login succefull", Toast.LENGTH_SHORT).show();
                            JSONObject resultObj = object.getJSONObject("result");
                            String name=resultObj.getString("username");
                            Intent intent = new Intent(LoginActivity.this, Home.class);
                            intent.putExtra("Name",name);
                            startActivity(intent);

                        } else {
                            Toast.makeText(LoginActivity.this, "login Failed", Toast.LENGTH_SHORT).show();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
//
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(LoginActivity.this, "Please Check Connection ", Toast.LENGTH_SHORT).show();

                // showErrorDialog();

            }
        });

    }

}
