var app = angular.module('GetMeJob', [
    "ui.router", 
    "oitozero.ngSweetAlert", 
    "ngMessages"
]);

app.config(function($stateProvider, $locationProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');
    $stateProvider
    .state('/', {
        url: '/',
        templateUrl: '/views/dashboard/dashboard.html'
    });

    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
});
