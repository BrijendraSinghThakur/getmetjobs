<?php

return [
    'driver' => env('FCM_PROTOCOL', 'http'),
    'log_enabled' => false,

    'http' => [
        'server_key' => 'AAAAPKS-Z3g:APA91bEGoux7TeKVhYSRNYT-PmJLlEtcrl2GzNk90Rmszb1Yp_Ey0gwePZa6aknwthZVXrY8V0kyTfW_ybabq2WEyU031jV-Ze1NtB6S7c-RHMsZc5ae3uQSf7U3_rtrUhBo-7N0NV9x',
        'sender_id' => '260461979512',
        'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
        'server_group_url' => 'https://android.googleapis.com/gcm/notification',
        'timeout' => 30.0, // in second
    ],
];
